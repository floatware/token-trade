import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Balance e2e test', () => {

    let navBarPage: NavBarPage;
    let balanceDialogPage: BalanceDialogPage;
    let balanceComponentsPage: BalanceComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Balances', () => {
        navBarPage.goToEntity('balance');
        balanceComponentsPage = new BalanceComponentsPage();
        expect(balanceComponentsPage.getTitle()).toMatch(/tokentradeApp.balance.home.title/);

    });

    it('should load create Balance dialog', () => {
        balanceComponentsPage.clickOnCreateButton();
        balanceDialogPage = new BalanceDialogPage();
        expect(balanceDialogPage.getModalTitle()).toMatch(/tokentradeApp.balance.home.createOrEditLabel/);
        balanceDialogPage.close();
    });

    it('should create and save Balances', () => {
        balanceComponentsPage.clickOnCreateButton();
        balanceDialogPage.setBalanceDataInput('balanceData');
        expect(balanceDialogPage.getBalanceDataInput()).toMatch('balanceData');
        balanceDialogPage.marketAccountSelectLastOption();
        balanceDialogPage.parentSelectLastOption();
        balanceDialogPage.save();
        expect(balanceDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class BalanceComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-balance div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class BalanceDialogPage {
    modalTitle = element(by.css('h4#myBalanceLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    balanceDataInput = element(by.css('input#field_balanceData'));
    marketAccountSelect = element(by.css('select#field_marketAccount'));
    parentSelect = element(by.css('select#field_parent'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setBalanceDataInput = function (balanceData) {
        this.balanceDataInput.sendKeys(balanceData);
    }

    getBalanceDataInput = function () {
        return this.balanceDataInput.getAttribute('value');
    }

    marketAccountSelectLastOption = function () {
        this.marketAccountSelect.all(by.tagName('option')).last().click();
    }

    marketAccountSelectOption = function (option) {
        this.marketAccountSelect.sendKeys(option);
    }

    getMarketAccountSelect = function () {
        return this.marketAccountSelect;
    }

    getMarketAccountSelectedOption = function () {
        return this.marketAccountSelect.element(by.css('option:checked')).getText();
    }

    parentSelectLastOption = function () {
        this.parentSelect.all(by.tagName('option')).last().click();
    }

    parentSelectOption = function (option) {
        this.parentSelect.sendKeys(option);
    }

    getParentSelect = function () {
        return this.parentSelect;
    }

    getParentSelectedOption = function () {
        return this.parentSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
