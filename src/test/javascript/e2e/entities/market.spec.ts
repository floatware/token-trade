import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Market e2e test', () => {

    let navBarPage: NavBarPage;
    let marketDialogPage: MarketDialogPage;
    let marketComponentsPage: MarketComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Markets', () => {
        navBarPage.goToEntity('market');
        marketComponentsPage = new MarketComponentsPage();
        expect(marketComponentsPage.getTitle()).toMatch(/tokentradeApp.market.home.title/);

    });

    it('should load create Market dialog', () => {
        marketComponentsPage.clickOnCreateButton();
        marketDialogPage = new MarketDialogPage();
        expect(marketDialogPage.getModalTitle()).toMatch(/tokentradeApp.market.home.createOrEditLabel/);
        marketDialogPage.close();
    });

    it('should create and save Markets', () => {
        marketComponentsPage.clickOnCreateButton();
        marketDialogPage.setNameInput('name');
        expect(marketDialogPage.getNameInput()).toMatch('name');
        marketDialogPage.setShortNameInput('shortName');
        expect(marketDialogPage.getShortNameInput()).toMatch('shortName');
        marketDialogPage.setImplementationInput('implementation');
        expect(marketDialogPage.getImplementationInput()).toMatch('implementation');
        marketDialogPage.setChartRepresentationInput('chartRepresentation');
        expect(marketDialogPage.getChartRepresentationInput()).toMatch('chartRepresentation');
        marketDialogPage.noteSelectLastOption();
        marketDialogPage.tenantSelectLastOption();
        marketDialogPage.save();
        expect(marketDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class MarketComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-market div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class MarketDialogPage {
    modalTitle = element(by.css('h4#myMarketLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    shortNameInput = element(by.css('input#field_shortName'));
    implementationInput = element(by.css('input#field_implementation'));
    chartRepresentationInput = element(by.css('input#field_chartRepresentation'));
    noteSelect = element(by.css('select#field_note'));
    tenantSelect = element(by.css('select#field_tenant'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function (name) {
        this.nameInput.sendKeys(name);
    }

    getNameInput = function () {
        return this.nameInput.getAttribute('value');
    }

    setShortNameInput = function (shortName) {
        this.shortNameInput.sendKeys(shortName);
    }

    getShortNameInput = function () {
        return this.shortNameInput.getAttribute('value');
    }

    setImplementationInput = function (implementation) {
        this.implementationInput.sendKeys(implementation);
    }

    getImplementationInput = function () {
        return this.implementationInput.getAttribute('value');
    }

    setChartRepresentationInput = function (chartRepresentation) {
        this.chartRepresentationInput.sendKeys(chartRepresentation);
    }

    getChartRepresentationInput = function () {
        return this.chartRepresentationInput.getAttribute('value');
    }

    noteSelectLastOption = function () {
        this.noteSelect.all(by.tagName('option')).last().click();
    }

    noteSelectOption = function (option) {
        this.noteSelect.sendKeys(option);
    }

    getNoteSelect = function () {
        return this.noteSelect;
    }

    getNoteSelectedOption = function () {
        return this.noteSelect.element(by.css('option:checked')).getText();
    }

    tenantSelectLastOption = function () {
        this.tenantSelect.all(by.tagName('option')).last().click();
    }

    tenantSelectOption = function (option) {
        this.tenantSelect.sendKeys(option);
    }

    getTenantSelect = function () {
        return this.tenantSelect;
    }

    getTenantSelectedOption = function () {
        return this.tenantSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
