import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('TradingPlan e2e test', () => {

    let navBarPage: NavBarPage;
    let tradingPlanDialogPage: TradingPlanDialogPage;
    let tradingPlanComponentsPage: TradingPlanComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load TradingPlans', () => {
        navBarPage.goToEntity('trading-plan');
        tradingPlanComponentsPage = new TradingPlanComponentsPage();
        expect(tradingPlanComponentsPage.getTitle()).toMatch(/tokentradeApp.tradingPlan.home.title/);

    });

    it('should load create TradingPlan dialog', () => {
        tradingPlanComponentsPage.clickOnCreateButton();
        tradingPlanDialogPage = new TradingPlanDialogPage();
        expect(tradingPlanDialogPage.getModalTitle()).toMatch(/tokentradeApp.tradingPlan.home.createOrEditLabel/);
        tradingPlanDialogPage.close();
    });

    it('should create and save TradingPlans', () => {
        tradingPlanComponentsPage.clickOnCreateButton();
        tradingPlanDialogPage.setUuidInput('uuid');
        expect(tradingPlanDialogPage.getUuidInput()).toMatch('uuid');
        tradingPlanDialogPage.tradingTypeSelectLastOption();
        tradingPlanDialogPage.noteSelectLastOption();
        tradingPlanDialogPage.traderSelectLastOption();
        tradingPlanDialogPage.save();
        expect(tradingPlanDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TradingPlanComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-trading-plan div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TradingPlanDialogPage {
    modalTitle = element(by.css('h4#myTradingPlanLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    uuidInput = element(by.css('input#field_uuid'));
    tradingTypeSelect = element(by.css('select#field_tradingType'));
    noteSelect = element(by.css('select#field_note'));
    traderSelect = element(by.css('select#field_trader'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setUuidInput = function (uuid) {
        this.uuidInput.sendKeys(uuid);
    }

    getUuidInput = function () {
        return this.uuidInput.getAttribute('value');
    }

    setTradingTypeSelect = function (tradingType) {
        this.tradingTypeSelect.sendKeys(tradingType);
    }

    getTradingTypeSelect = function () {
        return this.tradingTypeSelect.element(by.css('option:checked')).getText();
    }

    tradingTypeSelectLastOption = function () {
        this.tradingTypeSelect.all(by.tagName('option')).last().click();
    }
    noteSelectLastOption = function () {
        this.noteSelect.all(by.tagName('option')).last().click();
    }

    noteSelectOption = function (option) {
        this.noteSelect.sendKeys(option);
    }

    getNoteSelect = function () {
        return this.noteSelect;
    }

    getNoteSelectedOption = function () {
        return this.noteSelect.element(by.css('option:checked')).getText();
    }

    traderSelectLastOption = function () {
        this.traderSelect.all(by.tagName('option')).last().click();
    }

    traderSelectOption = function (option) {
        this.traderSelect.sendKeys(option);
    }

    getTraderSelect = function () {
        return this.traderSelect;
    }

    getTraderSelectedOption = function () {
        return this.traderSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
