import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Note e2e test', () => {

    let navBarPage: NavBarPage;
    let noteDialogPage: NoteDialogPage;
    let noteComponentsPage: NoteComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Notes', () => {
        navBarPage.goToEntity('note');
        noteComponentsPage = new NoteComponentsPage();
        expect(noteComponentsPage.getTitle()).toMatch(/tokentradeApp.note.home.title/);

    });

    it('should load create Note dialog', () => {
        noteComponentsPage.clickOnCreateButton();
        noteDialogPage = new NoteDialogPage();
        expect(noteDialogPage.getModalTitle()).toMatch(/tokentradeApp.note.home.createOrEditLabel/);
        noteDialogPage.close();
    });

    it('should create and save Notes', () => {
        noteComponentsPage.clickOnCreateButton();
        noteDialogPage.setDescriptionInput('description');
        expect(noteDialogPage.getDescriptionInput()).toMatch('description');
        noteDialogPage.save();
        expect(noteDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class NoteComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-note div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class NoteDialogPage {
    modalTitle = element(by.css('h4#myNoteLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    descriptionInput = element(by.css('input#field_description'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setDescriptionInput = function (description) {
        this.descriptionInput.sendKeys(description);
    }

    getDescriptionInput = function () {
        return this.descriptionInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
