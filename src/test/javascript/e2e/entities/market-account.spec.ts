import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('MarketAccount e2e test', () => {

    let navBarPage: NavBarPage;
    let marketAccountDialogPage: MarketAccountDialogPage;
    let marketAccountComponentsPage: MarketAccountComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load MarketAccounts', () => {
        navBarPage.goToEntity('market-account');
        marketAccountComponentsPage = new MarketAccountComponentsPage();
        expect(marketAccountComponentsPage.getTitle()).toMatch(/tokentradeApp.marketAccount.home.title/);

    });

    it('should load create MarketAccount dialog', () => {
        marketAccountComponentsPage.clickOnCreateButton();
        marketAccountDialogPage = new MarketAccountDialogPage();
        expect(marketAccountDialogPage.getModalTitle()).toMatch(/tokentradeApp.marketAccount.home.createOrEditLabel/);
        marketAccountDialogPage.close();
    });

    it('should create and save MarketAccounts', () => {
        marketAccountComponentsPage.clickOnCreateButton();
        marketAccountDialogPage.marketAccountTypeSelectLastOption();
        marketAccountDialogPage.setUserNameInput('userName');
        expect(marketAccountDialogPage.getUserNameInput()).toMatch('userName');
        marketAccountDialogPage.setPasswordInput('password');
        expect(marketAccountDialogPage.getPasswordInput()).toMatch('password');
        marketAccountDialogPage.setApiKeyInput('apiKey');
        expect(marketAccountDialogPage.getApiKeyInput()).toMatch('apiKey');
        marketAccountDialogPage.setSecretKeyInput('secretKey');
        expect(marketAccountDialogPage.getSecretKeyInput()).toMatch('secretKey');
        marketAccountDialogPage.clientSelectLastOption();
        marketAccountDialogPage.marketSelectLastOption();
        marketAccountDialogPage.save();
        expect(marketAccountDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class MarketAccountComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-market-account div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class MarketAccountDialogPage {
    modalTitle = element(by.css('h4#myMarketAccountLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    marketAccountTypeSelect = element(by.css('select#field_marketAccountType'));
    userNameInput = element(by.css('input#field_userName'));
    passwordInput = element(by.css('input#field_password'));
    apiKeyInput = element(by.css('input#field_apiKey'));
    secretKeyInput = element(by.css('input#field_secretKey'));
    clientSelect = element(by.css('select#field_client'));
    marketSelect = element(by.css('select#field_market'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setMarketAccountTypeSelect = function (marketAccountType) {
        this.marketAccountTypeSelect.sendKeys(marketAccountType);
    }

    getMarketAccountTypeSelect = function () {
        return this.marketAccountTypeSelect.element(by.css('option:checked')).getText();
    }

    marketAccountTypeSelectLastOption = function () {
        this.marketAccountTypeSelect.all(by.tagName('option')).last().click();
    }
    setUserNameInput = function (userName) {
        this.userNameInput.sendKeys(userName);
    }

    getUserNameInput = function () {
        return this.userNameInput.getAttribute('value');
    }

    setPasswordInput = function (password) {
        this.passwordInput.sendKeys(password);
    }

    getPasswordInput = function () {
        return this.passwordInput.getAttribute('value');
    }

    setApiKeyInput = function (apiKey) {
        this.apiKeyInput.sendKeys(apiKey);
    }

    getApiKeyInput = function () {
        return this.apiKeyInput.getAttribute('value');
    }

    setSecretKeyInput = function (secretKey) {
        this.secretKeyInput.sendKeys(secretKey);
    }

    getSecretKeyInput = function () {
        return this.secretKeyInput.getAttribute('value');
    }

    clientSelectLastOption = function () {
        this.clientSelect.all(by.tagName('option')).last().click();
    }

    clientSelectOption = function (option) {
        this.clientSelect.sendKeys(option);
    }

    getClientSelect = function () {
        return this.clientSelect;
    }

    getClientSelectedOption = function () {
        return this.clientSelect.element(by.css('option:checked')).getText();
    }

    marketSelectLastOption = function () {
        this.marketSelect.all(by.tagName('option')).last().click();
    }

    marketSelectOption = function (option) {
        this.marketSelect.sendKeys(option);
    }

    getMarketSelect = function () {
        return this.marketSelect;
    }

    getMarketSelectedOption = function () {
        return this.marketSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
