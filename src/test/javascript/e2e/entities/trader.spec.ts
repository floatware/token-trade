import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Trader e2e test', () => {

    let navBarPage: NavBarPage;
    let traderDialogPage: TraderDialogPage;
    let traderComponentsPage: TraderComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Traders', () => {
        navBarPage.goToEntity('trader');
        traderComponentsPage = new TraderComponentsPage();
        expect(traderComponentsPage.getTitle()).toMatch(/tokentradeApp.trader.home.title/);

    });

    it('should load create Trader dialog', () => {
        traderComponentsPage.clickOnCreateButton();
        traderDialogPage = new TraderDialogPage();
        expect(traderDialogPage.getModalTitle()).toMatch(/tokentradeApp.trader.home.createOrEditLabel/);
        traderDialogPage.close();
    });

    it('should create and save Traders', () => {
        traderComponentsPage.clickOnCreateButton();
        traderDialogPage.setNameInput('name');
        expect(traderDialogPage.getNameInput()).toMatch('name');
        traderDialogPage.setFirstNameInput('firstName');
        expect(traderDialogPage.getFirstNameInput()).toMatch('firstName');
        traderDialogPage.setLastNameInput('lastName');
        expect(traderDialogPage.getLastNameInput()).toMatch('lastName');
        traderDialogPage.tenantSelectLastOption();
        traderDialogPage.userSelectLastOption();
        traderDialogPage.save();
        expect(traderDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TraderComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-trader div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TraderDialogPage {
    modalTitle = element(by.css('h4#myTraderLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    firstNameInput = element(by.css('input#field_firstName'));
    lastNameInput = element(by.css('input#field_lastName'));
    tenantSelect = element(by.css('select#field_tenant'));
    userSelect = element(by.css('select#field_user'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function (name) {
        this.nameInput.sendKeys(name);
    }

    getNameInput = function () {
        return this.nameInput.getAttribute('value');
    }

    setFirstNameInput = function (firstName) {
        this.firstNameInput.sendKeys(firstName);
    }

    getFirstNameInput = function () {
        return this.firstNameInput.getAttribute('value');
    }

    setLastNameInput = function (lastName) {
        this.lastNameInput.sendKeys(lastName);
    }

    getLastNameInput = function () {
        return this.lastNameInput.getAttribute('value');
    }

    tenantSelectLastOption = function () {
        this.tenantSelect.all(by.tagName('option')).last().click();
    }

    tenantSelectOption = function (option) {
        this.tenantSelect.sendKeys(option);
    }

    getTenantSelect = function () {
        return this.tenantSelect;
    }

    getTenantSelectedOption = function () {
        return this.tenantSelect.element(by.css('option:checked')).getText();
    }

    userSelectLastOption = function () {
        this.userSelect.all(by.tagName('option')).last().click();
    }

    userSelectOption = function (option) {
        this.userSelect.sendKeys(option);
    }

    getUserSelect = function () {
        return this.userSelect;
    }

    getUserSelectedOption = function () {
        return this.userSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
