import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('TradingOrder e2e test', () => {

    let navBarPage: NavBarPage;
    let tradingOrderDialogPage: TradingOrderDialogPage;
    let tradingOrderComponentsPage: TradingOrderComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load TradingOrders', () => {
        navBarPage.goToEntity('trading-order');
        tradingOrderComponentsPage = new TradingOrderComponentsPage();
        expect(tradingOrderComponentsPage.getTitle()).toMatch(/tokentradeApp.tradingOrder.home.title/);

    });

    it('should load create TradingOrder dialog', () => {
        tradingOrderComponentsPage.clickOnCreateButton();
        tradingOrderDialogPage = new TradingOrderDialogPage();
        expect(tradingOrderDialogPage.getModalTitle()).toMatch(/tokentradeApp.tradingOrder.home.createOrEditLabel/);
        tradingOrderDialogPage.close();
    });

    it('should create and save TradingOrders', () => {
        tradingOrderComponentsPage.clickOnCreateButton();
        tradingOrderDialogPage.setUuidInput('uuid');
        expect(tradingOrderDialogPage.getUuidInput()).toMatch('uuid');
        tradingOrderDialogPage.statusSelectLastOption();
        tradingOrderDialogPage.setOriginalAmountInput('5');
        expect(tradingOrderDialogPage.getOriginalAmountInput()).toMatch('5');
        tradingOrderDialogPage.setAveragePriceInput('5');
        expect(tradingOrderDialogPage.getAveragePriceInput()).toMatch('5');
        tradingOrderDialogPage.setCumulativeAmountInput('5');
        expect(tradingOrderDialogPage.getCumulativeAmountInput()).toMatch('5');
        tradingOrderDialogPage.marketAccountSelectLastOption();
        tradingOrderDialogPage.tradingOrderTypeSelectLastOption();
        tradingOrderDialogPage.tradingOrderPackageSelectLastOption();
        tradingOrderDialogPage.assetSelectLastOption();
        tradingOrderDialogPage.save();
        expect(tradingOrderDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TradingOrderComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-trading-order div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TradingOrderDialogPage {
    modalTitle = element(by.css('h4#myTradingOrderLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    uuidInput = element(by.css('input#field_uuid'));
    statusSelect = element(by.css('select#field_status'));
    originalAmountInput = element(by.css('input#field_originalAmount'));
    averagePriceInput = element(by.css('input#field_averagePrice'));
    cumulativeAmountInput = element(by.css('input#field_cumulativeAmount'));
    marketAccountSelect = element(by.css('select#field_marketAccount'));
    tradingOrderTypeSelect = element(by.css('select#field_tradingOrderType'));
    tradingOrderPackageSelect = element(by.css('select#field_tradingOrderPackage'));
    assetSelect = element(by.css('select#field_asset'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setUuidInput = function (uuid) {
        this.uuidInput.sendKeys(uuid);
    }

    getUuidInput = function () {
        return this.uuidInput.getAttribute('value');
    }

    setStatusSelect = function (status) {
        this.statusSelect.sendKeys(status);
    }

    getStatusSelect = function () {
        return this.statusSelect.element(by.css('option:checked')).getText();
    }

    statusSelectLastOption = function () {
        this.statusSelect.all(by.tagName('option')).last().click();
    }
    setOriginalAmountInput = function (originalAmount) {
        this.originalAmountInput.sendKeys(originalAmount);
    }

    getOriginalAmountInput = function () {
        return this.originalAmountInput.getAttribute('value');
    }

    setAveragePriceInput = function (averagePrice) {
        this.averagePriceInput.sendKeys(averagePrice);
    }

    getAveragePriceInput = function () {
        return this.averagePriceInput.getAttribute('value');
    }

    setCumulativeAmountInput = function (cumulativeAmount) {
        this.cumulativeAmountInput.sendKeys(cumulativeAmount);
    }

    getCumulativeAmountInput = function () {
        return this.cumulativeAmountInput.getAttribute('value');
    }

    marketAccountSelectLastOption = function () {
        this.marketAccountSelect.all(by.tagName('option')).last().click();
    }

    marketAccountSelectOption = function (option) {
        this.marketAccountSelect.sendKeys(option);
    }

    getMarketAccountSelect = function () {
        return this.marketAccountSelect;
    }

    getMarketAccountSelectedOption = function () {
        return this.marketAccountSelect.element(by.css('option:checked')).getText();
    }

    tradingOrderTypeSelectLastOption = function () {
        this.tradingOrderTypeSelect.all(by.tagName('option')).last().click();
    }

    tradingOrderTypeSelectOption = function (option) {
        this.tradingOrderTypeSelect.sendKeys(option);
    }

    getTradingOrderTypeSelect = function () {
        return this.tradingOrderTypeSelect;
    }

    getTradingOrderTypeSelectedOption = function () {
        return this.tradingOrderTypeSelect.element(by.css('option:checked')).getText();
    }

    tradingOrderPackageSelectLastOption = function () {
        this.tradingOrderPackageSelect.all(by.tagName('option')).last().click();
    }

    tradingOrderPackageSelectOption = function (option) {
        this.tradingOrderPackageSelect.sendKeys(option);
    }

    getTradingOrderPackageSelect = function () {
        return this.tradingOrderPackageSelect;
    }

    getTradingOrderPackageSelectedOption = function () {
        return this.tradingOrderPackageSelect.element(by.css('option:checked')).getText();
    }

    assetSelectLastOption = function () {
        this.assetSelect.all(by.tagName('option')).last().click();
    }

    assetSelectOption = function (option) {
        this.assetSelect.sendKeys(option);
    }

    getAssetSelect = function () {
        return this.assetSelect;
    }

    getAssetSelectedOption = function () {
        return this.assetSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
