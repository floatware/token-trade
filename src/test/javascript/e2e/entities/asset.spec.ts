import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Asset e2e test', () => {

    let navBarPage: NavBarPage;
    let assetDialogPage: AssetDialogPage;
    let assetComponentsPage: AssetComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Assets', () => {
        navBarPage.goToEntity('asset');
        assetComponentsPage = new AssetComponentsPage();
        expect(assetComponentsPage.getTitle()).toMatch(/tokentradeApp.asset.home.title/);

    });

    it('should load create Asset dialog', () => {
        assetComponentsPage.clickOnCreateButton();
        assetDialogPage = new AssetDialogPage();
        expect(assetDialogPage.getModalTitle()).toMatch(/tokentradeApp.asset.home.createOrEditLabel/);
        assetDialogPage.close();
    });

    it('should create and save Assets', () => {
        assetComponentsPage.clickOnCreateButton();
        assetDialogPage.setNameInput('name');
        expect(assetDialogPage.getNameInput()).toMatch('name');
        assetDialogPage.setShortNameInput('shortName');
        expect(assetDialogPage.getShortNameInput()).toMatch('shortName');
        assetDialogPage.setImplementationInput('implementation');
        expect(assetDialogPage.getImplementationInput()).toMatch('implementation');
        assetDialogPage.setChartRepresentationInput('chartRepresentation');
        expect(assetDialogPage.getChartRepresentationInput()).toMatch('chartRepresentation');
        assetDialogPage.marketSelectLastOption();
        assetDialogPage.marketCurrencySelectLastOption();
        assetDialogPage.baseCurrencySelectLastOption();
        assetDialogPage.save();
        expect(assetDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class AssetComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-asset div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AssetDialogPage {
    modalTitle = element(by.css('h4#myAssetLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    shortNameInput = element(by.css('input#field_shortName'));
    implementationInput = element(by.css('input#field_implementation'));
    chartRepresentationInput = element(by.css('input#field_chartRepresentation'));
    marketSelect = element(by.css('select#field_market'));
    marketCurrencySelect = element(by.css('select#field_marketCurrency'));
    baseCurrencySelect = element(by.css('select#field_baseCurrency'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function (name) {
        this.nameInput.sendKeys(name);
    }

    getNameInput = function () {
        return this.nameInput.getAttribute('value');
    }

    setShortNameInput = function (shortName) {
        this.shortNameInput.sendKeys(shortName);
    }

    getShortNameInput = function () {
        return this.shortNameInput.getAttribute('value');
    }

    setImplementationInput = function (implementation) {
        this.implementationInput.sendKeys(implementation);
    }

    getImplementationInput = function () {
        return this.implementationInput.getAttribute('value');
    }

    setChartRepresentationInput = function (chartRepresentation) {
        this.chartRepresentationInput.sendKeys(chartRepresentation);
    }

    getChartRepresentationInput = function () {
        return this.chartRepresentationInput.getAttribute('value');
    }

    marketSelectLastOption = function () {
        this.marketSelect.all(by.tagName('option')).last().click();
    }

    marketSelectOption = function (option) {
        this.marketSelect.sendKeys(option);
    }

    getMarketSelect = function () {
        return this.marketSelect;
    }

    getMarketSelectedOption = function () {
        return this.marketSelect.element(by.css('option:checked')).getText();
    }

    marketCurrencySelectLastOption = function () {
        this.marketCurrencySelect.all(by.tagName('option')).last().click();
    }

    marketCurrencySelectOption = function (option) {
        this.marketCurrencySelect.sendKeys(option);
    }

    getMarketCurrencySelect = function () {
        return this.marketCurrencySelect;
    }

    getMarketCurrencySelectedOption = function () {
        return this.marketCurrencySelect.element(by.css('option:checked')).getText();
    }

    baseCurrencySelectLastOption = function () {
        this.baseCurrencySelect.all(by.tagName('option')).last().click();
    }

    baseCurrencySelectOption = function (option) {
        this.baseCurrencySelect.sendKeys(option);
    }

    getBaseCurrencySelect = function () {
        return this.baseCurrencySelect;
    }

    getBaseCurrencySelectedOption = function () {
        return this.baseCurrencySelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
