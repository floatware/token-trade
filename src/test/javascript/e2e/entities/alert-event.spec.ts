import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('AlertEvent e2e test', () => {

    let navBarPage: NavBarPage;
    let alertEventDialogPage: AlertEventDialogPage;
    let alertEventComponentsPage: AlertEventComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load AlertEvents', () => {
        navBarPage.goToEntity('alert-event');
        alertEventComponentsPage = new AlertEventComponentsPage();
        expect(alertEventComponentsPage.getTitle()).toMatch(/tokentradeApp.alertEvent.home.title/);

    });

    it('should load create AlertEvent dialog', () => {
        alertEventComponentsPage.clickOnCreateButton();
        alertEventDialogPage = new AlertEventDialogPage();
        expect(alertEventDialogPage.getModalTitle()).toMatch(/tokentradeApp.alertEvent.home.createOrEditLabel/);
        alertEventDialogPage.close();
    });

    it('should create and save AlertEvents', () => {
        alertEventComponentsPage.clickOnCreateButton();
        alertEventDialogPage.setCurrentPriceInput('5');
        expect(alertEventDialogPage.getCurrentPriceInput()).toMatch('5');
        alertEventDialogPage.setAlertPriceInput('5');
        expect(alertEventDialogPage.getAlertPriceInput()).toMatch('5');
        alertEventDialogPage.getFiredInput().isSelected().then(function (selected) {
            if (selected) {
                alertEventDialogPage.getFiredInput().click();
                expect(alertEventDialogPage.getFiredInput().isSelected()).toBeFalsy();
            } else {
                alertEventDialogPage.getFiredInput().click();
                expect(alertEventDialogPage.getFiredInput().isSelected()).toBeTruthy();
            }
        });
        alertEventDialogPage.setNoteInput('note');
        expect(alertEventDialogPage.getNoteInput()).toMatch('note');
        alertEventDialogPage.assetSelectLastOption();
        alertEventDialogPage.save();
        expect(alertEventDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class AlertEventComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-alert-event div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AlertEventDialogPage {
    modalTitle = element(by.css('h4#myAlertEventLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    currentPriceInput = element(by.css('input#field_currentPrice'));
    alertPriceInput = element(by.css('input#field_alertPrice'));
    firedInput = element(by.css('input#field_fired'));
    noteInput = element(by.css('input#field_note'));
    assetSelect = element(by.css('select#field_asset'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setCurrentPriceInput = function (currentPrice) {
        this.currentPriceInput.sendKeys(currentPrice);
    }

    getCurrentPriceInput = function () {
        return this.currentPriceInput.getAttribute('value');
    }

    setAlertPriceInput = function (alertPrice) {
        this.alertPriceInput.sendKeys(alertPrice);
    }

    getAlertPriceInput = function () {
        return this.alertPriceInput.getAttribute('value');
    }

    getFiredInput = function () {
        return this.firedInput;
    }
    setNoteInput = function (note) {
        this.noteInput.sendKeys(note);
    }

    getNoteInput = function () {
        return this.noteInput.getAttribute('value');
    }

    assetSelectLastOption = function () {
        this.assetSelect.all(by.tagName('option')).last().click();
    }

    assetSelectOption = function (option) {
        this.assetSelect.sendKeys(option);
    }

    getAssetSelect = function () {
        return this.assetSelect;
    }

    getAssetSelectedOption = function () {
        return this.assetSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
