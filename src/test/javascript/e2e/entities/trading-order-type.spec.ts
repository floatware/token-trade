import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('TradingOrderType e2e test', () => {

    let navBarPage: NavBarPage;
    let tradingOrderTypeDialogPage: TradingOrderTypeDialogPage;
    let tradingOrderTypeComponentsPage: TradingOrderTypeComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load TradingOrderTypes', () => {
        navBarPage.goToEntity('trading-order-type');
        tradingOrderTypeComponentsPage = new TradingOrderTypeComponentsPage();
        expect(tradingOrderTypeComponentsPage.getTitle()).toMatch(/tokentradeApp.tradingOrderType.home.title/);

    });

    it('should load create TradingOrderType dialog', () => {
        tradingOrderTypeComponentsPage.clickOnCreateButton();
        tradingOrderTypeDialogPage = new TradingOrderTypeDialogPage();
        expect(tradingOrderTypeDialogPage.getModalTitle()).toMatch(/tokentradeApp.tradingOrderType.home.createOrEditLabel/);
        tradingOrderTypeDialogPage.close();
    });

    it('should create and save TradingOrderTypes', () => {
        tradingOrderTypeComponentsPage.clickOnCreateButton();
        tradingOrderTypeDialogPage.setTradingOrdertypeInput('tradingOrdertype');
        expect(tradingOrderTypeDialogPage.getTradingOrdertypeInput()).toMatch('tradingOrdertype');
        tradingOrderTypeDialogPage.supportedMarketSelectLastOption();
        tradingOrderTypeDialogPage.save();
        expect(tradingOrderTypeDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TradingOrderTypeComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-trading-order-type div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TradingOrderTypeDialogPage {
    modalTitle = element(by.css('h4#myTradingOrderTypeLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    tradingOrdertypeInput = element(by.css('input#field_tradingOrdertype'));
    supportedMarketSelect = element(by.css('select#field_supportedMarket'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setTradingOrdertypeInput = function (tradingOrdertype) {
        this.tradingOrdertypeInput.sendKeys(tradingOrdertype);
    }

    getTradingOrdertypeInput = function () {
        return this.tradingOrdertypeInput.getAttribute('value');
    }

    supportedMarketSelectLastOption = function () {
        this.supportedMarketSelect.all(by.tagName('option')).last().click();
    }

    supportedMarketSelectOption = function (option) {
        this.supportedMarketSelect.sendKeys(option);
    }

    getSupportedMarketSelect = function () {
        return this.supportedMarketSelect;
    }

    getSupportedMarketSelectedOption = function () {
        return this.supportedMarketSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
