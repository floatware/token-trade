import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('TradingOrderPackage e2e test', () => {

    let navBarPage: NavBarPage;
    let tradingOrderPackageDialogPage: TradingOrderPackageDialogPage;
    let tradingOrderPackageComponentsPage: TradingOrderPackageComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load TradingOrderPackages', () => {
        navBarPage.goToEntity('trading-order-package');
        tradingOrderPackageComponentsPage = new TradingOrderPackageComponentsPage();
        expect(tradingOrderPackageComponentsPage.getTitle()).toMatch(/tokentradeApp.tradingOrderPackage.home.title/);

    });

    it('should load create TradingOrderPackage dialog', () => {
        tradingOrderPackageComponentsPage.clickOnCreateButton();
        tradingOrderPackageDialogPage = new TradingOrderPackageDialogPage();
        expect(tradingOrderPackageDialogPage.getModalTitle()).toMatch(/tokentradeApp.tradingOrderPackage.home.createOrEditLabel/);
        tradingOrderPackageDialogPage.close();
    });

    it('should create and save TradingOrderPackages', () => {
        tradingOrderPackageComponentsPage.clickOnCreateButton();
        tradingOrderPackageDialogPage.setUuidInput('uuid');
        expect(tradingOrderPackageDialogPage.getUuidInput()).toMatch('uuid');
        tradingOrderPackageDialogPage.tradingPlanSelectLastOption();
        tradingOrderPackageDialogPage.save();
        expect(tradingOrderPackageDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TradingOrderPackageComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-trading-order-package div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TradingOrderPackageDialogPage {
    modalTitle = element(by.css('h4#myTradingOrderPackageLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    uuidInput = element(by.css('input#field_uuid'));
    tradingPlanSelect = element(by.css('select#field_tradingPlan'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setUuidInput = function (uuid) {
        this.uuidInput.sendKeys(uuid);
    }

    getUuidInput = function () {
        return this.uuidInput.getAttribute('value');
    }

    tradingPlanSelectLastOption = function () {
        this.tradingPlanSelect.all(by.tagName('option')).last().click();
    }

    tradingPlanSelectOption = function (option) {
        this.tradingPlanSelect.sendKeys(option);
    }

    getTradingPlanSelect = function () {
        return this.tradingPlanSelect;
    }

    getTradingPlanSelectedOption = function () {
        return this.tradingPlanSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
