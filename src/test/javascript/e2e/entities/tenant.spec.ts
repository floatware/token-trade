import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Tenant e2e test', () => {

    let navBarPage: NavBarPage;
    let tenantDialogPage: TenantDialogPage;
    let tenantComponentsPage: TenantComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Tenants', () => {
        navBarPage.goToEntity('tenant');
        tenantComponentsPage = new TenantComponentsPage();
        expect(tenantComponentsPage.getTitle()).toMatch(/tokentradeApp.tenant.home.title/);

    });

    it('should load create Tenant dialog', () => {
        tenantComponentsPage.clickOnCreateButton();
        tenantDialogPage = new TenantDialogPage();
        expect(tenantDialogPage.getModalTitle()).toMatch(/tokentradeApp.tenant.home.createOrEditLabel/);
        tenantDialogPage.close();
    });

    it('should create and save Tenants', () => {
        tenantComponentsPage.clickOnCreateButton();
        tenantDialogPage.setNameInput('name');
        expect(tenantDialogPage.getNameInput()).toMatch('name');
        tenantDialogPage.setShortNameInput('shortName');
        expect(tenantDialogPage.getShortNameInput()).toMatch('shortName');
        tenantDialogPage.setResourceInput('resource');
        expect(tenantDialogPage.getResourceInput()).toMatch('resource');
        tenantDialogPage.adminSelectLastOption();
        tenantDialogPage.save();
        expect(tenantDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TenantComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-tenant div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TenantDialogPage {
    modalTitle = element(by.css('h4#myTenantLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    shortNameInput = element(by.css('input#field_shortName'));
    resourceInput = element(by.css('input#field_resource'));
    adminSelect = element(by.css('select#field_admin'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function (name) {
        this.nameInput.sendKeys(name);
    }

    getNameInput = function () {
        return this.nameInput.getAttribute('value');
    }

    setShortNameInput = function (shortName) {
        this.shortNameInput.sendKeys(shortName);
    }

    getShortNameInput = function () {
        return this.shortNameInput.getAttribute('value');
    }

    setResourceInput = function (resource) {
        this.resourceInput.sendKeys(resource);
    }

    getResourceInput = function () {
        return this.resourceInput.getAttribute('value');
    }

    adminSelectLastOption = function () {
        this.adminSelect.all(by.tagName('option')).last().click();
    }

    adminSelectOption = function (option) {
        this.adminSelect.sendKeys(option);
    }

    getAdminSelect = function () {
        return this.adminSelect;
    }

    getAdminSelectedOption = function () {
        return this.adminSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
