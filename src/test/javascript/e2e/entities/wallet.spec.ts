import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Wallet e2e test', () => {

    let navBarPage: NavBarPage;
    let walletDialogPage: WalletDialogPage;
    let walletComponentsPage: WalletComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Wallets', () => {
        navBarPage.goToEntity('wallet');
        walletComponentsPage = new WalletComponentsPage();
        expect(walletComponentsPage.getTitle()).toMatch(/tokentradeApp.wallet.home.title/);

    });

    it('should load create Wallet dialog', () => {
        walletComponentsPage.clickOnCreateButton();
        walletDialogPage = new WalletDialogPage();
        expect(walletDialogPage.getModalTitle()).toMatch(/tokentradeApp.wallet.home.createOrEditLabel/);
        walletDialogPage.close();
    });

    it('should create and save Wallets', () => {
        walletComponentsPage.clickOnCreateButton();
        walletDialogPage.setDescriptionInput('description');
        expect(walletDialogPage.getDescriptionInput()).toMatch('description');
        walletDialogPage.setPublicKeyInput('publicKey');
        expect(walletDialogPage.getPublicKeyInput()).toMatch('publicKey');
        walletDialogPage.currencySelectLastOption();
        walletDialogPage.clientSelectLastOption();
        walletDialogPage.save();
        expect(walletDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class WalletComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-wallet div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class WalletDialogPage {
    modalTitle = element(by.css('h4#myWalletLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    descriptionInput = element(by.css('input#field_description'));
    publicKeyInput = element(by.css('input#field_publicKey'));
    currencySelect = element(by.css('select#field_currency'));
    clientSelect = element(by.css('select#field_client'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setDescriptionInput = function (description) {
        this.descriptionInput.sendKeys(description);
    }

    getDescriptionInput = function () {
        return this.descriptionInput.getAttribute('value');
    }

    setPublicKeyInput = function (publicKey) {
        this.publicKeyInput.sendKeys(publicKey);
    }

    getPublicKeyInput = function () {
        return this.publicKeyInput.getAttribute('value');
    }

    currencySelectLastOption = function () {
        this.currencySelect.all(by.tagName('option')).last().click();
    }

    currencySelectOption = function (option) {
        this.currencySelect.sendKeys(option);
    }

    getCurrencySelect = function () {
        return this.currencySelect;
    }

    getCurrencySelectedOption = function () {
        return this.currencySelect.element(by.css('option:checked')).getText();
    }

    clientSelectLastOption = function () {
        this.clientSelect.all(by.tagName('option')).last().click();
    }

    clientSelectOption = function (option) {
        this.clientSelect.sendKeys(option);
    }

    getClientSelect = function () {
        return this.clientSelect;
    }

    getClientSelectedOption = function () {
        return this.clientSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
