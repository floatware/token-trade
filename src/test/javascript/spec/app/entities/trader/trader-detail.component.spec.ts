/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TokentradeTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TraderDetailComponent } from '../../../../../../main/webapp/app/entities/trader/trader-detail.component';
import { TraderService } from '../../../../../../main/webapp/app/entities/trader/trader.service';
import { Trader } from '../../../../../../main/webapp/app/entities/trader/trader.model';

describe('Component Tests', () => {

    describe('Trader Management Detail Component', () => {
        let comp: TraderDetailComponent;
        let fixture: ComponentFixture<TraderDetailComponent>;
        let service: TraderService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TokentradeTestModule],
                declarations: [TraderDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    TraderService,
                    JhiEventManager
                ]
            }).overrideTemplate(TraderDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TraderDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TraderService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Trader(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.trader).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
