/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TokentradeTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ClientGroupDetailComponent } from '../../../../../../main/webapp/app/entities/client-group/client-group-detail.component';
import { ClientGroupService } from '../../../../../../main/webapp/app/entities/client-group/client-group.service';
import { ClientGroup } from '../../../../../../main/webapp/app/entities/client-group/client-group.model';

describe('Component Tests', () => {

    describe('ClientGroup Management Detail Component', () => {
        let comp: ClientGroupDetailComponent;
        let fixture: ComponentFixture<ClientGroupDetailComponent>;
        let service: ClientGroupService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TokentradeTestModule],
                declarations: [ClientGroupDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ClientGroupService,
                    JhiEventManager
                ]
            }).overrideTemplate(ClientGroupDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClientGroupDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClientGroupService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ClientGroup(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.clientGroup).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
