/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TokentradeTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TradingOrderTypeDetailComponent } from '../../../../../../main/webapp/app/entities/trading-order-type/trading-order-type-detail.component';
import { TradingOrderTypeService } from '../../../../../../main/webapp/app/entities/trading-order-type/trading-order-type.service';
import { TradingOrderType } from '../../../../../../main/webapp/app/entities/trading-order-type/trading-order-type.model';

describe('Component Tests', () => {

    describe('TradingOrderType Management Detail Component', () => {
        let comp: TradingOrderTypeDetailComponent;
        let fixture: ComponentFixture<TradingOrderTypeDetailComponent>;
        let service: TradingOrderTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TokentradeTestModule],
                declarations: [TradingOrderTypeDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    TradingOrderTypeService,
                    JhiEventManager
                ]
            }).overrideTemplate(TradingOrderTypeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TradingOrderTypeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TradingOrderTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new TradingOrderType(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.tradingOrderType).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
