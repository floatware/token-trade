/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TokentradeTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TenantDetailComponent } from '../../../../../../main/webapp/app/entities/tenant/tenant-detail.component';
import { TenantService } from '../../../../../../main/webapp/app/entities/tenant/tenant.service';
import { Tenant } from '../../../../../../main/webapp/app/entities/tenant/tenant.model';

describe('Component Tests', () => {

    describe('Tenant Management Detail Component', () => {
        let comp: TenantDetailComponent;
        let fixture: ComponentFixture<TenantDetailComponent>;
        let service: TenantService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TokentradeTestModule],
                declarations: [TenantDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    TenantService,
                    JhiEventManager
                ]
            }).overrideTemplate(TenantDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TenantDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TenantService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Tenant(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.tenant).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
