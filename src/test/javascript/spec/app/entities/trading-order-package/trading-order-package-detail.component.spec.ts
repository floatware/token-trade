/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TokentradeTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TradingOrderPackageDetailComponent } from '../../../../../../main/webapp/app/entities/trading-order-package/trading-order-package-detail.component';
import { TradingOrderPackageService } from '../../../../../../main/webapp/app/entities/trading-order-package/trading-order-package.service';
import { TradingOrderPackage } from '../../../../../../main/webapp/app/entities/trading-order-package/trading-order-package.model';

describe('Component Tests', () => {

    describe('TradingOrderPackage Management Detail Component', () => {
        let comp: TradingOrderPackageDetailComponent;
        let fixture: ComponentFixture<TradingOrderPackageDetailComponent>;
        let service: TradingOrderPackageService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TokentradeTestModule],
                declarations: [TradingOrderPackageDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    TradingOrderPackageService,
                    JhiEventManager
                ]
            }).overrideTemplate(TradingOrderPackageDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TradingOrderPackageDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TradingOrderPackageService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new TradingOrderPackage(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.tradingOrderPackage).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
