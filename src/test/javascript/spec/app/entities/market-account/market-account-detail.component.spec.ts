/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TokentradeTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { MarketAccountDetailComponent } from '../../../../../../main/webapp/app/entities/market-account/market-account-detail.component';
import { MarketAccountService } from '../../../../../../main/webapp/app/entities/market-account/market-account.service';
import { MarketAccount } from '../../../../../../main/webapp/app/entities/market-account/market-account.model';

describe('Component Tests', () => {

    describe('MarketAccount Management Detail Component', () => {
        let comp: MarketAccountDetailComponent;
        let fixture: ComponentFixture<MarketAccountDetailComponent>;
        let service: MarketAccountService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TokentradeTestModule],
                declarations: [MarketAccountDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    MarketAccountService,
                    JhiEventManager
                ]
            }).overrideTemplate(MarketAccountDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MarketAccountDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MarketAccountService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new MarketAccount(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.marketAccount).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
