/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TokentradeTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { BalanceDetailComponent } from '../../../../../../main/webapp/app/entities/balance/balance-detail.component';
import { BalanceService } from '../../../../../../main/webapp/app/entities/balance/balance.service';
import { Balance } from '../../../../../../main/webapp/app/entities/balance/balance.model';

describe('Component Tests', () => {

    describe('Balance Management Detail Component', () => {
        let comp: BalanceDetailComponent;
        let fixture: ComponentFixture<BalanceDetailComponent>;
        let service: BalanceService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TokentradeTestModule],
                declarations: [BalanceDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    BalanceService,
                    JhiEventManager
                ]
            }).overrideTemplate(BalanceDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BalanceDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BalanceService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Balance(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.balance).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
