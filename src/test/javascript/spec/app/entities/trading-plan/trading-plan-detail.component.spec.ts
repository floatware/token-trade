/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TokentradeTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TradingPlanDetailComponent } from '../../../../../../main/webapp/app/entities/trading-plan/trading-plan-detail.component';
import { TradingPlanService } from '../../../../../../main/webapp/app/entities/trading-plan/trading-plan.service';
import { TradingPlan } from '../../../../../../main/webapp/app/entities/trading-plan/trading-plan.model';

describe('Component Tests', () => {

    describe('TradingPlan Management Detail Component', () => {
        let comp: TradingPlanDetailComponent;
        let fixture: ComponentFixture<TradingPlanDetailComponent>;
        let service: TradingPlanService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TokentradeTestModule],
                declarations: [TradingPlanDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    TradingPlanService,
                    JhiEventManager
                ]
            }).overrideTemplate(TradingPlanDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TradingPlanDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TradingPlanService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new TradingPlan(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.tradingPlan).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
