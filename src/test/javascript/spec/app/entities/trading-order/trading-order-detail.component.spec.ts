/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TokentradeTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TradingOrderDetailComponent } from '../../../../../../main/webapp/app/entities/trading-order/trading-order-detail.component';
import { TradingOrderService } from '../../../../../../main/webapp/app/entities/trading-order/trading-order.service';
import { TradingOrder } from '../../../../../../main/webapp/app/entities/trading-order/trading-order.model';

describe('Component Tests', () => {

    describe('TradingOrder Management Detail Component', () => {
        let comp: TradingOrderDetailComponent;
        let fixture: ComponentFixture<TradingOrderDetailComponent>;
        let service: TradingOrderService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TokentradeTestModule],
                declarations: [TradingOrderDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    TradingOrderService,
                    JhiEventManager
                ]
            }).overrideTemplate(TradingOrderDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TradingOrderDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TradingOrderService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new TradingOrder(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.tradingOrder).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
