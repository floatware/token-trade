/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TokentradeTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AlertEventDetailComponent } from '../../../../../../main/webapp/app/entities/alert-event/alert-event-detail.component';
import { AlertEventService } from '../../../../../../main/webapp/app/entities/alert-event/alert-event.service';
import { AlertEvent } from '../../../../../../main/webapp/app/entities/alert-event/alert-event.model';

describe('Component Tests', () => {

    describe('AlertEvent Management Detail Component', () => {
        let comp: AlertEventDetailComponent;
        let fixture: ComponentFixture<AlertEventDetailComponent>;
        let service: AlertEventService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TokentradeTestModule],
                declarations: [AlertEventDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    AlertEventService,
                    JhiEventManager
                ]
            }).overrideTemplate(AlertEventDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AlertEventDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AlertEventService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new AlertEvent(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.alertEvent).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
