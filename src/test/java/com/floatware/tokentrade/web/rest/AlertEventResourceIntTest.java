package com.floatware.tokentrade.web.rest;

import com.floatware.tokentrade.TokentradeApp;

import com.floatware.tokentrade.domain.AlertEvent;
import com.floatware.tokentrade.repository.AlertEventRepository;
import com.floatware.tokentrade.service.AlertEventService;
import com.floatware.tokentrade.repository.search.AlertEventSearchRepository;
import com.floatware.tokentrade.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AlertEventResource REST controller.
 *
 * @see AlertEventResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TokentradeApp.class)
public class AlertEventResourceIntTest {

    private static final BigDecimal DEFAULT_CURRENT_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_CURRENT_PRICE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_ALERT_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_ALERT_PRICE = new BigDecimal(2);

    private static final Boolean DEFAULT_FIRED = false;
    private static final Boolean UPDATED_FIRED = true;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private AlertEventRepository alertEventRepository;

    @Autowired
    private AlertEventService alertEventService;

    @Autowired
    private AlertEventSearchRepository alertEventSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAlertEventMockMvc;

    private AlertEvent alertEvent;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AlertEventResource alertEventResource = new AlertEventResource(alertEventService);
        this.restAlertEventMockMvc = MockMvcBuilders.standaloneSetup(alertEventResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AlertEvent createEntity(EntityManager em) {
        AlertEvent alertEvent = new AlertEvent()
            .currentPrice(DEFAULT_CURRENT_PRICE)
            .alertPrice(DEFAULT_ALERT_PRICE)
            .fired(DEFAULT_FIRED)
            .note(DEFAULT_NOTE);
        return alertEvent;
    }

    @Before
    public void initTest() {
        alertEventSearchRepository.deleteAll();
        alertEvent = createEntity(em);
    }

    @Test
    @Transactional
    public void createAlertEvent() throws Exception {
        int databaseSizeBeforeCreate = alertEventRepository.findAll().size();

        // Create the AlertEvent
        restAlertEventMockMvc.perform(post("/api/alert-events")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(alertEvent)))
            .andExpect(status().isCreated());

        // Validate the AlertEvent in the database
        List<AlertEvent> alertEventList = alertEventRepository.findAll();
        assertThat(alertEventList).hasSize(databaseSizeBeforeCreate + 1);
        AlertEvent testAlertEvent = alertEventList.get(alertEventList.size() - 1);
        assertThat(testAlertEvent.getCurrentPrice()).isEqualTo(DEFAULT_CURRENT_PRICE);
        assertThat(testAlertEvent.getAlertPrice()).isEqualTo(DEFAULT_ALERT_PRICE);
        assertThat(testAlertEvent.isFired()).isEqualTo(DEFAULT_FIRED);
        assertThat(testAlertEvent.getNote()).isEqualTo(DEFAULT_NOTE);

        // Validate the AlertEvent in Elasticsearch
        AlertEvent alertEventEs = alertEventSearchRepository.findOne(testAlertEvent.getId());
        assertThat(alertEventEs).isEqualToComparingFieldByField(testAlertEvent);
    }

    @Test
    @Transactional
    public void createAlertEventWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = alertEventRepository.findAll().size();

        // Create the AlertEvent with an existing ID
        alertEvent.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAlertEventMockMvc.perform(post("/api/alert-events")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(alertEvent)))
            .andExpect(status().isBadRequest());

        // Validate the AlertEvent in the database
        List<AlertEvent> alertEventList = alertEventRepository.findAll();
        assertThat(alertEventList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCurrentPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = alertEventRepository.findAll().size();
        // set the field null
        alertEvent.setCurrentPrice(null);

        // Create the AlertEvent, which fails.

        restAlertEventMockMvc.perform(post("/api/alert-events")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(alertEvent)))
            .andExpect(status().isBadRequest());

        List<AlertEvent> alertEventList = alertEventRepository.findAll();
        assertThat(alertEventList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAlertPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = alertEventRepository.findAll().size();
        // set the field null
        alertEvent.setAlertPrice(null);

        // Create the AlertEvent, which fails.

        restAlertEventMockMvc.perform(post("/api/alert-events")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(alertEvent)))
            .andExpect(status().isBadRequest());

        List<AlertEvent> alertEventList = alertEventRepository.findAll();
        assertThat(alertEventList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAlertEvents() throws Exception {
        // Initialize the database
        alertEventRepository.saveAndFlush(alertEvent);

        // Get all the alertEventList
        restAlertEventMockMvc.perform(get("/api/alert-events?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(alertEvent.getId().intValue())))
            .andExpect(jsonPath("$.[*].currentPrice").value(hasItem(DEFAULT_CURRENT_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].alertPrice").value(hasItem(DEFAULT_ALERT_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].fired").value(hasItem(DEFAULT_FIRED.booleanValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }

    @Test
    @Transactional
    public void getAlertEvent() throws Exception {
        // Initialize the database
        alertEventRepository.saveAndFlush(alertEvent);

        // Get the alertEvent
        restAlertEventMockMvc.perform(get("/api/alert-events/{id}", alertEvent.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(alertEvent.getId().intValue()))
            .andExpect(jsonPath("$.currentPrice").value(DEFAULT_CURRENT_PRICE.intValue()))
            .andExpect(jsonPath("$.alertPrice").value(DEFAULT_ALERT_PRICE.intValue()))
            .andExpect(jsonPath("$.fired").value(DEFAULT_FIRED.booleanValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAlertEvent() throws Exception {
        // Get the alertEvent
        restAlertEventMockMvc.perform(get("/api/alert-events/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAlertEvent() throws Exception {
        // Initialize the database
        alertEventService.save(alertEvent);

        int databaseSizeBeforeUpdate = alertEventRepository.findAll().size();

        // Update the alertEvent
        AlertEvent updatedAlertEvent = alertEventRepository.findOne(alertEvent.getId());
        updatedAlertEvent
            .currentPrice(UPDATED_CURRENT_PRICE)
            .alertPrice(UPDATED_ALERT_PRICE)
            .fired(UPDATED_FIRED)
            .note(UPDATED_NOTE);

        restAlertEventMockMvc.perform(put("/api/alert-events")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAlertEvent)))
            .andExpect(status().isOk());

        // Validate the AlertEvent in the database
        List<AlertEvent> alertEventList = alertEventRepository.findAll();
        assertThat(alertEventList).hasSize(databaseSizeBeforeUpdate);
        AlertEvent testAlertEvent = alertEventList.get(alertEventList.size() - 1);
        assertThat(testAlertEvent.getCurrentPrice()).isEqualTo(UPDATED_CURRENT_PRICE);
        assertThat(testAlertEvent.getAlertPrice()).isEqualTo(UPDATED_ALERT_PRICE);
        assertThat(testAlertEvent.isFired()).isEqualTo(UPDATED_FIRED);
        assertThat(testAlertEvent.getNote()).isEqualTo(UPDATED_NOTE);

        // Validate the AlertEvent in Elasticsearch
        AlertEvent alertEventEs = alertEventSearchRepository.findOne(testAlertEvent.getId());
        assertThat(alertEventEs).isEqualToComparingFieldByField(testAlertEvent);
    }

    @Test
    @Transactional
    public void updateNonExistingAlertEvent() throws Exception {
        int databaseSizeBeforeUpdate = alertEventRepository.findAll().size();

        // Create the AlertEvent

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAlertEventMockMvc.perform(put("/api/alert-events")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(alertEvent)))
            .andExpect(status().isCreated());

        // Validate the AlertEvent in the database
        List<AlertEvent> alertEventList = alertEventRepository.findAll();
        assertThat(alertEventList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAlertEvent() throws Exception {
        // Initialize the database
        alertEventService.save(alertEvent);

        int databaseSizeBeforeDelete = alertEventRepository.findAll().size();

        // Get the alertEvent
        restAlertEventMockMvc.perform(delete("/api/alert-events/{id}", alertEvent.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean alertEventExistsInEs = alertEventSearchRepository.exists(alertEvent.getId());
        assertThat(alertEventExistsInEs).isFalse();

        // Validate the database is empty
        List<AlertEvent> alertEventList = alertEventRepository.findAll();
        assertThat(alertEventList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAlertEvent() throws Exception {
        // Initialize the database
        alertEventService.save(alertEvent);

        // Search the alertEvent
        restAlertEventMockMvc.perform(get("/api/_search/alert-events?query=id:" + alertEvent.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(alertEvent.getId().intValue())))
            .andExpect(jsonPath("$.[*].currentPrice").value(hasItem(DEFAULT_CURRENT_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].alertPrice").value(hasItem(DEFAULT_ALERT_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].fired").value(hasItem(DEFAULT_FIRED.booleanValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AlertEvent.class);
        AlertEvent alertEvent1 = new AlertEvent();
        alertEvent1.setId(1L);
        AlertEvent alertEvent2 = new AlertEvent();
        alertEvent2.setId(alertEvent1.getId());
        assertThat(alertEvent1).isEqualTo(alertEvent2);
        alertEvent2.setId(2L);
        assertThat(alertEvent1).isNotEqualTo(alertEvent2);
        alertEvent1.setId(null);
        assertThat(alertEvent1).isNotEqualTo(alertEvent2);
    }
}
