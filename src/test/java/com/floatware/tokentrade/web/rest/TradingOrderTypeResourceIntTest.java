package com.floatware.tokentrade.web.rest;

import com.floatware.tokentrade.TokentradeApp;

import com.floatware.tokentrade.domain.TradingOrderType;
import com.floatware.tokentrade.repository.TradingOrderTypeRepository;
import com.floatware.tokentrade.service.TradingOrderTypeService;
import com.floatware.tokentrade.repository.search.TradingOrderTypeSearchRepository;
import com.floatware.tokentrade.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TradingOrderTypeResource REST controller.
 *
 * @see TradingOrderTypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TokentradeApp.class)
public class TradingOrderTypeResourceIntTest {

    private static final String DEFAULT_TRADING_ORDERTYPE = "AAAAAAAAAA";
    private static final String UPDATED_TRADING_ORDERTYPE = "BBBBBBBBBB";

    @Autowired
    private TradingOrderTypeRepository tradingOrderTypeRepository;

    @Autowired
    private TradingOrderTypeService tradingOrderTypeService;

    @Autowired
    private TradingOrderTypeSearchRepository tradingOrderTypeSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTradingOrderTypeMockMvc;

    private TradingOrderType tradingOrderType;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TradingOrderTypeResource tradingOrderTypeResource = new TradingOrderTypeResource(tradingOrderTypeService);
        this.restTradingOrderTypeMockMvc = MockMvcBuilders.standaloneSetup(tradingOrderTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TradingOrderType createEntity(EntityManager em) {
        TradingOrderType tradingOrderType = new TradingOrderType()
            .tradingOrdertype(DEFAULT_TRADING_ORDERTYPE);
        return tradingOrderType;
    }

    @Before
    public void initTest() {
        tradingOrderTypeSearchRepository.deleteAll();
        tradingOrderType = createEntity(em);
    }

    @Test
    @Transactional
    public void createTradingOrderType() throws Exception {
        int databaseSizeBeforeCreate = tradingOrderTypeRepository.findAll().size();

        // Create the TradingOrderType
        restTradingOrderTypeMockMvc.perform(post("/api/trading-order-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingOrderType)))
            .andExpect(status().isCreated());

        // Validate the TradingOrderType in the database
        List<TradingOrderType> tradingOrderTypeList = tradingOrderTypeRepository.findAll();
        assertThat(tradingOrderTypeList).hasSize(databaseSizeBeforeCreate + 1);
        TradingOrderType testTradingOrderType = tradingOrderTypeList.get(tradingOrderTypeList.size() - 1);
        assertThat(testTradingOrderType.getTradingOrdertype()).isEqualTo(DEFAULT_TRADING_ORDERTYPE);

        // Validate the TradingOrderType in Elasticsearch
        TradingOrderType tradingOrderTypeEs = tradingOrderTypeSearchRepository.findOne(testTradingOrderType.getId());
        assertThat(tradingOrderTypeEs).isEqualToComparingFieldByField(testTradingOrderType);
    }

    @Test
    @Transactional
    public void createTradingOrderTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tradingOrderTypeRepository.findAll().size();

        // Create the TradingOrderType with an existing ID
        tradingOrderType.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTradingOrderTypeMockMvc.perform(post("/api/trading-order-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingOrderType)))
            .andExpect(status().isBadRequest());

        // Validate the TradingOrderType in the database
        List<TradingOrderType> tradingOrderTypeList = tradingOrderTypeRepository.findAll();
        assertThat(tradingOrderTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTradingOrdertypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = tradingOrderTypeRepository.findAll().size();
        // set the field null
        tradingOrderType.setTradingOrdertype(null);

        // Create the TradingOrderType, which fails.

        restTradingOrderTypeMockMvc.perform(post("/api/trading-order-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingOrderType)))
            .andExpect(status().isBadRequest());

        List<TradingOrderType> tradingOrderTypeList = tradingOrderTypeRepository.findAll();
        assertThat(tradingOrderTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTradingOrderTypes() throws Exception {
        // Initialize the database
        tradingOrderTypeRepository.saveAndFlush(tradingOrderType);

        // Get all the tradingOrderTypeList
        restTradingOrderTypeMockMvc.perform(get("/api/trading-order-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tradingOrderType.getId().intValue())))
            .andExpect(jsonPath("$.[*].tradingOrdertype").value(hasItem(DEFAULT_TRADING_ORDERTYPE.toString())));
    }

    @Test
    @Transactional
    public void getTradingOrderType() throws Exception {
        // Initialize the database
        tradingOrderTypeRepository.saveAndFlush(tradingOrderType);

        // Get the tradingOrderType
        restTradingOrderTypeMockMvc.perform(get("/api/trading-order-types/{id}", tradingOrderType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tradingOrderType.getId().intValue()))
            .andExpect(jsonPath("$.tradingOrdertype").value(DEFAULT_TRADING_ORDERTYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTradingOrderType() throws Exception {
        // Get the tradingOrderType
        restTradingOrderTypeMockMvc.perform(get("/api/trading-order-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTradingOrderType() throws Exception {
        // Initialize the database
        tradingOrderTypeService.save(tradingOrderType);

        int databaseSizeBeforeUpdate = tradingOrderTypeRepository.findAll().size();

        // Update the tradingOrderType
        TradingOrderType updatedTradingOrderType = tradingOrderTypeRepository.findOne(tradingOrderType.getId());
        updatedTradingOrderType
            .tradingOrdertype(UPDATED_TRADING_ORDERTYPE);

        restTradingOrderTypeMockMvc.perform(put("/api/trading-order-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTradingOrderType)))
            .andExpect(status().isOk());

        // Validate the TradingOrderType in the database
        List<TradingOrderType> tradingOrderTypeList = tradingOrderTypeRepository.findAll();
        assertThat(tradingOrderTypeList).hasSize(databaseSizeBeforeUpdate);
        TradingOrderType testTradingOrderType = tradingOrderTypeList.get(tradingOrderTypeList.size() - 1);
        assertThat(testTradingOrderType.getTradingOrdertype()).isEqualTo(UPDATED_TRADING_ORDERTYPE);

        // Validate the TradingOrderType in Elasticsearch
        TradingOrderType tradingOrderTypeEs = tradingOrderTypeSearchRepository.findOne(testTradingOrderType.getId());
        assertThat(tradingOrderTypeEs).isEqualToComparingFieldByField(testTradingOrderType);
    }

    @Test
    @Transactional
    public void updateNonExistingTradingOrderType() throws Exception {
        int databaseSizeBeforeUpdate = tradingOrderTypeRepository.findAll().size();

        // Create the TradingOrderType

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTradingOrderTypeMockMvc.perform(put("/api/trading-order-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingOrderType)))
            .andExpect(status().isCreated());

        // Validate the TradingOrderType in the database
        List<TradingOrderType> tradingOrderTypeList = tradingOrderTypeRepository.findAll();
        assertThat(tradingOrderTypeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTradingOrderType() throws Exception {
        // Initialize the database
        tradingOrderTypeService.save(tradingOrderType);

        int databaseSizeBeforeDelete = tradingOrderTypeRepository.findAll().size();

        // Get the tradingOrderType
        restTradingOrderTypeMockMvc.perform(delete("/api/trading-order-types/{id}", tradingOrderType.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tradingOrderTypeExistsInEs = tradingOrderTypeSearchRepository.exists(tradingOrderType.getId());
        assertThat(tradingOrderTypeExistsInEs).isFalse();

        // Validate the database is empty
        List<TradingOrderType> tradingOrderTypeList = tradingOrderTypeRepository.findAll();
        assertThat(tradingOrderTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTradingOrderType() throws Exception {
        // Initialize the database
        tradingOrderTypeService.save(tradingOrderType);

        // Search the tradingOrderType
        restTradingOrderTypeMockMvc.perform(get("/api/_search/trading-order-types?query=id:" + tradingOrderType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tradingOrderType.getId().intValue())))
            .andExpect(jsonPath("$.[*].tradingOrdertype").value(hasItem(DEFAULT_TRADING_ORDERTYPE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TradingOrderType.class);
        TradingOrderType tradingOrderType1 = new TradingOrderType();
        tradingOrderType1.setId(1L);
        TradingOrderType tradingOrderType2 = new TradingOrderType();
        tradingOrderType2.setId(tradingOrderType1.getId());
        assertThat(tradingOrderType1).isEqualTo(tradingOrderType2);
        tradingOrderType2.setId(2L);
        assertThat(tradingOrderType1).isNotEqualTo(tradingOrderType2);
        tradingOrderType1.setId(null);
        assertThat(tradingOrderType1).isNotEqualTo(tradingOrderType2);
    }
}
