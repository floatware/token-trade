package com.floatware.tokentrade.web.rest;

import com.floatware.tokentrade.TokentradeApp;

import com.floatware.tokentrade.domain.ClientGroup;
import com.floatware.tokentrade.repository.ClientGroupRepository;
import com.floatware.tokentrade.service.ClientGroupService;
import com.floatware.tokentrade.repository.search.ClientGroupSearchRepository;
import com.floatware.tokentrade.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ClientGroupResource REST controller.
 *
 * @see ClientGroupResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TokentradeApp.class)
public class ClientGroupResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private ClientGroupRepository clientGroupRepository;

    @Autowired
    private ClientGroupService clientGroupService;

    @Autowired
    private ClientGroupSearchRepository clientGroupSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restClientGroupMockMvc;

    private ClientGroup clientGroup;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClientGroupResource clientGroupResource = new ClientGroupResource(clientGroupService);
        this.restClientGroupMockMvc = MockMvcBuilders.standaloneSetup(clientGroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientGroup createEntity(EntityManager em) {
        ClientGroup clientGroup = new ClientGroup()
            .name(DEFAULT_NAME);
        return clientGroup;
    }

    @Before
    public void initTest() {
        clientGroupSearchRepository.deleteAll();
        clientGroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createClientGroup() throws Exception {
        int databaseSizeBeforeCreate = clientGroupRepository.findAll().size();

        // Create the ClientGroup
        restClientGroupMockMvc.perform(post("/api/client-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientGroup)))
            .andExpect(status().isCreated());

        // Validate the ClientGroup in the database
        List<ClientGroup> clientGroupList = clientGroupRepository.findAll();
        assertThat(clientGroupList).hasSize(databaseSizeBeforeCreate + 1);
        ClientGroup testClientGroup = clientGroupList.get(clientGroupList.size() - 1);
        assertThat(testClientGroup.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the ClientGroup in Elasticsearch
        ClientGroup clientGroupEs = clientGroupSearchRepository.findOne(testClientGroup.getId());
        assertThat(clientGroupEs).isEqualToComparingFieldByField(testClientGroup);
    }

    @Test
    @Transactional
    public void createClientGroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clientGroupRepository.findAll().size();

        // Create the ClientGroup with an existing ID
        clientGroup.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientGroupMockMvc.perform(post("/api/client-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientGroup)))
            .andExpect(status().isBadRequest());

        // Validate the ClientGroup in the database
        List<ClientGroup> clientGroupList = clientGroupRepository.findAll();
        assertThat(clientGroupList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientGroupRepository.findAll().size();
        // set the field null
        clientGroup.setName(null);

        // Create the ClientGroup, which fails.

        restClientGroupMockMvc.perform(post("/api/client-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientGroup)))
            .andExpect(status().isBadRequest());

        List<ClientGroup> clientGroupList = clientGroupRepository.findAll();
        assertThat(clientGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClientGroups() throws Exception {
        // Initialize the database
        clientGroupRepository.saveAndFlush(clientGroup);

        // Get all the clientGroupList
        restClientGroupMockMvc.perform(get("/api/client-groups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getClientGroup() throws Exception {
        // Initialize the database
        clientGroupRepository.saveAndFlush(clientGroup);

        // Get the clientGroup
        restClientGroupMockMvc.perform(get("/api/client-groups/{id}", clientGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clientGroup.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingClientGroup() throws Exception {
        // Get the clientGroup
        restClientGroupMockMvc.perform(get("/api/client-groups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClientGroup() throws Exception {
        // Initialize the database
        clientGroupService.save(clientGroup);

        int databaseSizeBeforeUpdate = clientGroupRepository.findAll().size();

        // Update the clientGroup
        ClientGroup updatedClientGroup = clientGroupRepository.findOne(clientGroup.getId());
        updatedClientGroup
            .name(UPDATED_NAME);

        restClientGroupMockMvc.perform(put("/api/client-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedClientGroup)))
            .andExpect(status().isOk());

        // Validate the ClientGroup in the database
        List<ClientGroup> clientGroupList = clientGroupRepository.findAll();
        assertThat(clientGroupList).hasSize(databaseSizeBeforeUpdate);
        ClientGroup testClientGroup = clientGroupList.get(clientGroupList.size() - 1);
        assertThat(testClientGroup.getName()).isEqualTo(UPDATED_NAME);

        // Validate the ClientGroup in Elasticsearch
        ClientGroup clientGroupEs = clientGroupSearchRepository.findOne(testClientGroup.getId());
        assertThat(clientGroupEs).isEqualToComparingFieldByField(testClientGroup);
    }

    @Test
    @Transactional
    public void updateNonExistingClientGroup() throws Exception {
        int databaseSizeBeforeUpdate = clientGroupRepository.findAll().size();

        // Create the ClientGroup

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restClientGroupMockMvc.perform(put("/api/client-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientGroup)))
            .andExpect(status().isCreated());

        // Validate the ClientGroup in the database
        List<ClientGroup> clientGroupList = clientGroupRepository.findAll();
        assertThat(clientGroupList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteClientGroup() throws Exception {
        // Initialize the database
        clientGroupService.save(clientGroup);

        int databaseSizeBeforeDelete = clientGroupRepository.findAll().size();

        // Get the clientGroup
        restClientGroupMockMvc.perform(delete("/api/client-groups/{id}", clientGroup.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean clientGroupExistsInEs = clientGroupSearchRepository.exists(clientGroup.getId());
        assertThat(clientGroupExistsInEs).isFalse();

        // Validate the database is empty
        List<ClientGroup> clientGroupList = clientGroupRepository.findAll();
        assertThat(clientGroupList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchClientGroup() throws Exception {
        // Initialize the database
        clientGroupService.save(clientGroup);

        // Search the clientGroup
        restClientGroupMockMvc.perform(get("/api/_search/client-groups?query=id:" + clientGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientGroup.class);
        ClientGroup clientGroup1 = new ClientGroup();
        clientGroup1.setId(1L);
        ClientGroup clientGroup2 = new ClientGroup();
        clientGroup2.setId(clientGroup1.getId());
        assertThat(clientGroup1).isEqualTo(clientGroup2);
        clientGroup2.setId(2L);
        assertThat(clientGroup1).isNotEqualTo(clientGroup2);
        clientGroup1.setId(null);
        assertThat(clientGroup1).isNotEqualTo(clientGroup2);
    }
}
