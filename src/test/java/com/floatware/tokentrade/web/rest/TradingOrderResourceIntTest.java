package com.floatware.tokentrade.web.rest;

import com.floatware.tokentrade.TokentradeApp;

import com.floatware.tokentrade.domain.TradingOrder;
import com.floatware.tokentrade.repository.TradingOrderRepository;
import com.floatware.tokentrade.service.TradingOrderService;
import com.floatware.tokentrade.repository.search.TradingOrderSearchRepository;
import com.floatware.tokentrade.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.floatware.tokentrade.domain.enumeration.TradingOrderStatus;
/**
 * Test class for the TradingOrderResource REST controller.
 *
 * @see TradingOrderResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TokentradeApp.class)
public class TradingOrderResourceIntTest {

    private static final String DEFAULT_UUID = "AAAAAAAAAA";
    private static final String UPDATED_UUID = "BBBBBBBBBB";

    private static final TradingOrderStatus DEFAULT_STATUS = TradingOrderStatus.PENDING_NEW;
    private static final TradingOrderStatus UPDATED_STATUS = TradingOrderStatus.NEW;

    private static final BigDecimal DEFAULT_ORIGINAL_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_ORIGINAL_AMOUNT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_AVERAGE_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_AVERAGE_PRICE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_CUMULATIVE_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_CUMULATIVE_AMOUNT = new BigDecimal(2);

    @Autowired
    private TradingOrderRepository tradingOrderRepository;

    @Autowired
    private TradingOrderService tradingOrderService;

    @Autowired
    private TradingOrderSearchRepository tradingOrderSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTradingOrderMockMvc;

    private TradingOrder tradingOrder;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TradingOrderResource tradingOrderResource = new TradingOrderResource(tradingOrderService);
        this.restTradingOrderMockMvc = MockMvcBuilders.standaloneSetup(tradingOrderResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TradingOrder createEntity(EntityManager em) {
        TradingOrder tradingOrder = new TradingOrder()
            .uuid(DEFAULT_UUID)
            .status(DEFAULT_STATUS)
            .originalAmount(DEFAULT_ORIGINAL_AMOUNT)
            .averagePrice(DEFAULT_AVERAGE_PRICE)
            .cumulativeAmount(DEFAULT_CUMULATIVE_AMOUNT);
        return tradingOrder;
    }

    @Before
    public void initTest() {
        tradingOrderSearchRepository.deleteAll();
        tradingOrder = createEntity(em);
    }

    @Test
    @Transactional
    public void createTradingOrder() throws Exception {
        int databaseSizeBeforeCreate = tradingOrderRepository.findAll().size();

        // Create the TradingOrder
        restTradingOrderMockMvc.perform(post("/api/trading-orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingOrder)))
            .andExpect(status().isCreated());

        // Validate the TradingOrder in the database
        List<TradingOrder> tradingOrderList = tradingOrderRepository.findAll();
        assertThat(tradingOrderList).hasSize(databaseSizeBeforeCreate + 1);
        TradingOrder testTradingOrder = tradingOrderList.get(tradingOrderList.size() - 1);
        assertThat(testTradingOrder.getUuid()).isEqualTo(DEFAULT_UUID);
        assertThat(testTradingOrder.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testTradingOrder.getOriginalAmount()).isEqualTo(DEFAULT_ORIGINAL_AMOUNT);
        assertThat(testTradingOrder.getAveragePrice()).isEqualTo(DEFAULT_AVERAGE_PRICE);
        assertThat(testTradingOrder.getCumulativeAmount()).isEqualTo(DEFAULT_CUMULATIVE_AMOUNT);

        // Validate the TradingOrder in Elasticsearch
        TradingOrder tradingOrderEs = tradingOrderSearchRepository.findOne(testTradingOrder.getId());
        assertThat(tradingOrderEs).isEqualToComparingFieldByField(testTradingOrder);
    }

    @Test
    @Transactional
    public void createTradingOrderWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tradingOrderRepository.findAll().size();

        // Create the TradingOrder with an existing ID
        tradingOrder.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTradingOrderMockMvc.perform(post("/api/trading-orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingOrder)))
            .andExpect(status().isBadRequest());

        // Validate the TradingOrder in the database
        List<TradingOrder> tradingOrderList = tradingOrderRepository.findAll();
        assertThat(tradingOrderList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkUuidIsRequired() throws Exception {
        int databaseSizeBeforeTest = tradingOrderRepository.findAll().size();
        // set the field null
        tradingOrder.setUuid(null);

        // Create the TradingOrder, which fails.

        restTradingOrderMockMvc.perform(post("/api/trading-orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingOrder)))
            .andExpect(status().isBadRequest());

        List<TradingOrder> tradingOrderList = tradingOrderRepository.findAll();
        assertThat(tradingOrderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = tradingOrderRepository.findAll().size();
        // set the field null
        tradingOrder.setStatus(null);

        // Create the TradingOrder, which fails.

        restTradingOrderMockMvc.perform(post("/api/trading-orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingOrder)))
            .andExpect(status().isBadRequest());

        List<TradingOrder> tradingOrderList = tradingOrderRepository.findAll();
        assertThat(tradingOrderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOriginalAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = tradingOrderRepository.findAll().size();
        // set the field null
        tradingOrder.setOriginalAmount(null);

        // Create the TradingOrder, which fails.

        restTradingOrderMockMvc.perform(post("/api/trading-orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingOrder)))
            .andExpect(status().isBadRequest());

        List<TradingOrder> tradingOrderList = tradingOrderRepository.findAll();
        assertThat(tradingOrderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTradingOrders() throws Exception {
        // Initialize the database
        tradingOrderRepository.saveAndFlush(tradingOrder);

        // Get all the tradingOrderList
        restTradingOrderMockMvc.perform(get("/api/trading-orders?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tradingOrder.getId().intValue())))
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(DEFAULT_UUID.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].originalAmount").value(hasItem(DEFAULT_ORIGINAL_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].averagePrice").value(hasItem(DEFAULT_AVERAGE_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].cumulativeAmount").value(hasItem(DEFAULT_CUMULATIVE_AMOUNT.intValue())));
    }

    @Test
    @Transactional
    public void getTradingOrder() throws Exception {
        // Initialize the database
        tradingOrderRepository.saveAndFlush(tradingOrder);

        // Get the tradingOrder
        restTradingOrderMockMvc.perform(get("/api/trading-orders/{id}", tradingOrder.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tradingOrder.getId().intValue()))
            .andExpect(jsonPath("$.uuid").value(DEFAULT_UUID.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.originalAmount").value(DEFAULT_ORIGINAL_AMOUNT.intValue()))
            .andExpect(jsonPath("$.averagePrice").value(DEFAULT_AVERAGE_PRICE.intValue()))
            .andExpect(jsonPath("$.cumulativeAmount").value(DEFAULT_CUMULATIVE_AMOUNT.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTradingOrder() throws Exception {
        // Get the tradingOrder
        restTradingOrderMockMvc.perform(get("/api/trading-orders/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTradingOrder() throws Exception {
        // Initialize the database
        tradingOrderService.save(tradingOrder);

        int databaseSizeBeforeUpdate = tradingOrderRepository.findAll().size();

        // Update the tradingOrder
        TradingOrder updatedTradingOrder = tradingOrderRepository.findOne(tradingOrder.getId());
        updatedTradingOrder
            .uuid(UPDATED_UUID)
            .status(UPDATED_STATUS)
            .originalAmount(UPDATED_ORIGINAL_AMOUNT)
            .averagePrice(UPDATED_AVERAGE_PRICE)
            .cumulativeAmount(UPDATED_CUMULATIVE_AMOUNT);

        restTradingOrderMockMvc.perform(put("/api/trading-orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTradingOrder)))
            .andExpect(status().isOk());

        // Validate the TradingOrder in the database
        List<TradingOrder> tradingOrderList = tradingOrderRepository.findAll();
        assertThat(tradingOrderList).hasSize(databaseSizeBeforeUpdate);
        TradingOrder testTradingOrder = tradingOrderList.get(tradingOrderList.size() - 1);
        assertThat(testTradingOrder.getUuid()).isEqualTo(UPDATED_UUID);
        assertThat(testTradingOrder.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testTradingOrder.getOriginalAmount()).isEqualTo(UPDATED_ORIGINAL_AMOUNT);
        assertThat(testTradingOrder.getAveragePrice()).isEqualTo(UPDATED_AVERAGE_PRICE);
        assertThat(testTradingOrder.getCumulativeAmount()).isEqualTo(UPDATED_CUMULATIVE_AMOUNT);

        // Validate the TradingOrder in Elasticsearch
        TradingOrder tradingOrderEs = tradingOrderSearchRepository.findOne(testTradingOrder.getId());
        assertThat(tradingOrderEs).isEqualToComparingFieldByField(testTradingOrder);
    }

    @Test
    @Transactional
    public void updateNonExistingTradingOrder() throws Exception {
        int databaseSizeBeforeUpdate = tradingOrderRepository.findAll().size();

        // Create the TradingOrder

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTradingOrderMockMvc.perform(put("/api/trading-orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingOrder)))
            .andExpect(status().isCreated());

        // Validate the TradingOrder in the database
        List<TradingOrder> tradingOrderList = tradingOrderRepository.findAll();
        assertThat(tradingOrderList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTradingOrder() throws Exception {
        // Initialize the database
        tradingOrderService.save(tradingOrder);

        int databaseSizeBeforeDelete = tradingOrderRepository.findAll().size();

        // Get the tradingOrder
        restTradingOrderMockMvc.perform(delete("/api/trading-orders/{id}", tradingOrder.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tradingOrderExistsInEs = tradingOrderSearchRepository.exists(tradingOrder.getId());
        assertThat(tradingOrderExistsInEs).isFalse();

        // Validate the database is empty
        List<TradingOrder> tradingOrderList = tradingOrderRepository.findAll();
        assertThat(tradingOrderList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTradingOrder() throws Exception {
        // Initialize the database
        tradingOrderService.save(tradingOrder);

        // Search the tradingOrder
        restTradingOrderMockMvc.perform(get("/api/_search/trading-orders?query=id:" + tradingOrder.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tradingOrder.getId().intValue())))
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(DEFAULT_UUID.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].originalAmount").value(hasItem(DEFAULT_ORIGINAL_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].averagePrice").value(hasItem(DEFAULT_AVERAGE_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].cumulativeAmount").value(hasItem(DEFAULT_CUMULATIVE_AMOUNT.intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TradingOrder.class);
        TradingOrder tradingOrder1 = new TradingOrder();
        tradingOrder1.setId(1L);
        TradingOrder tradingOrder2 = new TradingOrder();
        tradingOrder2.setId(tradingOrder1.getId());
        assertThat(tradingOrder1).isEqualTo(tradingOrder2);
        tradingOrder2.setId(2L);
        assertThat(tradingOrder1).isNotEqualTo(tradingOrder2);
        tradingOrder1.setId(null);
        assertThat(tradingOrder1).isNotEqualTo(tradingOrder2);
    }
}
