package com.floatware.tokentrade.web.rest;

import com.floatware.tokentrade.TokentradeApp;

import com.floatware.tokentrade.domain.TradingOrderPackage;
import com.floatware.tokentrade.repository.TradingOrderPackageRepository;
import com.floatware.tokentrade.service.TradingOrderPackageService;
import com.floatware.tokentrade.repository.search.TradingOrderPackageSearchRepository;
import com.floatware.tokentrade.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TradingOrderPackageResource REST controller.
 *
 * @see TradingOrderPackageResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TokentradeApp.class)
public class TradingOrderPackageResourceIntTest {

    private static final String DEFAULT_UUID = "AAAAAAAAAA";
    private static final String UPDATED_UUID = "BBBBBBBBBB";

    @Autowired
    private TradingOrderPackageRepository tradingOrderPackageRepository;

    @Autowired
    private TradingOrderPackageService tradingOrderPackageService;

    @Autowired
    private TradingOrderPackageSearchRepository tradingOrderPackageSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTradingOrderPackageMockMvc;

    private TradingOrderPackage tradingOrderPackage;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TradingOrderPackageResource tradingOrderPackageResource = new TradingOrderPackageResource(tradingOrderPackageService);
        this.restTradingOrderPackageMockMvc = MockMvcBuilders.standaloneSetup(tradingOrderPackageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TradingOrderPackage createEntity(EntityManager em) {
        TradingOrderPackage tradingOrderPackage = new TradingOrderPackage()
            .uuid(DEFAULT_UUID);
        return tradingOrderPackage;
    }

    @Before
    public void initTest() {
        tradingOrderPackageSearchRepository.deleteAll();
        tradingOrderPackage = createEntity(em);
    }

    @Test
    @Transactional
    public void createTradingOrderPackage() throws Exception {
        int databaseSizeBeforeCreate = tradingOrderPackageRepository.findAll().size();

        // Create the TradingOrderPackage
        restTradingOrderPackageMockMvc.perform(post("/api/trading-order-packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingOrderPackage)))
            .andExpect(status().isCreated());

        // Validate the TradingOrderPackage in the database
        List<TradingOrderPackage> tradingOrderPackageList = tradingOrderPackageRepository.findAll();
        assertThat(tradingOrderPackageList).hasSize(databaseSizeBeforeCreate + 1);
        TradingOrderPackage testTradingOrderPackage = tradingOrderPackageList.get(tradingOrderPackageList.size() - 1);
        assertThat(testTradingOrderPackage.getUuid()).isEqualTo(DEFAULT_UUID);

        // Validate the TradingOrderPackage in Elasticsearch
        TradingOrderPackage tradingOrderPackageEs = tradingOrderPackageSearchRepository.findOne(testTradingOrderPackage.getId());
        assertThat(tradingOrderPackageEs).isEqualToComparingFieldByField(testTradingOrderPackage);
    }

    @Test
    @Transactional
    public void createTradingOrderPackageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tradingOrderPackageRepository.findAll().size();

        // Create the TradingOrderPackage with an existing ID
        tradingOrderPackage.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTradingOrderPackageMockMvc.perform(post("/api/trading-order-packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingOrderPackage)))
            .andExpect(status().isBadRequest());

        // Validate the TradingOrderPackage in the database
        List<TradingOrderPackage> tradingOrderPackageList = tradingOrderPackageRepository.findAll();
        assertThat(tradingOrderPackageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkUuidIsRequired() throws Exception {
        int databaseSizeBeforeTest = tradingOrderPackageRepository.findAll().size();
        // set the field null
        tradingOrderPackage.setUuid(null);

        // Create the TradingOrderPackage, which fails.

        restTradingOrderPackageMockMvc.perform(post("/api/trading-order-packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingOrderPackage)))
            .andExpect(status().isBadRequest());

        List<TradingOrderPackage> tradingOrderPackageList = tradingOrderPackageRepository.findAll();
        assertThat(tradingOrderPackageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTradingOrderPackages() throws Exception {
        // Initialize the database
        tradingOrderPackageRepository.saveAndFlush(tradingOrderPackage);

        // Get all the tradingOrderPackageList
        restTradingOrderPackageMockMvc.perform(get("/api/trading-order-packages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tradingOrderPackage.getId().intValue())))
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(DEFAULT_UUID.toString())));
    }

    @Test
    @Transactional
    public void getTradingOrderPackage() throws Exception {
        // Initialize the database
        tradingOrderPackageRepository.saveAndFlush(tradingOrderPackage);

        // Get the tradingOrderPackage
        restTradingOrderPackageMockMvc.perform(get("/api/trading-order-packages/{id}", tradingOrderPackage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tradingOrderPackage.getId().intValue()))
            .andExpect(jsonPath("$.uuid").value(DEFAULT_UUID.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTradingOrderPackage() throws Exception {
        // Get the tradingOrderPackage
        restTradingOrderPackageMockMvc.perform(get("/api/trading-order-packages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTradingOrderPackage() throws Exception {
        // Initialize the database
        tradingOrderPackageService.save(tradingOrderPackage);

        int databaseSizeBeforeUpdate = tradingOrderPackageRepository.findAll().size();

        // Update the tradingOrderPackage
        TradingOrderPackage updatedTradingOrderPackage = tradingOrderPackageRepository.findOne(tradingOrderPackage.getId());
        updatedTradingOrderPackage
            .uuid(UPDATED_UUID);

        restTradingOrderPackageMockMvc.perform(put("/api/trading-order-packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTradingOrderPackage)))
            .andExpect(status().isOk());

        // Validate the TradingOrderPackage in the database
        List<TradingOrderPackage> tradingOrderPackageList = tradingOrderPackageRepository.findAll();
        assertThat(tradingOrderPackageList).hasSize(databaseSizeBeforeUpdate);
        TradingOrderPackage testTradingOrderPackage = tradingOrderPackageList.get(tradingOrderPackageList.size() - 1);
        assertThat(testTradingOrderPackage.getUuid()).isEqualTo(UPDATED_UUID);

        // Validate the TradingOrderPackage in Elasticsearch
        TradingOrderPackage tradingOrderPackageEs = tradingOrderPackageSearchRepository.findOne(testTradingOrderPackage.getId());
        assertThat(tradingOrderPackageEs).isEqualToComparingFieldByField(testTradingOrderPackage);
    }

    @Test
    @Transactional
    public void updateNonExistingTradingOrderPackage() throws Exception {
        int databaseSizeBeforeUpdate = tradingOrderPackageRepository.findAll().size();

        // Create the TradingOrderPackage

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTradingOrderPackageMockMvc.perform(put("/api/trading-order-packages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingOrderPackage)))
            .andExpect(status().isCreated());

        // Validate the TradingOrderPackage in the database
        List<TradingOrderPackage> tradingOrderPackageList = tradingOrderPackageRepository.findAll();
        assertThat(tradingOrderPackageList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTradingOrderPackage() throws Exception {
        // Initialize the database
        tradingOrderPackageService.save(tradingOrderPackage);

        int databaseSizeBeforeDelete = tradingOrderPackageRepository.findAll().size();

        // Get the tradingOrderPackage
        restTradingOrderPackageMockMvc.perform(delete("/api/trading-order-packages/{id}", tradingOrderPackage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tradingOrderPackageExistsInEs = tradingOrderPackageSearchRepository.exists(tradingOrderPackage.getId());
        assertThat(tradingOrderPackageExistsInEs).isFalse();

        // Validate the database is empty
        List<TradingOrderPackage> tradingOrderPackageList = tradingOrderPackageRepository.findAll();
        assertThat(tradingOrderPackageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTradingOrderPackage() throws Exception {
        // Initialize the database
        tradingOrderPackageService.save(tradingOrderPackage);

        // Search the tradingOrderPackage
        restTradingOrderPackageMockMvc.perform(get("/api/_search/trading-order-packages?query=id:" + tradingOrderPackage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tradingOrderPackage.getId().intValue())))
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(DEFAULT_UUID.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TradingOrderPackage.class);
        TradingOrderPackage tradingOrderPackage1 = new TradingOrderPackage();
        tradingOrderPackage1.setId(1L);
        TradingOrderPackage tradingOrderPackage2 = new TradingOrderPackage();
        tradingOrderPackage2.setId(tradingOrderPackage1.getId());
        assertThat(tradingOrderPackage1).isEqualTo(tradingOrderPackage2);
        tradingOrderPackage2.setId(2L);
        assertThat(tradingOrderPackage1).isNotEqualTo(tradingOrderPackage2);
        tradingOrderPackage1.setId(null);
        assertThat(tradingOrderPackage1).isNotEqualTo(tradingOrderPackage2);
    }
}
