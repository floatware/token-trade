package com.floatware.tokentrade.web.rest;

import com.floatware.tokentrade.TokentradeApp;

import com.floatware.tokentrade.domain.MarketAccount;
import com.floatware.tokentrade.repository.MarketAccountRepository;
import com.floatware.tokentrade.service.MarketAccountService;
import com.floatware.tokentrade.repository.search.MarketAccountSearchRepository;
import com.floatware.tokentrade.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.floatware.tokentrade.domain.enumeration.MarketAccountType;
/**
 * Test class for the MarketAccountResource REST controller.
 *
 * @see MarketAccountResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TokentradeApp.class)
public class MarketAccountResourceIntTest {

    private static final MarketAccountType DEFAULT_MARKET_ACCOUNT_TYPE = MarketAccountType.PRIMARY;
    private static final MarketAccountType UPDATED_MARKET_ACCOUNT_TYPE = MarketAccountType.SECONDARY;

    private static final String DEFAULT_USER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_USER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    private static final String DEFAULT_API_KEY = "AAAAAAAAAA";
    private static final String UPDATED_API_KEY = "BBBBBBBBBB";

    private static final String DEFAULT_SECRET_KEY = "AAAAAAAAAA";
    private static final String UPDATED_SECRET_KEY = "BBBBBBBBBB";

    @Autowired
    private MarketAccountRepository marketAccountRepository;

    @Autowired
    private MarketAccountService marketAccountService;

    @Autowired
    private MarketAccountSearchRepository marketAccountSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMarketAccountMockMvc;

    private MarketAccount marketAccount;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MarketAccountResource marketAccountResource = new MarketAccountResource(marketAccountService);
        this.restMarketAccountMockMvc = MockMvcBuilders.standaloneSetup(marketAccountResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MarketAccount createEntity(EntityManager em) {
        MarketAccount marketAccount = new MarketAccount()
            .marketAccountType(DEFAULT_MARKET_ACCOUNT_TYPE)
            .userName(DEFAULT_USER_NAME)
            .password(DEFAULT_PASSWORD)
            .apiKey(DEFAULT_API_KEY)
            .secretKey(DEFAULT_SECRET_KEY);
        return marketAccount;
    }

    @Before
    public void initTest() {
        marketAccountSearchRepository.deleteAll();
        marketAccount = createEntity(em);
    }

    @Test
    @Transactional
    public void createMarketAccount() throws Exception {
        int databaseSizeBeforeCreate = marketAccountRepository.findAll().size();

        // Create the MarketAccount
        restMarketAccountMockMvc.perform(post("/api/market-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(marketAccount)))
            .andExpect(status().isCreated());

        // Validate the MarketAccount in the database
        List<MarketAccount> marketAccountList = marketAccountRepository.findAll();
        assertThat(marketAccountList).hasSize(databaseSizeBeforeCreate + 1);
        MarketAccount testMarketAccount = marketAccountList.get(marketAccountList.size() - 1);
        assertThat(testMarketAccount.getMarketAccountType()).isEqualTo(DEFAULT_MARKET_ACCOUNT_TYPE);
        assertThat(testMarketAccount.getUserName()).isEqualTo(DEFAULT_USER_NAME);
        assertThat(testMarketAccount.getPassword()).isEqualTo(DEFAULT_PASSWORD);
        assertThat(testMarketAccount.getApiKey()).isEqualTo(DEFAULT_API_KEY);
        assertThat(testMarketAccount.getSecretKey()).isEqualTo(DEFAULT_SECRET_KEY);

        // Validate the MarketAccount in Elasticsearch
        MarketAccount marketAccountEs = marketAccountSearchRepository.findOne(testMarketAccount.getId());
        assertThat(marketAccountEs).isEqualToComparingFieldByField(testMarketAccount);
    }

    @Test
    @Transactional
    public void createMarketAccountWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = marketAccountRepository.findAll().size();

        // Create the MarketAccount with an existing ID
        marketAccount.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMarketAccountMockMvc.perform(post("/api/market-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(marketAccount)))
            .andExpect(status().isBadRequest());

        // Validate the MarketAccount in the database
        List<MarketAccount> marketAccountList = marketAccountRepository.findAll();
        assertThat(marketAccountList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkMarketAccountTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = marketAccountRepository.findAll().size();
        // set the field null
        marketAccount.setMarketAccountType(null);

        // Create the MarketAccount, which fails.

        restMarketAccountMockMvc.perform(post("/api/market-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(marketAccount)))
            .andExpect(status().isBadRequest());

        List<MarketAccount> marketAccountList = marketAccountRepository.findAll();
        assertThat(marketAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkApiKeyIsRequired() throws Exception {
        int databaseSizeBeforeTest = marketAccountRepository.findAll().size();
        // set the field null
        marketAccount.setApiKey(null);

        // Create the MarketAccount, which fails.

        restMarketAccountMockMvc.perform(post("/api/market-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(marketAccount)))
            .andExpect(status().isBadRequest());

        List<MarketAccount> marketAccountList = marketAccountRepository.findAll();
        assertThat(marketAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSecretKeyIsRequired() throws Exception {
        int databaseSizeBeforeTest = marketAccountRepository.findAll().size();
        // set the field null
        marketAccount.setSecretKey(null);

        // Create the MarketAccount, which fails.

        restMarketAccountMockMvc.perform(post("/api/market-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(marketAccount)))
            .andExpect(status().isBadRequest());

        List<MarketAccount> marketAccountList = marketAccountRepository.findAll();
        assertThat(marketAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMarketAccounts() throws Exception {
        // Initialize the database
        marketAccountRepository.saveAndFlush(marketAccount);

        // Get all the marketAccountList
        restMarketAccountMockMvc.perform(get("/api/market-accounts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(marketAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].marketAccountType").value(hasItem(DEFAULT_MARKET_ACCOUNT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].userName").value(hasItem(DEFAULT_USER_NAME.toString())))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD.toString())))
            .andExpect(jsonPath("$.[*].apiKey").value(hasItem(DEFAULT_API_KEY.toString())))
            .andExpect(jsonPath("$.[*].secretKey").value(hasItem(DEFAULT_SECRET_KEY.toString())));
    }

    @Test
    @Transactional
    public void getMarketAccount() throws Exception {
        // Initialize the database
        marketAccountRepository.saveAndFlush(marketAccount);

        // Get the marketAccount
        restMarketAccountMockMvc.perform(get("/api/market-accounts/{id}", marketAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(marketAccount.getId().intValue()))
            .andExpect(jsonPath("$.marketAccountType").value(DEFAULT_MARKET_ACCOUNT_TYPE.toString()))
            .andExpect(jsonPath("$.userName").value(DEFAULT_USER_NAME.toString()))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD.toString()))
            .andExpect(jsonPath("$.apiKey").value(DEFAULT_API_KEY.toString()))
            .andExpect(jsonPath("$.secretKey").value(DEFAULT_SECRET_KEY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMarketAccount() throws Exception {
        // Get the marketAccount
        restMarketAccountMockMvc.perform(get("/api/market-accounts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMarketAccount() throws Exception {
        // Initialize the database
        marketAccountService.save(marketAccount);

        int databaseSizeBeforeUpdate = marketAccountRepository.findAll().size();

        // Update the marketAccount
        MarketAccount updatedMarketAccount = marketAccountRepository.findOne(marketAccount.getId());
        updatedMarketAccount
            .marketAccountType(UPDATED_MARKET_ACCOUNT_TYPE)
            .userName(UPDATED_USER_NAME)
            .password(UPDATED_PASSWORD)
            .apiKey(UPDATED_API_KEY)
            .secretKey(UPDATED_SECRET_KEY);

        restMarketAccountMockMvc.perform(put("/api/market-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMarketAccount)))
            .andExpect(status().isOk());

        // Validate the MarketAccount in the database
        List<MarketAccount> marketAccountList = marketAccountRepository.findAll();
        assertThat(marketAccountList).hasSize(databaseSizeBeforeUpdate);
        MarketAccount testMarketAccount = marketAccountList.get(marketAccountList.size() - 1);
        assertThat(testMarketAccount.getMarketAccountType()).isEqualTo(UPDATED_MARKET_ACCOUNT_TYPE);
        assertThat(testMarketAccount.getUserName()).isEqualTo(UPDATED_USER_NAME);
        assertThat(testMarketAccount.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testMarketAccount.getApiKey()).isEqualTo(UPDATED_API_KEY);
        assertThat(testMarketAccount.getSecretKey()).isEqualTo(UPDATED_SECRET_KEY);

        // Validate the MarketAccount in Elasticsearch
        MarketAccount marketAccountEs = marketAccountSearchRepository.findOne(testMarketAccount.getId());
        assertThat(marketAccountEs).isEqualToComparingFieldByField(testMarketAccount);
    }

    @Test
    @Transactional
    public void updateNonExistingMarketAccount() throws Exception {
        int databaseSizeBeforeUpdate = marketAccountRepository.findAll().size();

        // Create the MarketAccount

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMarketAccountMockMvc.perform(put("/api/market-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(marketAccount)))
            .andExpect(status().isCreated());

        // Validate the MarketAccount in the database
        List<MarketAccount> marketAccountList = marketAccountRepository.findAll();
        assertThat(marketAccountList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMarketAccount() throws Exception {
        // Initialize the database
        marketAccountService.save(marketAccount);

        int databaseSizeBeforeDelete = marketAccountRepository.findAll().size();

        // Get the marketAccount
        restMarketAccountMockMvc.perform(delete("/api/market-accounts/{id}", marketAccount.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean marketAccountExistsInEs = marketAccountSearchRepository.exists(marketAccount.getId());
        assertThat(marketAccountExistsInEs).isFalse();

        // Validate the database is empty
        List<MarketAccount> marketAccountList = marketAccountRepository.findAll();
        assertThat(marketAccountList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchMarketAccount() throws Exception {
        // Initialize the database
        marketAccountService.save(marketAccount);

        // Search the marketAccount
        restMarketAccountMockMvc.perform(get("/api/_search/market-accounts?query=id:" + marketAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(marketAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].marketAccountType").value(hasItem(DEFAULT_MARKET_ACCOUNT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].userName").value(hasItem(DEFAULT_USER_NAME.toString())))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD.toString())))
            .andExpect(jsonPath("$.[*].apiKey").value(hasItem(DEFAULT_API_KEY.toString())))
            .andExpect(jsonPath("$.[*].secretKey").value(hasItem(DEFAULT_SECRET_KEY.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MarketAccount.class);
        MarketAccount marketAccount1 = new MarketAccount();
        marketAccount1.setId(1L);
        MarketAccount marketAccount2 = new MarketAccount();
        marketAccount2.setId(marketAccount1.getId());
        assertThat(marketAccount1).isEqualTo(marketAccount2);
        marketAccount2.setId(2L);
        assertThat(marketAccount1).isNotEqualTo(marketAccount2);
        marketAccount1.setId(null);
        assertThat(marketAccount1).isNotEqualTo(marketAccount2);
    }
}
