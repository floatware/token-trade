package com.floatware.tokentrade.web.rest;

import com.floatware.tokentrade.TokentradeApp;

import com.floatware.tokentrade.domain.TradingPlan;
import com.floatware.tokentrade.repository.TradingPlanRepository;
import com.floatware.tokentrade.service.TradingPlanService;
import com.floatware.tokentrade.repository.search.TradingPlanSearchRepository;
import com.floatware.tokentrade.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.floatware.tokentrade.domain.enumeration.TradingType;
/**
 * Test class for the TradingPlanResource REST controller.
 *
 * @see TradingPlanResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TokentradeApp.class)
public class TradingPlanResourceIntTest {

    private static final String DEFAULT_UUID = "AAAAAAAAAA";
    private static final String UPDATED_UUID = "BBBBBBBBBB";

    private static final TradingType DEFAULT_TRADING_TYPE = TradingType.BID;
    private static final TradingType UPDATED_TRADING_TYPE = TradingType.ASK;

    @Autowired
    private TradingPlanRepository tradingPlanRepository;

    @Autowired
    private TradingPlanService tradingPlanService;

    @Autowired
    private TradingPlanSearchRepository tradingPlanSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTradingPlanMockMvc;

    private TradingPlan tradingPlan;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TradingPlanResource tradingPlanResource = new TradingPlanResource(tradingPlanService);
        this.restTradingPlanMockMvc = MockMvcBuilders.standaloneSetup(tradingPlanResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TradingPlan createEntity(EntityManager em) {
        TradingPlan tradingPlan = new TradingPlan()
            .uuid(DEFAULT_UUID)
            .tradingType(DEFAULT_TRADING_TYPE);
        return tradingPlan;
    }

    @Before
    public void initTest() {
        tradingPlanSearchRepository.deleteAll();
        tradingPlan = createEntity(em);
    }

    @Test
    @Transactional
    public void createTradingPlan() throws Exception {
        int databaseSizeBeforeCreate = tradingPlanRepository.findAll().size();

        // Create the TradingPlan
        restTradingPlanMockMvc.perform(post("/api/trading-plans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingPlan)))
            .andExpect(status().isCreated());

        // Validate the TradingPlan in the database
        List<TradingPlan> tradingPlanList = tradingPlanRepository.findAll();
        assertThat(tradingPlanList).hasSize(databaseSizeBeforeCreate + 1);
        TradingPlan testTradingPlan = tradingPlanList.get(tradingPlanList.size() - 1);
        assertThat(testTradingPlan.getUuid()).isEqualTo(DEFAULT_UUID);
        assertThat(testTradingPlan.getTradingType()).isEqualTo(DEFAULT_TRADING_TYPE);

        // Validate the TradingPlan in Elasticsearch
        TradingPlan tradingPlanEs = tradingPlanSearchRepository.findOne(testTradingPlan.getId());
        assertThat(tradingPlanEs).isEqualToComparingFieldByField(testTradingPlan);
    }

    @Test
    @Transactional
    public void createTradingPlanWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tradingPlanRepository.findAll().size();

        // Create the TradingPlan with an existing ID
        tradingPlan.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTradingPlanMockMvc.perform(post("/api/trading-plans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingPlan)))
            .andExpect(status().isBadRequest());

        // Validate the TradingPlan in the database
        List<TradingPlan> tradingPlanList = tradingPlanRepository.findAll();
        assertThat(tradingPlanList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkUuidIsRequired() throws Exception {
        int databaseSizeBeforeTest = tradingPlanRepository.findAll().size();
        // set the field null
        tradingPlan.setUuid(null);

        // Create the TradingPlan, which fails.

        restTradingPlanMockMvc.perform(post("/api/trading-plans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingPlan)))
            .andExpect(status().isBadRequest());

        List<TradingPlan> tradingPlanList = tradingPlanRepository.findAll();
        assertThat(tradingPlanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTradingTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = tradingPlanRepository.findAll().size();
        // set the field null
        tradingPlan.setTradingType(null);

        // Create the TradingPlan, which fails.

        restTradingPlanMockMvc.perform(post("/api/trading-plans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingPlan)))
            .andExpect(status().isBadRequest());

        List<TradingPlan> tradingPlanList = tradingPlanRepository.findAll();
        assertThat(tradingPlanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTradingPlans() throws Exception {
        // Initialize the database
        tradingPlanRepository.saveAndFlush(tradingPlan);

        // Get all the tradingPlanList
        restTradingPlanMockMvc.perform(get("/api/trading-plans?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tradingPlan.getId().intValue())))
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(DEFAULT_UUID.toString())))
            .andExpect(jsonPath("$.[*].tradingType").value(hasItem(DEFAULT_TRADING_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getTradingPlan() throws Exception {
        // Initialize the database
        tradingPlanRepository.saveAndFlush(tradingPlan);

        // Get the tradingPlan
        restTradingPlanMockMvc.perform(get("/api/trading-plans/{id}", tradingPlan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tradingPlan.getId().intValue()))
            .andExpect(jsonPath("$.uuid").value(DEFAULT_UUID.toString()))
            .andExpect(jsonPath("$.tradingType").value(DEFAULT_TRADING_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTradingPlan() throws Exception {
        // Get the tradingPlan
        restTradingPlanMockMvc.perform(get("/api/trading-plans/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTradingPlan() throws Exception {
        // Initialize the database
        tradingPlanService.save(tradingPlan);

        int databaseSizeBeforeUpdate = tradingPlanRepository.findAll().size();

        // Update the tradingPlan
        TradingPlan updatedTradingPlan = tradingPlanRepository.findOne(tradingPlan.getId());
        updatedTradingPlan
            .uuid(UPDATED_UUID)
            .tradingType(UPDATED_TRADING_TYPE);

        restTradingPlanMockMvc.perform(put("/api/trading-plans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTradingPlan)))
            .andExpect(status().isOk());

        // Validate the TradingPlan in the database
        List<TradingPlan> tradingPlanList = tradingPlanRepository.findAll();
        assertThat(tradingPlanList).hasSize(databaseSizeBeforeUpdate);
        TradingPlan testTradingPlan = tradingPlanList.get(tradingPlanList.size() - 1);
        assertThat(testTradingPlan.getUuid()).isEqualTo(UPDATED_UUID);
        assertThat(testTradingPlan.getTradingType()).isEqualTo(UPDATED_TRADING_TYPE);

        // Validate the TradingPlan in Elasticsearch
        TradingPlan tradingPlanEs = tradingPlanSearchRepository.findOne(testTradingPlan.getId());
        assertThat(tradingPlanEs).isEqualToComparingFieldByField(testTradingPlan);
    }

    @Test
    @Transactional
    public void updateNonExistingTradingPlan() throws Exception {
        int databaseSizeBeforeUpdate = tradingPlanRepository.findAll().size();

        // Create the TradingPlan

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTradingPlanMockMvc.perform(put("/api/trading-plans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tradingPlan)))
            .andExpect(status().isCreated());

        // Validate the TradingPlan in the database
        List<TradingPlan> tradingPlanList = tradingPlanRepository.findAll();
        assertThat(tradingPlanList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTradingPlan() throws Exception {
        // Initialize the database
        tradingPlanService.save(tradingPlan);

        int databaseSizeBeforeDelete = tradingPlanRepository.findAll().size();

        // Get the tradingPlan
        restTradingPlanMockMvc.perform(delete("/api/trading-plans/{id}", tradingPlan.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tradingPlanExistsInEs = tradingPlanSearchRepository.exists(tradingPlan.getId());
        assertThat(tradingPlanExistsInEs).isFalse();

        // Validate the database is empty
        List<TradingPlan> tradingPlanList = tradingPlanRepository.findAll();
        assertThat(tradingPlanList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTradingPlan() throws Exception {
        // Initialize the database
        tradingPlanService.save(tradingPlan);

        // Search the tradingPlan
        restTradingPlanMockMvc.perform(get("/api/_search/trading-plans?query=id:" + tradingPlan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tradingPlan.getId().intValue())))
            .andExpect(jsonPath("$.[*].uuid").value(hasItem(DEFAULT_UUID.toString())))
            .andExpect(jsonPath("$.[*].tradingType").value(hasItem(DEFAULT_TRADING_TYPE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TradingPlan.class);
        TradingPlan tradingPlan1 = new TradingPlan();
        tradingPlan1.setId(1L);
        TradingPlan tradingPlan2 = new TradingPlan();
        tradingPlan2.setId(tradingPlan1.getId());
        assertThat(tradingPlan1).isEqualTo(tradingPlan2);
        tradingPlan2.setId(2L);
        assertThat(tradingPlan1).isNotEqualTo(tradingPlan2);
        tradingPlan1.setId(null);
        assertThat(tradingPlan1).isNotEqualTo(tradingPlan2);
    }
}
