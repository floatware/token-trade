import { BaseEntity } from './../../shared';

export const enum ClientStatus {
    'ACTIVE',
    'INACTIVE',
    'INTERN'
}

export class Client implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public email?: string,
        public status?: ClientStatus,
        public tenant?: BaseEntity,
        public clientGroups?: BaseEntity[],
    ) {
    }
}
