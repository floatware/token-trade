import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AlertEventComponent } from './alert-event.component';
import { AlertEventDetailComponent } from './alert-event-detail.component';
import { AlertEventPopupComponent } from './alert-event-dialog.component';
import { AlertEventDeletePopupComponent } from './alert-event-delete-dialog.component';

export const alertEventRoute: Routes = [
    {
        path: 'alert-event',
        component: AlertEventComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.alertEvent.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'alert-event/:id',
        component: AlertEventDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.alertEvent.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const alertEventPopupRoute: Routes = [
    {
        path: 'alert-event-new',
        component: AlertEventPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.alertEvent.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'alert-event/:id/edit',
        component: AlertEventPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.alertEvent.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'alert-event/:id/delete',
        component: AlertEventDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.alertEvent.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
