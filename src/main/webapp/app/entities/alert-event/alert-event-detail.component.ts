import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { AlertEvent } from './alert-event.model';
import { AlertEventService } from './alert-event.service';

@Component({
    selector: 'jhi-alert-event-detail',
    templateUrl: './alert-event-detail.component.html'
})
export class AlertEventDetailComponent implements OnInit, OnDestroy {

    alertEvent: AlertEvent;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private alertEventService: AlertEventService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAlertEvents();
    }

    load(id) {
        this.alertEventService.find(id).subscribe((alertEvent) => {
            this.alertEvent = alertEvent;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAlertEvents() {
        this.eventSubscriber = this.eventManager.subscribe(
            'alertEventListModification',
            (response) => this.load(this.alertEvent.id)
        );
    }
}
