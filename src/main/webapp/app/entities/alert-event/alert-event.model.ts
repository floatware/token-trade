import { BaseEntity } from './../../shared';

export class AlertEvent implements BaseEntity {
    constructor(
        public id?: number,
        public currentPrice?: number,
        public alertPrice?: number,
        public fired?: boolean,
        public note?: string,
        public asset?: BaseEntity,
    ) {
        this.fired = false;
    }
}
