import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AlertEvent } from './alert-event.model';
import { AlertEventPopupService } from './alert-event-popup.service';
import { AlertEventService } from './alert-event.service';
import { Asset, AssetService } from '../asset';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-alert-event-dialog',
    templateUrl: './alert-event-dialog.component.html'
})
export class AlertEventDialogComponent implements OnInit {

    alertEvent: AlertEvent;
    isSaving: boolean;

    assets: Asset[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private alertEventService: AlertEventService,
        private assetService: AssetService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.assetService.query()
            .subscribe((res: ResponseWrapper) => { this.assets = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.alertEvent.id !== undefined) {
            this.subscribeToSaveResponse(
                this.alertEventService.update(this.alertEvent));
        } else {
            this.subscribeToSaveResponse(
                this.alertEventService.create(this.alertEvent));
        }
    }

    private subscribeToSaveResponse(result: Observable<AlertEvent>) {
        result.subscribe((res: AlertEvent) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: AlertEvent) {
        this.eventManager.broadcast({ name: 'alertEventListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAssetById(index: number, item: Asset) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-alert-event-popup',
    template: ''
})
export class AlertEventPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private alertEventPopupService: AlertEventPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.alertEventPopupService
                    .open(AlertEventDialogComponent as Component, params['id']);
            } else {
                this.alertEventPopupService
                    .open(AlertEventDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
