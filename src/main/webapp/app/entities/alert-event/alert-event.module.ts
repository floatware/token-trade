import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TokentradeSharedModule } from '../../shared';
import {
    AlertEventService,
    AlertEventPopupService,
    AlertEventComponent,
    AlertEventDetailComponent,
    AlertEventDialogComponent,
    AlertEventPopupComponent,
    AlertEventDeletePopupComponent,
    AlertEventDeleteDialogComponent,
    alertEventRoute,
    alertEventPopupRoute,
} from './';

const ENTITY_STATES = [
    ...alertEventRoute,
    ...alertEventPopupRoute,
];

@NgModule({
    imports: [
        TokentradeSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AlertEventComponent,
        AlertEventDetailComponent,
        AlertEventDialogComponent,
        AlertEventDeleteDialogComponent,
        AlertEventPopupComponent,
        AlertEventDeletePopupComponent,
    ],
    entryComponents: [
        AlertEventComponent,
        AlertEventDialogComponent,
        AlertEventPopupComponent,
        AlertEventDeleteDialogComponent,
        AlertEventDeletePopupComponent,
    ],
    providers: [
        AlertEventService,
        AlertEventPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TokentradeAlertEventModule {}
