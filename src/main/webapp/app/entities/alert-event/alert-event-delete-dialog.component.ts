import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AlertEvent } from './alert-event.model';
import { AlertEventPopupService } from './alert-event-popup.service';
import { AlertEventService } from './alert-event.service';

@Component({
    selector: 'jhi-alert-event-delete-dialog',
    templateUrl: './alert-event-delete-dialog.component.html'
})
export class AlertEventDeleteDialogComponent {

    alertEvent: AlertEvent;

    constructor(
        private alertEventService: AlertEventService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.alertEventService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'alertEventListModification',
                content: 'Deleted an alertEvent'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-alert-event-delete-popup',
    template: ''
})
export class AlertEventDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private alertEventPopupService: AlertEventPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.alertEventPopupService
                .open(AlertEventDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
