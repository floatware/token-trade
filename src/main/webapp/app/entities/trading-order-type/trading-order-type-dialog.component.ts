import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TradingOrderType } from './trading-order-type.model';
import { TradingOrderTypePopupService } from './trading-order-type-popup.service';
import { TradingOrderTypeService } from './trading-order-type.service';
import { Market, MarketService } from '../market';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-trading-order-type-dialog',
    templateUrl: './trading-order-type-dialog.component.html'
})
export class TradingOrderTypeDialogComponent implements OnInit {

    tradingOrderType: TradingOrderType;
    isSaving: boolean;

    markets: Market[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private tradingOrderTypeService: TradingOrderTypeService,
        private marketService: MarketService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.marketService.query()
            .subscribe((res: ResponseWrapper) => { this.markets = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.tradingOrderType.id !== undefined) {
            this.subscribeToSaveResponse(
                this.tradingOrderTypeService.update(this.tradingOrderType));
        } else {
            this.subscribeToSaveResponse(
                this.tradingOrderTypeService.create(this.tradingOrderType));
        }
    }

    private subscribeToSaveResponse(result: Observable<TradingOrderType>) {
        result.subscribe((res: TradingOrderType) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: TradingOrderType) {
        this.eventManager.broadcast({ name: 'tradingOrderTypeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackMarketById(index: number, item: Market) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-trading-order-type-popup',
    template: ''
})
export class TradingOrderTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tradingOrderTypePopupService: TradingOrderTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.tradingOrderTypePopupService
                    .open(TradingOrderTypeDialogComponent as Component, params['id']);
            } else {
                this.tradingOrderTypePopupService
                    .open(TradingOrderTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
