import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TokentradeSharedModule } from '../../shared';
import {
    TradingOrderTypeService,
    TradingOrderTypePopupService,
    TradingOrderTypeComponent,
    TradingOrderTypeDetailComponent,
    TradingOrderTypeDialogComponent,
    TradingOrderTypePopupComponent,
    TradingOrderTypeDeletePopupComponent,
    TradingOrderTypeDeleteDialogComponent,
    tradingOrderTypeRoute,
    tradingOrderTypePopupRoute,
} from './';

const ENTITY_STATES = [
    ...tradingOrderTypeRoute,
    ...tradingOrderTypePopupRoute,
];

@NgModule({
    imports: [
        TokentradeSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TradingOrderTypeComponent,
        TradingOrderTypeDetailComponent,
        TradingOrderTypeDialogComponent,
        TradingOrderTypeDeleteDialogComponent,
        TradingOrderTypePopupComponent,
        TradingOrderTypeDeletePopupComponent,
    ],
    entryComponents: [
        TradingOrderTypeComponent,
        TradingOrderTypeDialogComponent,
        TradingOrderTypePopupComponent,
        TradingOrderTypeDeleteDialogComponent,
        TradingOrderTypeDeletePopupComponent,
    ],
    providers: [
        TradingOrderTypeService,
        TradingOrderTypePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TokentradeTradingOrderTypeModule {}
