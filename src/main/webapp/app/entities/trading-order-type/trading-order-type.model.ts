import { BaseEntity } from './../../shared';

export class TradingOrderType implements BaseEntity {
    constructor(
        public id?: number,
        public tradingOrdertype?: string,
        public supportedMarket?: BaseEntity,
    ) {
    }
}
