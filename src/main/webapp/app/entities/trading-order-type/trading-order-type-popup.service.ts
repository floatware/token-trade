import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TradingOrderType } from './trading-order-type.model';
import { TradingOrderTypeService } from './trading-order-type.service';

@Injectable()
export class TradingOrderTypePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private tradingOrderTypeService: TradingOrderTypeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.tradingOrderTypeService.find(id).subscribe((tradingOrderType) => {
                    this.ngbModalRef = this.tradingOrderTypeModalRef(component, tradingOrderType);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.tradingOrderTypeModalRef(component, new TradingOrderType());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    tradingOrderTypeModalRef(component: Component, tradingOrderType: TradingOrderType): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.tradingOrderType = tradingOrderType;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
