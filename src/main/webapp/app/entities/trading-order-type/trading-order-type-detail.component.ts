import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TradingOrderType } from './trading-order-type.model';
import { TradingOrderTypeService } from './trading-order-type.service';

@Component({
    selector: 'jhi-trading-order-type-detail',
    templateUrl: './trading-order-type-detail.component.html'
})
export class TradingOrderTypeDetailComponent implements OnInit, OnDestroy {

    tradingOrderType: TradingOrderType;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private tradingOrderTypeService: TradingOrderTypeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTradingOrderTypes();
    }

    load(id) {
        this.tradingOrderTypeService.find(id).subscribe((tradingOrderType) => {
            this.tradingOrderType = tradingOrderType;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTradingOrderTypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'tradingOrderTypeListModification',
            (response) => this.load(this.tradingOrderType.id)
        );
    }
}
