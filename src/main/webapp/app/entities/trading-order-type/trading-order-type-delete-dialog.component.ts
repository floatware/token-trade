import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TradingOrderType } from './trading-order-type.model';
import { TradingOrderTypePopupService } from './trading-order-type-popup.service';
import { TradingOrderTypeService } from './trading-order-type.service';

@Component({
    selector: 'jhi-trading-order-type-delete-dialog',
    templateUrl: './trading-order-type-delete-dialog.component.html'
})
export class TradingOrderTypeDeleteDialogComponent {

    tradingOrderType: TradingOrderType;

    constructor(
        private tradingOrderTypeService: TradingOrderTypeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.tradingOrderTypeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'tradingOrderTypeListModification',
                content: 'Deleted an tradingOrderType'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-trading-order-type-delete-popup',
    template: ''
})
export class TradingOrderTypeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tradingOrderTypePopupService: TradingOrderTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.tradingOrderTypePopupService
                .open(TradingOrderTypeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
