import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TradingOrderTypeComponent } from './trading-order-type.component';
import { TradingOrderTypeDetailComponent } from './trading-order-type-detail.component';
import { TradingOrderTypePopupComponent } from './trading-order-type-dialog.component';
import { TradingOrderTypeDeletePopupComponent } from './trading-order-type-delete-dialog.component';

export const tradingOrderTypeRoute: Routes = [
    {
        path: 'trading-order-type',
        component: TradingOrderTypeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingOrderType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'trading-order-type/:id',
        component: TradingOrderTypeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingOrderType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const tradingOrderTypePopupRoute: Routes = [
    {
        path: 'trading-order-type-new',
        component: TradingOrderTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingOrderType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'trading-order-type/:id/edit',
        component: TradingOrderTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingOrderType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'trading-order-type/:id/delete',
        component: TradingOrderTypeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingOrderType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
