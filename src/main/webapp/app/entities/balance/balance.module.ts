import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TokentradeSharedModule } from '../../shared';
import {
    BalanceService,
    BalancePopupService,
    BalanceComponent,
    BalanceDetailComponent,
    BalanceDialogComponent,
    BalancePopupComponent,
    BalanceDeletePopupComponent,
    BalanceDeleteDialogComponent,
    balanceRoute,
    balancePopupRoute,
    BalanceResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...balanceRoute,
    ...balancePopupRoute,
];

@NgModule({
    imports: [
        TokentradeSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        BalanceComponent,
        BalanceDetailComponent,
        BalanceDialogComponent,
        BalanceDeleteDialogComponent,
        BalancePopupComponent,
        BalanceDeletePopupComponent,
    ],
    entryComponents: [
        BalanceComponent,
        BalanceDialogComponent,
        BalancePopupComponent,
        BalanceDeleteDialogComponent,
        BalanceDeletePopupComponent,
    ],
    providers: [
        BalanceService,
        BalancePopupService,
        BalanceResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TokentradeBalanceModule {}
