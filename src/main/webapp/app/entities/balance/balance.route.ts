import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { BalanceComponent } from './balance.component';
import { BalanceDetailComponent } from './balance-detail.component';
import { BalancePopupComponent } from './balance-dialog.component';
import { BalanceDeletePopupComponent } from './balance-delete-dialog.component';

@Injectable()
export class BalanceResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const balanceRoute: Routes = [
    {
        path: 'balance',
        component: BalanceComponent,
        resolve: {
            'pagingParams': BalanceResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.balance.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'balance/:id',
        component: BalanceDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.balance.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const balancePopupRoute: Routes = [
    {
        path: 'balance-new',
        component: BalancePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.balance.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'balance/:id/edit',
        component: BalancePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.balance.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'balance/:id/delete',
        component: BalanceDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.balance.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
