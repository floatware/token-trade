import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Balance } from './balance.model';
import { BalancePopupService } from './balance-popup.service';
import { BalanceService } from './balance.service';
import { MarketAccount, MarketAccountService } from '../market-account';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-balance-dialog',
    templateUrl: './balance-dialog.component.html'
})
export class BalanceDialogComponent implements OnInit {

    balance: Balance;
    isSaving: boolean;

    marketaccounts: MarketAccount[];

    balances: Balance[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private balanceService: BalanceService,
        private marketAccountService: MarketAccountService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.marketAccountService.query()
            .subscribe((res: ResponseWrapper) => { this.marketaccounts = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.balanceService.query()
            .subscribe((res: ResponseWrapper) => { this.balances = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.balance.id !== undefined) {
            this.subscribeToSaveResponse(
                this.balanceService.update(this.balance));
        } else {
            this.subscribeToSaveResponse(
                this.balanceService.create(this.balance));
        }
    }

    private subscribeToSaveResponse(result: Observable<Balance>) {
        result.subscribe((res: Balance) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Balance) {
        this.eventManager.broadcast({ name: 'balanceListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackMarketAccountById(index: number, item: MarketAccount) {
        return item.id;
    }

    trackBalanceById(index: number, item: Balance) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-balance-popup',
    template: ''
})
export class BalancePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private balancePopupService: BalancePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.balancePopupService
                    .open(BalanceDialogComponent as Component, params['id']);
            } else {
                this.balancePopupService
                    .open(BalanceDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
