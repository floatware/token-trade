import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Balance } from './balance.model';
import { BalanceService } from './balance.service';

@Component({
    selector: 'jhi-balance-detail',
    templateUrl: './balance-detail.component.html'
})
export class BalanceDetailComponent implements OnInit, OnDestroy {

    balance: Balance;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private balanceService: BalanceService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInBalances();
    }

    load(id) {
        this.balanceService.find(id).subscribe((balance) => {
            this.balance = balance;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBalances() {
        this.eventSubscriber = this.eventManager.subscribe(
            'balanceListModification',
            (response) => this.load(this.balance.id)
        );
    }
}
