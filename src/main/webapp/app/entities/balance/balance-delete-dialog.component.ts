import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Balance } from './balance.model';
import { BalancePopupService } from './balance-popup.service';
import { BalanceService } from './balance.service';

@Component({
    selector: 'jhi-balance-delete-dialog',
    templateUrl: './balance-delete-dialog.component.html'
})
export class BalanceDeleteDialogComponent {

    balance: Balance;

    constructor(
        private balanceService: BalanceService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.balanceService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'balanceListModification',
                content: 'Deleted an balance'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-balance-delete-popup',
    template: ''
})
export class BalanceDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private balancePopupService: BalancePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.balancePopupService
                .open(BalanceDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
