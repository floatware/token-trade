import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Balance } from './balance.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class BalanceService {

    private resourceUrl = SERVER_API_URL + 'api/balances';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/balances';

    constructor(private http: Http) { }

    create(balance: Balance): Observable<Balance> {
        const copy = this.convert(balance);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(balance: Balance): Observable<Balance> {
        const copy = this.convert(balance);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Balance> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Balance.
     */
    private convertItemFromServer(json: any): Balance {
        const entity: Balance = Object.assign(new Balance(), json);
        return entity;
    }

    /**
     * Convert a Balance to a JSON which can be sent to the server.
     */
    private convert(balance: Balance): Balance {
        const copy: Balance = Object.assign({}, balance);
        return copy;
    }
}
