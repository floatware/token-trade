import { BaseEntity } from './../../shared';

export class Balance implements BaseEntity {
    constructor(
        public id?: number,
        public balanceData?: string,
        public marketAccount?: BaseEntity,
        public parent?: BaseEntity,
    ) {
    }
}
