import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TradingPlan } from './trading-plan.model';
import { TradingPlanPopupService } from './trading-plan-popup.service';
import { TradingPlanService } from './trading-plan.service';

@Component({
    selector: 'jhi-trading-plan-delete-dialog',
    templateUrl: './trading-plan-delete-dialog.component.html'
})
export class TradingPlanDeleteDialogComponent {

    tradingPlan: TradingPlan;

    constructor(
        private tradingPlanService: TradingPlanService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.tradingPlanService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'tradingPlanListModification',
                content: 'Deleted an tradingPlan'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-trading-plan-delete-popup',
    template: ''
})
export class TradingPlanDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tradingPlanPopupService: TradingPlanPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.tradingPlanPopupService
                .open(TradingPlanDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
