import { BaseEntity } from './../../shared';

export const enum TradingType {
    'BID',
    'ASK',
    'BID_LIMIT',
    'ASK_LIMIT'
}

export class TradingPlan implements BaseEntity {
    constructor(
        public id?: number,
        public uuid?: string,
        public tradingType?: TradingType,
        public note?: BaseEntity,
        public trader?: BaseEntity,
    ) {
    }
}
