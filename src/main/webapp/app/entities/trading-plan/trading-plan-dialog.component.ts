import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TradingPlan } from './trading-plan.model';
import { TradingPlanPopupService } from './trading-plan-popup.service';
import { TradingPlanService } from './trading-plan.service';
import { Note, NoteService } from '../note';
import { Trader, TraderService } from '../trader';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-trading-plan-dialog',
    templateUrl: './trading-plan-dialog.component.html'
})
export class TradingPlanDialogComponent implements OnInit {

    tradingPlan: TradingPlan;
    isSaving: boolean;

    notes: Note[];

    traders: Trader[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private tradingPlanService: TradingPlanService,
        private noteService: NoteService,
        private traderService: TraderService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.noteService
            .query({filter: 'tradingplan-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.tradingPlan.note || !this.tradingPlan.note.id) {
                    this.notes = res.json;
                } else {
                    this.noteService
                        .find(this.tradingPlan.note.id)
                        .subscribe((subRes: Note) => {
                            this.notes = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.traderService.query()
            .subscribe((res: ResponseWrapper) => { this.traders = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.tradingPlan.id !== undefined) {
            this.subscribeToSaveResponse(
                this.tradingPlanService.update(this.tradingPlan));
        } else {
            this.subscribeToSaveResponse(
                this.tradingPlanService.create(this.tradingPlan));
        }
    }

    private subscribeToSaveResponse(result: Observable<TradingPlan>) {
        result.subscribe((res: TradingPlan) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: TradingPlan) {
        this.eventManager.broadcast({ name: 'tradingPlanListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackNoteById(index: number, item: Note) {
        return item.id;
    }

    trackTraderById(index: number, item: Trader) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-trading-plan-popup',
    template: ''
})
export class TradingPlanPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tradingPlanPopupService: TradingPlanPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.tradingPlanPopupService
                    .open(TradingPlanDialogComponent as Component, params['id']);
            } else {
                this.tradingPlanPopupService
                    .open(TradingPlanDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
