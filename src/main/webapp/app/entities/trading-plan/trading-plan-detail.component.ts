import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TradingPlan } from './trading-plan.model';
import { TradingPlanService } from './trading-plan.service';

@Component({
    selector: 'jhi-trading-plan-detail',
    templateUrl: './trading-plan-detail.component.html'
})
export class TradingPlanDetailComponent implements OnInit, OnDestroy {

    tradingPlan: TradingPlan;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private tradingPlanService: TradingPlanService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTradingPlans();
    }

    load(id) {
        this.tradingPlanService.find(id).subscribe((tradingPlan) => {
            this.tradingPlan = tradingPlan;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTradingPlans() {
        this.eventSubscriber = this.eventManager.subscribe(
            'tradingPlanListModification',
            (response) => this.load(this.tradingPlan.id)
        );
    }
}
