import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TradingPlanComponent } from './trading-plan.component';
import { TradingPlanDetailComponent } from './trading-plan-detail.component';
import { TradingPlanPopupComponent } from './trading-plan-dialog.component';
import { TradingPlanDeletePopupComponent } from './trading-plan-delete-dialog.component';

export const tradingPlanRoute: Routes = [
    {
        path: 'trading-plan',
        component: TradingPlanComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingPlan.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'trading-plan/:id',
        component: TradingPlanDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingPlan.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const tradingPlanPopupRoute: Routes = [
    {
        path: 'trading-plan-new',
        component: TradingPlanPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingPlan.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'trading-plan/:id/edit',
        component: TradingPlanPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingPlan.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'trading-plan/:id/delete',
        component: TradingPlanDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingPlan.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
