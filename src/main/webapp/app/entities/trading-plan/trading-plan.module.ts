import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TokentradeSharedModule } from '../../shared';
import {
    TradingPlanService,
    TradingPlanPopupService,
    TradingPlanComponent,
    TradingPlanDetailComponent,
    TradingPlanDialogComponent,
    TradingPlanPopupComponent,
    TradingPlanDeletePopupComponent,
    TradingPlanDeleteDialogComponent,
    tradingPlanRoute,
    tradingPlanPopupRoute,
} from './';

const ENTITY_STATES = [
    ...tradingPlanRoute,
    ...tradingPlanPopupRoute,
];

@NgModule({
    imports: [
        TokentradeSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TradingPlanComponent,
        TradingPlanDetailComponent,
        TradingPlanDialogComponent,
        TradingPlanDeleteDialogComponent,
        TradingPlanPopupComponent,
        TradingPlanDeletePopupComponent,
    ],
    entryComponents: [
        TradingPlanComponent,
        TradingPlanDialogComponent,
        TradingPlanPopupComponent,
        TradingPlanDeleteDialogComponent,
        TradingPlanDeletePopupComponent,
    ],
    providers: [
        TradingPlanService,
        TradingPlanPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TokentradeTradingPlanModule {}
