import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TokentradeSharedModule } from '../../shared';
import {
    MarketAccountService,
    MarketAccountPopupService,
    MarketAccountComponent,
    MarketAccountDetailComponent,
    MarketAccountDialogComponent,
    MarketAccountPopupComponent,
    MarketAccountDeletePopupComponent,
    MarketAccountDeleteDialogComponent,
    marketAccountRoute,
    marketAccountPopupRoute,
    MarketAccountResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...marketAccountRoute,
    ...marketAccountPopupRoute,
];

@NgModule({
    imports: [
        TokentradeSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        MarketAccountComponent,
        MarketAccountDetailComponent,
        MarketAccountDialogComponent,
        MarketAccountDeleteDialogComponent,
        MarketAccountPopupComponent,
        MarketAccountDeletePopupComponent,
    ],
    entryComponents: [
        MarketAccountComponent,
        MarketAccountDialogComponent,
        MarketAccountPopupComponent,
        MarketAccountDeleteDialogComponent,
        MarketAccountDeletePopupComponent,
    ],
    providers: [
        MarketAccountService,
        MarketAccountPopupService,
        MarketAccountResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TokentradeMarketAccountModule {}
