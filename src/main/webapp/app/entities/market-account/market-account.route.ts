import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { MarketAccountComponent } from './market-account.component';
import { MarketAccountDetailComponent } from './market-account-detail.component';
import { MarketAccountPopupComponent } from './market-account-dialog.component';
import { MarketAccountDeletePopupComponent } from './market-account-delete-dialog.component';

@Injectable()
export class MarketAccountResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const marketAccountRoute: Routes = [
    {
        path: 'market-account',
        component: MarketAccountComponent,
        resolve: {
            'pagingParams': MarketAccountResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.marketAccount.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'market-account/:id',
        component: MarketAccountDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.marketAccount.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const marketAccountPopupRoute: Routes = [
    {
        path: 'market-account-new',
        component: MarketAccountPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.marketAccount.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'market-account/:id/edit',
        component: MarketAccountPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.marketAccount.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'market-account/:id/delete',
        component: MarketAccountDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.marketAccount.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
