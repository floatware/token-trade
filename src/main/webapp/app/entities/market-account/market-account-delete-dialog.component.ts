import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { MarketAccount } from './market-account.model';
import { MarketAccountPopupService } from './market-account-popup.service';
import { MarketAccountService } from './market-account.service';

@Component({
    selector: 'jhi-market-account-delete-dialog',
    templateUrl: './market-account-delete-dialog.component.html'
})
export class MarketAccountDeleteDialogComponent {

    marketAccount: MarketAccount;

    constructor(
        private marketAccountService: MarketAccountService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.marketAccountService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'marketAccountListModification',
                content: 'Deleted an marketAccount'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-market-account-delete-popup',
    template: ''
})
export class MarketAccountDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private marketAccountPopupService: MarketAccountPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.marketAccountPopupService
                .open(MarketAccountDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
