import { BaseEntity } from './../../shared';

export const enum MarketAccountType {
    'PRIMARY',
    'SECONDARY'
}

export class MarketAccount implements BaseEntity {
    constructor(
        public id?: number,
        public marketAccountType?: MarketAccountType,
        public userName?: string,
        public password?: string,
        public apiKey?: string,
        public secretKey?: string,
        public client?: BaseEntity,
        public market?: BaseEntity,
    ) {
    }
}
