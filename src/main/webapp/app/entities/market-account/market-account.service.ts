import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { MarketAccount } from './market-account.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class MarketAccountService {

    private resourceUrl = SERVER_API_URL + 'api/market-accounts';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/market-accounts';

    constructor(private http: Http) { }

    create(marketAccount: MarketAccount): Observable<MarketAccount> {
        const copy = this.convert(marketAccount);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(marketAccount: MarketAccount): Observable<MarketAccount> {
        const copy = this.convert(marketAccount);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<MarketAccount> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to MarketAccount.
     */
    private convertItemFromServer(json: any): MarketAccount {
        const entity: MarketAccount = Object.assign(new MarketAccount(), json);
        return entity;
    }

    /**
     * Convert a MarketAccount to a JSON which can be sent to the server.
     */
    private convert(marketAccount: MarketAccount): MarketAccount {
        const copy: MarketAccount = Object.assign({}, marketAccount);
        return copy;
    }
}
