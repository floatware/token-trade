import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { MarketAccount } from './market-account.model';
import { MarketAccountService } from './market-account.service';

@Component({
    selector: 'jhi-market-account-detail',
    templateUrl: './market-account-detail.component.html'
})
export class MarketAccountDetailComponent implements OnInit, OnDestroy {

    marketAccount: MarketAccount;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private marketAccountService: MarketAccountService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInMarketAccounts();
    }

    load(id) {
        this.marketAccountService.find(id).subscribe((marketAccount) => {
            this.marketAccount = marketAccount;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMarketAccounts() {
        this.eventSubscriber = this.eventManager.subscribe(
            'marketAccountListModification',
            (response) => this.load(this.marketAccount.id)
        );
    }
}
