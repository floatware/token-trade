import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { MarketAccount } from './market-account.model';
import { MarketAccountPopupService } from './market-account-popup.service';
import { MarketAccountService } from './market-account.service';
import { Client, ClientService } from '../client';
import { Market, MarketService } from '../market';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-market-account-dialog',
    templateUrl: './market-account-dialog.component.html'
})
export class MarketAccountDialogComponent implements OnInit {

    marketAccount: MarketAccount;
    isSaving: boolean;

    clients: Client[];

    markets: Market[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private marketAccountService: MarketAccountService,
        private clientService: ClientService,
        private marketService: MarketService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.clientService.query()
            .subscribe((res: ResponseWrapper) => { this.clients = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.marketService.query()
            .subscribe((res: ResponseWrapper) => { this.markets = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.marketAccount.id !== undefined) {
            this.subscribeToSaveResponse(
                this.marketAccountService.update(this.marketAccount));
        } else {
            this.subscribeToSaveResponse(
                this.marketAccountService.create(this.marketAccount));
        }
    }

    private subscribeToSaveResponse(result: Observable<MarketAccount>) {
        result.subscribe((res: MarketAccount) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: MarketAccount) {
        this.eventManager.broadcast({ name: 'marketAccountListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClientById(index: number, item: Client) {
        return item.id;
    }

    trackMarketById(index: number, item: Market) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-market-account-popup',
    template: ''
})
export class MarketAccountPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private marketAccountPopupService: MarketAccountPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.marketAccountPopupService
                    .open(MarketAccountDialogComponent as Component, params['id']);
            } else {
                this.marketAccountPopupService
                    .open(MarketAccountDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
