import { BaseEntity } from './../../shared';

export class TradingOrderPackage implements BaseEntity {
    constructor(
        public id?: number,
        public uuid?: string,
        public tradingPlan?: BaseEntity,
    ) {
    }
}
