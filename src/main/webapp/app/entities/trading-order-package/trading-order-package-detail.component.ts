import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TradingOrderPackage } from './trading-order-package.model';
import { TradingOrderPackageService } from './trading-order-package.service';

@Component({
    selector: 'jhi-trading-order-package-detail',
    templateUrl: './trading-order-package-detail.component.html'
})
export class TradingOrderPackageDetailComponent implements OnInit, OnDestroy {

    tradingOrderPackage: TradingOrderPackage;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private tradingOrderPackageService: TradingOrderPackageService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTradingOrderPackages();
    }

    load(id) {
        this.tradingOrderPackageService.find(id).subscribe((tradingOrderPackage) => {
            this.tradingOrderPackage = tradingOrderPackage;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTradingOrderPackages() {
        this.eventSubscriber = this.eventManager.subscribe(
            'tradingOrderPackageListModification',
            (response) => this.load(this.tradingOrderPackage.id)
        );
    }
}
