import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { TradingOrderPackage } from './trading-order-package.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class TradingOrderPackageService {

    private resourceUrl = SERVER_API_URL + 'api/trading-order-packages';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/trading-order-packages';

    constructor(private http: Http) { }

    create(tradingOrderPackage: TradingOrderPackage): Observable<TradingOrderPackage> {
        const copy = this.convert(tradingOrderPackage);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(tradingOrderPackage: TradingOrderPackage): Observable<TradingOrderPackage> {
        const copy = this.convert(tradingOrderPackage);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<TradingOrderPackage> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to TradingOrderPackage.
     */
    private convertItemFromServer(json: any): TradingOrderPackage {
        const entity: TradingOrderPackage = Object.assign(new TradingOrderPackage(), json);
        return entity;
    }

    /**
     * Convert a TradingOrderPackage to a JSON which can be sent to the server.
     */
    private convert(tradingOrderPackage: TradingOrderPackage): TradingOrderPackage {
        const copy: TradingOrderPackage = Object.assign({}, tradingOrderPackage);
        return copy;
    }
}
