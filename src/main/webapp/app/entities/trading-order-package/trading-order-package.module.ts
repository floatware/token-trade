import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TokentradeSharedModule } from '../../shared';
import {
    TradingOrderPackageService,
    TradingOrderPackagePopupService,
    TradingOrderPackageComponent,
    TradingOrderPackageDetailComponent,
    TradingOrderPackageDialogComponent,
    TradingOrderPackagePopupComponent,
    TradingOrderPackageDeletePopupComponent,
    TradingOrderPackageDeleteDialogComponent,
    tradingOrderPackageRoute,
    tradingOrderPackagePopupRoute,
} from './';

const ENTITY_STATES = [
    ...tradingOrderPackageRoute,
    ...tradingOrderPackagePopupRoute,
];

@NgModule({
    imports: [
        TokentradeSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TradingOrderPackageComponent,
        TradingOrderPackageDetailComponent,
        TradingOrderPackageDialogComponent,
        TradingOrderPackageDeleteDialogComponent,
        TradingOrderPackagePopupComponent,
        TradingOrderPackageDeletePopupComponent,
    ],
    entryComponents: [
        TradingOrderPackageComponent,
        TradingOrderPackageDialogComponent,
        TradingOrderPackagePopupComponent,
        TradingOrderPackageDeleteDialogComponent,
        TradingOrderPackageDeletePopupComponent,
    ],
    providers: [
        TradingOrderPackageService,
        TradingOrderPackagePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TokentradeTradingOrderPackageModule {}
