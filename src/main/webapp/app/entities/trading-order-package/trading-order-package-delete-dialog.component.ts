import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TradingOrderPackage } from './trading-order-package.model';
import { TradingOrderPackagePopupService } from './trading-order-package-popup.service';
import { TradingOrderPackageService } from './trading-order-package.service';

@Component({
    selector: 'jhi-trading-order-package-delete-dialog',
    templateUrl: './trading-order-package-delete-dialog.component.html'
})
export class TradingOrderPackageDeleteDialogComponent {

    tradingOrderPackage: TradingOrderPackage;

    constructor(
        private tradingOrderPackageService: TradingOrderPackageService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.tradingOrderPackageService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'tradingOrderPackageListModification',
                content: 'Deleted an tradingOrderPackage'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-trading-order-package-delete-popup',
    template: ''
})
export class TradingOrderPackageDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tradingOrderPackagePopupService: TradingOrderPackagePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.tradingOrderPackagePopupService
                .open(TradingOrderPackageDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
