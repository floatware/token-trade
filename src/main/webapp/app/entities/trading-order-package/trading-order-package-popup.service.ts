import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TradingOrderPackage } from './trading-order-package.model';
import { TradingOrderPackageService } from './trading-order-package.service';

@Injectable()
export class TradingOrderPackagePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private tradingOrderPackageService: TradingOrderPackageService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.tradingOrderPackageService.find(id).subscribe((tradingOrderPackage) => {
                    this.ngbModalRef = this.tradingOrderPackageModalRef(component, tradingOrderPackage);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.tradingOrderPackageModalRef(component, new TradingOrderPackage());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    tradingOrderPackageModalRef(component: Component, tradingOrderPackage: TradingOrderPackage): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.tradingOrderPackage = tradingOrderPackage;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
