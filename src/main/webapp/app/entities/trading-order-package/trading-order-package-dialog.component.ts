import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TradingOrderPackage } from './trading-order-package.model';
import { TradingOrderPackagePopupService } from './trading-order-package-popup.service';
import { TradingOrderPackageService } from './trading-order-package.service';
import { TradingPlan, TradingPlanService } from '../trading-plan';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-trading-order-package-dialog',
    templateUrl: './trading-order-package-dialog.component.html'
})
export class TradingOrderPackageDialogComponent implements OnInit {

    tradingOrderPackage: TradingOrderPackage;
    isSaving: boolean;

    tradingplans: TradingPlan[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private tradingOrderPackageService: TradingOrderPackageService,
        private tradingPlanService: TradingPlanService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.tradingPlanService.query()
            .subscribe((res: ResponseWrapper) => { this.tradingplans = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.tradingOrderPackage.id !== undefined) {
            this.subscribeToSaveResponse(
                this.tradingOrderPackageService.update(this.tradingOrderPackage));
        } else {
            this.subscribeToSaveResponse(
                this.tradingOrderPackageService.create(this.tradingOrderPackage));
        }
    }

    private subscribeToSaveResponse(result: Observable<TradingOrderPackage>) {
        result.subscribe((res: TradingOrderPackage) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: TradingOrderPackage) {
        this.eventManager.broadcast({ name: 'tradingOrderPackageListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackTradingPlanById(index: number, item: TradingPlan) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-trading-order-package-popup',
    template: ''
})
export class TradingOrderPackagePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tradingOrderPackagePopupService: TradingOrderPackagePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.tradingOrderPackagePopupService
                    .open(TradingOrderPackageDialogComponent as Component, params['id']);
            } else {
                this.tradingOrderPackagePopupService
                    .open(TradingOrderPackageDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
