import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TradingOrderPackageComponent } from './trading-order-package.component';
import { TradingOrderPackageDetailComponent } from './trading-order-package-detail.component';
import { TradingOrderPackagePopupComponent } from './trading-order-package-dialog.component';
import { TradingOrderPackageDeletePopupComponent } from './trading-order-package-delete-dialog.component';

export const tradingOrderPackageRoute: Routes = [
    {
        path: 'trading-order-package',
        component: TradingOrderPackageComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingOrderPackage.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'trading-order-package/:id',
        component: TradingOrderPackageDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingOrderPackage.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const tradingOrderPackagePopupRoute: Routes = [
    {
        path: 'trading-order-package-new',
        component: TradingOrderPackagePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingOrderPackage.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'trading-order-package/:id/edit',
        component: TradingOrderPackagePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingOrderPackage.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'trading-order-package/:id/delete',
        component: TradingOrderPackageDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingOrderPackage.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
