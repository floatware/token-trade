import { BaseEntity } from './../../shared';

export class Asset implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public shortName?: string,
        public implementation?: string,
        public chartRepresentation?: string,
        public market?: BaseEntity,
        public marketCurrency?: BaseEntity,
        public baseCurrency?: BaseEntity,
    ) {
    }
}
