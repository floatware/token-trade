import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AssetComponent } from './asset.component';
import { AssetDetailComponent } from './asset-detail.component';
import { AssetPopupComponent } from './asset-dialog.component';
import { AssetDeletePopupComponent } from './asset-delete-dialog.component';

@Injectable()
export class AssetResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const assetRoute: Routes = [
    {
        path: 'asset',
        component: AssetComponent,
        resolve: {
            'pagingParams': AssetResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.asset.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'asset/:id',
        component: AssetDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.asset.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const assetPopupRoute: Routes = [
    {
        path: 'asset-new',
        component: AssetPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.asset.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'asset/:id/edit',
        component: AssetPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.asset.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'asset/:id/delete',
        component: AssetDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.asset.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
