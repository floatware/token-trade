import { BaseEntity } from './../../shared';

export class Market implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public shortName?: string,
        public implementation?: string,
        public chartRepresentation?: string,
        public note?: BaseEntity,
        public tenant?: BaseEntity,
    ) {
    }
}
