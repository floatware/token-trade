import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Market } from './market.model';
import { MarketPopupService } from './market-popup.service';
import { MarketService } from './market.service';
import { Note, NoteService } from '../note';
import { Tenant, TenantService } from '../tenant';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-market-dialog',
    templateUrl: './market-dialog.component.html'
})
export class MarketDialogComponent implements OnInit {

    market: Market;
    isSaving: boolean;

    notes: Note[];

    tenants: Tenant[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private marketService: MarketService,
        private noteService: NoteService,
        private tenantService: TenantService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.noteService
            .query({filter: 'market-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.market.note || !this.market.note.id) {
                    this.notes = res.json;
                } else {
                    this.noteService
                        .find(this.market.note.id)
                        .subscribe((subRes: Note) => {
                            this.notes = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.tenantService.query()
            .subscribe((res: ResponseWrapper) => { this.tenants = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.market.id !== undefined) {
            this.subscribeToSaveResponse(
                this.marketService.update(this.market));
        } else {
            this.subscribeToSaveResponse(
                this.marketService.create(this.market));
        }
    }

    private subscribeToSaveResponse(result: Observable<Market>) {
        result.subscribe((res: Market) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Market) {
        this.eventManager.broadcast({ name: 'marketListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackNoteById(index: number, item: Note) {
        return item.id;
    }

    trackTenantById(index: number, item: Tenant) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-market-popup',
    template: ''
})
export class MarketPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private marketPopupService: MarketPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.marketPopupService
                    .open(MarketDialogComponent as Component, params['id']);
            } else {
                this.marketPopupService
                    .open(MarketDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
