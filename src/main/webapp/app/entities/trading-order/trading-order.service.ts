import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { TradingOrder } from './trading-order.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class TradingOrderService {

    private resourceUrl = SERVER_API_URL + 'api/trading-orders';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/trading-orders';

    constructor(private http: Http) { }

    create(tradingOrder: TradingOrder): Observable<TradingOrder> {
        const copy = this.convert(tradingOrder);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(tradingOrder: TradingOrder): Observable<TradingOrder> {
        const copy = this.convert(tradingOrder);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<TradingOrder> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to TradingOrder.
     */
    private convertItemFromServer(json: any): TradingOrder {
        const entity: TradingOrder = Object.assign(new TradingOrder(), json);
        return entity;
    }

    /**
     * Convert a TradingOrder to a JSON which can be sent to the server.
     */
    private convert(tradingOrder: TradingOrder): TradingOrder {
        const copy: TradingOrder = Object.assign({}, tradingOrder);
        return copy;
    }
}
