import { BaseEntity } from './../../shared';

export const enum TradingOrderStatus {
    'PENDING_NEW',
    'NEW',
    'PARTIALLY_FILLED',
    'FILLED',
    'PENDING_CANCEL',
    'CANCELED',
    'PENDING_REPLACE',
    'REPLACED',
    'STOPPED',
    'REJECTED',
    'EXPIRED'
}

export class TradingOrder implements BaseEntity {
    constructor(
        public id?: number,
        public uuid?: string,
        public status?: TradingOrderStatus,
        public originalAmount?: number,
        public averagePrice?: number,
        public cumulativeAmount?: number,
        public marketAccount?: BaseEntity,
        public tradingOrderType?: BaseEntity,
        public tradingOrderPackage?: BaseEntity,
        public asset?: BaseEntity,
    ) {
    }
}
