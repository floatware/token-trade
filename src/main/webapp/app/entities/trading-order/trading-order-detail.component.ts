import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TradingOrder } from './trading-order.model';
import { TradingOrderService } from './trading-order.service';

@Component({
    selector: 'jhi-trading-order-detail',
    templateUrl: './trading-order-detail.component.html'
})
export class TradingOrderDetailComponent implements OnInit, OnDestroy {

    tradingOrder: TradingOrder;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private tradingOrderService: TradingOrderService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTradingOrders();
    }

    load(id) {
        this.tradingOrderService.find(id).subscribe((tradingOrder) => {
            this.tradingOrder = tradingOrder;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTradingOrders() {
        this.eventSubscriber = this.eventManager.subscribe(
            'tradingOrderListModification',
            (response) => this.load(this.tradingOrder.id)
        );
    }
}
