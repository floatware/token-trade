import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TradingOrderComponent } from './trading-order.component';
import { TradingOrderDetailComponent } from './trading-order-detail.component';
import { TradingOrderPopupComponent } from './trading-order-dialog.component';
import { TradingOrderDeletePopupComponent } from './trading-order-delete-dialog.component';

export const tradingOrderRoute: Routes = [
    {
        path: 'trading-order',
        component: TradingOrderComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'trading-order/:id',
        component: TradingOrderDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const tradingOrderPopupRoute: Routes = [
    {
        path: 'trading-order-new',
        component: TradingOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'trading-order/:id/edit',
        component: TradingOrderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'trading-order/:id/delete',
        component: TradingOrderDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.tradingOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
