import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TokentradeSharedModule } from '../../shared';
import {
    TradingOrderService,
    TradingOrderPopupService,
    TradingOrderComponent,
    TradingOrderDetailComponent,
    TradingOrderDialogComponent,
    TradingOrderPopupComponent,
    TradingOrderDeletePopupComponent,
    TradingOrderDeleteDialogComponent,
    tradingOrderRoute,
    tradingOrderPopupRoute,
} from './';

const ENTITY_STATES = [
    ...tradingOrderRoute,
    ...tradingOrderPopupRoute,
];

@NgModule({
    imports: [
        TokentradeSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TradingOrderComponent,
        TradingOrderDetailComponent,
        TradingOrderDialogComponent,
        TradingOrderDeleteDialogComponent,
        TradingOrderPopupComponent,
        TradingOrderDeletePopupComponent,
    ],
    entryComponents: [
        TradingOrderComponent,
        TradingOrderDialogComponent,
        TradingOrderPopupComponent,
        TradingOrderDeleteDialogComponent,
        TradingOrderDeletePopupComponent,
    ],
    providers: [
        TradingOrderService,
        TradingOrderPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TokentradeTradingOrderModule {}
