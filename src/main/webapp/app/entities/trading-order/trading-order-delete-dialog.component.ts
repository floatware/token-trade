import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TradingOrder } from './trading-order.model';
import { TradingOrderPopupService } from './trading-order-popup.service';
import { TradingOrderService } from './trading-order.service';

@Component({
    selector: 'jhi-trading-order-delete-dialog',
    templateUrl: './trading-order-delete-dialog.component.html'
})
export class TradingOrderDeleteDialogComponent {

    tradingOrder: TradingOrder;

    constructor(
        private tradingOrderService: TradingOrderService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.tradingOrderService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'tradingOrderListModification',
                content: 'Deleted an tradingOrder'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-trading-order-delete-popup',
    template: ''
})
export class TradingOrderDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tradingOrderPopupService: TradingOrderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.tradingOrderPopupService
                .open(TradingOrderDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
