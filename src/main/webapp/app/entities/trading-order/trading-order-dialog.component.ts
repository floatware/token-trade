import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TradingOrder } from './trading-order.model';
import { TradingOrderPopupService } from './trading-order-popup.service';
import { TradingOrderService } from './trading-order.service';
import { MarketAccount, MarketAccountService } from '../market-account';
import { TradingOrderType, TradingOrderTypeService } from '../trading-order-type';
import { TradingOrderPackage, TradingOrderPackageService } from '../trading-order-package';
import { Asset, AssetService } from '../asset';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-trading-order-dialog',
    templateUrl: './trading-order-dialog.component.html'
})
export class TradingOrderDialogComponent implements OnInit {

    tradingOrder: TradingOrder;
    isSaving: boolean;

    marketaccounts: MarketAccount[];

    tradingordertypes: TradingOrderType[];

    tradingorderpackages: TradingOrderPackage[];

    assets: Asset[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private tradingOrderService: TradingOrderService,
        private marketAccountService: MarketAccountService,
        private tradingOrderTypeService: TradingOrderTypeService,
        private tradingOrderPackageService: TradingOrderPackageService,
        private assetService: AssetService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.marketAccountService.query()
            .subscribe((res: ResponseWrapper) => { this.marketaccounts = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.tradingOrderTypeService.query()
            .subscribe((res: ResponseWrapper) => { this.tradingordertypes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.tradingOrderPackageService.query()
            .subscribe((res: ResponseWrapper) => { this.tradingorderpackages = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.assetService.query()
            .subscribe((res: ResponseWrapper) => { this.assets = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.tradingOrder.id !== undefined) {
            this.subscribeToSaveResponse(
                this.tradingOrderService.update(this.tradingOrder));
        } else {
            this.subscribeToSaveResponse(
                this.tradingOrderService.create(this.tradingOrder));
        }
    }

    private subscribeToSaveResponse(result: Observable<TradingOrder>) {
        result.subscribe((res: TradingOrder) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: TradingOrder) {
        this.eventManager.broadcast({ name: 'tradingOrderListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackMarketAccountById(index: number, item: MarketAccount) {
        return item.id;
    }

    trackTradingOrderTypeById(index: number, item: TradingOrderType) {
        return item.id;
    }

    trackTradingOrderPackageById(index: number, item: TradingOrderPackage) {
        return item.id;
    }

    trackAssetById(index: number, item: Asset) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-trading-order-popup',
    template: ''
})
export class TradingOrderPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tradingOrderPopupService: TradingOrderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.tradingOrderPopupService
                    .open(TradingOrderDialogComponent as Component, params['id']);
            } else {
                this.tradingOrderPopupService
                    .open(TradingOrderDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
