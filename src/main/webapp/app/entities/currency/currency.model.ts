import { BaseEntity } from './../../shared';

export class Currency implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public shortName?: string,
        public implementation?: string,
        public chartRepresentation?: string,
        public description?: string,
    ) {
    }
}
