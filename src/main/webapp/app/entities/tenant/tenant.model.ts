import { BaseEntity, User } from './../../shared';

export class Tenant implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public shortName?: string,
        public resource?: string,
        public admin?: User,
    ) {
    }
}
