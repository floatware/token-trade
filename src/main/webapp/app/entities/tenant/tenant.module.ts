import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TokentradeSharedModule } from '../../shared';
import { TokentradeAdminModule } from '../../admin/admin.module';
import {
    TenantService,
    TenantPopupService,
    TenantComponent,
    TenantDetailComponent,
    TenantDialogComponent,
    TenantPopupComponent,
    TenantDeletePopupComponent,
    TenantDeleteDialogComponent,
    tenantRoute,
    tenantPopupRoute,
} from './';

const ENTITY_STATES = [
    ...tenantRoute,
    ...tenantPopupRoute,
];

@NgModule({
    imports: [
        TokentradeSharedModule,
        TokentradeAdminModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TenantComponent,
        TenantDetailComponent,
        TenantDialogComponent,
        TenantDeleteDialogComponent,
        TenantPopupComponent,
        TenantDeletePopupComponent,
    ],
    entryComponents: [
        TenantComponent,
        TenantDialogComponent,
        TenantPopupComponent,
        TenantDeleteDialogComponent,
        TenantDeletePopupComponent,
    ],
    providers: [
        TenantService,
        TenantPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TokentradeTenantModule {}
