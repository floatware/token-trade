import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { ClientGroup } from './client-group.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ClientGroupService {

    private resourceUrl = SERVER_API_URL + 'api/client-groups';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/client-groups';

    constructor(private http: Http) { }

    create(clientGroup: ClientGroup): Observable<ClientGroup> {
        const copy = this.convert(clientGroup);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(clientGroup: ClientGroup): Observable<ClientGroup> {
        const copy = this.convert(clientGroup);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<ClientGroup> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to ClientGroup.
     */
    private convertItemFromServer(json: any): ClientGroup {
        const entity: ClientGroup = Object.assign(new ClientGroup(), json);
        return entity;
    }

    /**
     * Convert a ClientGroup to a JSON which can be sent to the server.
     */
    private convert(clientGroup: ClientGroup): ClientGroup {
        const copy: ClientGroup = Object.assign({}, clientGroup);
        return copy;
    }
}
