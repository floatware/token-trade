import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { ClientGroup } from './client-group.model';
import { ClientGroupService } from './client-group.service';

@Component({
    selector: 'jhi-client-group-detail',
    templateUrl: './client-group-detail.component.html'
})
export class ClientGroupDetailComponent implements OnInit, OnDestroy {

    clientGroup: ClientGroup;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private clientGroupService: ClientGroupService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClientGroups();
    }

    load(id) {
        this.clientGroupService.find(id).subscribe((clientGroup) => {
            this.clientGroup = clientGroup;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClientGroups() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clientGroupListModification',
            (response) => this.load(this.clientGroup.id)
        );
    }
}
