import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ClientGroupComponent } from './client-group.component';
import { ClientGroupDetailComponent } from './client-group-detail.component';
import { ClientGroupPopupComponent } from './client-group-dialog.component';
import { ClientGroupDeletePopupComponent } from './client-group-delete-dialog.component';

export const clientGroupRoute: Routes = [
    {
        path: 'client-group',
        component: ClientGroupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.clientGroup.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'client-group/:id',
        component: ClientGroupDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.clientGroup.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const clientGroupPopupRoute: Routes = [
    {
        path: 'client-group-new',
        component: ClientGroupPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.clientGroup.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-group/:id/edit',
        component: ClientGroupPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.clientGroup.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'client-group/:id/delete',
        component: ClientGroupDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.clientGroup.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
