export * from './client-group.model';
export * from './client-group-popup.service';
export * from './client-group.service';
export * from './client-group-dialog.component';
export * from './client-group-delete-dialog.component';
export * from './client-group-detail.component';
export * from './client-group.component';
export * from './client-group.route';
