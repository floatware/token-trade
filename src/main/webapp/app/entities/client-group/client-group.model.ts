import { BaseEntity } from './../../shared';

export class ClientGroup implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public clients?: BaseEntity[],
    ) {
    }
}
