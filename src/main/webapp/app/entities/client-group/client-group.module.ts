import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TokentradeSharedModule } from '../../shared';
import {
    ClientGroupService,
    ClientGroupPopupService,
    ClientGroupComponent,
    ClientGroupDetailComponent,
    ClientGroupDialogComponent,
    ClientGroupPopupComponent,
    ClientGroupDeletePopupComponent,
    ClientGroupDeleteDialogComponent,
    clientGroupRoute,
    clientGroupPopupRoute,
} from './';

const ENTITY_STATES = [
    ...clientGroupRoute,
    ...clientGroupPopupRoute,
];

@NgModule({
    imports: [
        TokentradeSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ClientGroupComponent,
        ClientGroupDetailComponent,
        ClientGroupDialogComponent,
        ClientGroupDeleteDialogComponent,
        ClientGroupPopupComponent,
        ClientGroupDeletePopupComponent,
    ],
    entryComponents: [
        ClientGroupComponent,
        ClientGroupDialogComponent,
        ClientGroupPopupComponent,
        ClientGroupDeleteDialogComponent,
        ClientGroupDeletePopupComponent,
    ],
    providers: [
        ClientGroupService,
        ClientGroupPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TokentradeClientGroupModule {}
