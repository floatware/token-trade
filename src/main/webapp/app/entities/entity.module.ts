import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { TokentradeTenantModule } from './tenant/tenant.module';
import { TokentradeMarketModule } from './market/market.module';
import { TokentradeNoteModule } from './note/note.module';
import { TokentradeAssetModule } from './asset/asset.module';
import { TokentradeCurrencyModule } from './currency/currency.module';
import { TokentradeAlertEventModule } from './alert-event/alert-event.module';
import { TokentradeTraderModule } from './trader/trader.module';
import { TokentradeTradingPlanModule } from './trading-plan/trading-plan.module';
import { TokentradeTradingOrderPackageModule } from './trading-order-package/trading-order-package.module';
import { TokentradeTradingOrderModule } from './trading-order/trading-order.module';
import { TokentradeTradingOrderTypeModule } from './trading-order-type/trading-order-type.module';
import { TokentradeClientModule } from './client/client.module';
import { TokentradeClientGroupModule } from './client-group/client-group.module';
import { TokentradeMarketAccountModule } from './market-account/market-account.module';
import { TokentradeWalletModule } from './wallet/wallet.module';
import { TokentradeBalanceModule } from './balance/balance.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        TokentradeTenantModule,
        TokentradeMarketModule,
        TokentradeNoteModule,
        TokentradeAssetModule,
        TokentradeCurrencyModule,
        TokentradeAlertEventModule,
        TokentradeTraderModule,
        TokentradeTradingPlanModule,
        TokentradeTradingOrderPackageModule,
        TokentradeTradingOrderModule,
        TokentradeTradingOrderTypeModule,
        TokentradeClientModule,
        TokentradeClientGroupModule,
        TokentradeMarketAccountModule,
        TokentradeWalletModule,
        TokentradeBalanceModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TokentradeEntityModule {}
