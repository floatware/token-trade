import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TraderComponent } from './trader.component';
import { TraderDetailComponent } from './trader-detail.component';
import { TraderPopupComponent } from './trader-dialog.component';
import { TraderDeletePopupComponent } from './trader-delete-dialog.component';

@Injectable()
export class TraderResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const traderRoute: Routes = [
    {
        path: 'trader',
        component: TraderComponent,
        resolve: {
            'pagingParams': TraderResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.trader.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'trader/:id',
        component: TraderDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.trader.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const traderPopupRoute: Routes = [
    {
        path: 'trader-new',
        component: TraderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.trader.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'trader/:id/edit',
        component: TraderPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.trader.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'trader/:id/delete',
        component: TraderDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.trader.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
