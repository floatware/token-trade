import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Trader } from './trader.model';
import { TraderPopupService } from './trader-popup.service';
import { TraderService } from './trader.service';

@Component({
    selector: 'jhi-trader-delete-dialog',
    templateUrl: './trader-delete-dialog.component.html'
})
export class TraderDeleteDialogComponent {

    trader: Trader;

    constructor(
        private traderService: TraderService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.traderService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'traderListModification',
                content: 'Deleted an trader'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-trader-delete-popup',
    template: ''
})
export class TraderDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private traderPopupService: TraderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.traderPopupService
                .open(TraderDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
