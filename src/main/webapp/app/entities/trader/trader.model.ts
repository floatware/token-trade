import { BaseEntity, User } from './../../shared';

export class Trader implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public firstName?: string,
        public lastName?: string,
        public tenant?: BaseEntity,
        public user?: User,
    ) {
    }
}
