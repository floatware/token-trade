import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TokentradeSharedModule } from '../../shared';
import { TokentradeAdminModule } from '../../admin/admin.module';
import {
    TraderService,
    TraderPopupService,
    TraderComponent,
    TraderDetailComponent,
    TraderDialogComponent,
    TraderPopupComponent,
    TraderDeletePopupComponent,
    TraderDeleteDialogComponent,
    traderRoute,
    traderPopupRoute,
    TraderResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...traderRoute,
    ...traderPopupRoute,
];

@NgModule({
    imports: [
        TokentradeSharedModule,
        TokentradeAdminModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TraderComponent,
        TraderDetailComponent,
        TraderDialogComponent,
        TraderDeleteDialogComponent,
        TraderPopupComponent,
        TraderDeletePopupComponent,
    ],
    entryComponents: [
        TraderComponent,
        TraderDialogComponent,
        TraderPopupComponent,
        TraderDeleteDialogComponent,
        TraderDeletePopupComponent,
    ],
    providers: [
        TraderService,
        TraderPopupService,
        TraderResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TokentradeTraderModule {}
