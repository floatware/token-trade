import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Trader } from './trader.model';
import { TraderPopupService } from './trader-popup.service';
import { TraderService } from './trader.service';
import { Tenant, TenantService } from '../tenant';
import { User, UserService } from '../../shared';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-trader-dialog',
    templateUrl: './trader-dialog.component.html'
})
export class TraderDialogComponent implements OnInit {

    trader: Trader;
    isSaving: boolean;

    tenants: Tenant[];

    users: User[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private traderService: TraderService,
        private tenantService: TenantService,
        private userService: UserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.tenantService.query()
            .subscribe((res: ResponseWrapper) => { this.tenants = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.userService.query()
            .subscribe((res: ResponseWrapper) => { this.users = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.trader.id !== undefined) {
            this.subscribeToSaveResponse(
                this.traderService.update(this.trader));
        } else {
            this.subscribeToSaveResponse(
                this.traderService.create(this.trader));
        }
    }

    private subscribeToSaveResponse(result: Observable<Trader>) {
        result.subscribe((res: Trader) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Trader) {
        this.eventManager.broadcast({ name: 'traderListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackTenantById(index: number, item: Tenant) {
        return item.id;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-trader-popup',
    template: ''
})
export class TraderPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private traderPopupService: TraderPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.traderPopupService
                    .open(TraderDialogComponent as Component, params['id']);
            } else {
                this.traderPopupService
                    .open(TraderDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
