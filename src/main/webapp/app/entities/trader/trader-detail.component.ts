import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Trader } from './trader.model';
import { TraderService } from './trader.service';

@Component({
    selector: 'jhi-trader-detail',
    templateUrl: './trader-detail.component.html'
})
export class TraderDetailComponent implements OnInit, OnDestroy {

    trader: Trader;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private traderService: TraderService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTraders();
    }

    load(id) {
        this.traderService.find(id).subscribe((trader) => {
            this.trader = trader;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTraders() {
        this.eventSubscriber = this.eventManager.subscribe(
            'traderListModification',
            (response) => this.load(this.trader.id)
        );
    }
}
