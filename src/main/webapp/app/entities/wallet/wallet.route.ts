import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { WalletComponent } from './wallet.component';
import { WalletDetailComponent } from './wallet-detail.component';
import { WalletPopupComponent } from './wallet-dialog.component';
import { WalletDeletePopupComponent } from './wallet-delete-dialog.component';

@Injectable()
export class WalletResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const walletRoute: Routes = [
    {
        path: 'wallet',
        component: WalletComponent,
        resolve: {
            'pagingParams': WalletResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.wallet.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'wallet/:id',
        component: WalletDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.wallet.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const walletPopupRoute: Routes = [
    {
        path: 'wallet-new',
        component: WalletPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.wallet.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'wallet/:id/edit',
        component: WalletPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.wallet.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'wallet/:id/delete',
        component: WalletDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'tokentradeApp.wallet.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
