import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TokentradeSharedModule } from '../../shared';
import {
    WalletService,
    WalletPopupService,
    WalletComponent,
    WalletDetailComponent,
    WalletDialogComponent,
    WalletPopupComponent,
    WalletDeletePopupComponent,
    WalletDeleteDialogComponent,
    walletRoute,
    walletPopupRoute,
    WalletResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...walletRoute,
    ...walletPopupRoute,
];

@NgModule({
    imports: [
        TokentradeSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        WalletComponent,
        WalletDetailComponent,
        WalletDialogComponent,
        WalletDeleteDialogComponent,
        WalletPopupComponent,
        WalletDeletePopupComponent,
    ],
    entryComponents: [
        WalletComponent,
        WalletDialogComponent,
        WalletPopupComponent,
        WalletDeleteDialogComponent,
        WalletDeletePopupComponent,
    ],
    providers: [
        WalletService,
        WalletPopupService,
        WalletResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TokentradeWalletModule {}
