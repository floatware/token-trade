import { BaseEntity } from './../../shared';

export class Wallet implements BaseEntity {
    constructor(
        public id?: number,
        public description?: string,
        public publicKey?: string,
        public currency?: BaseEntity,
        public client?: BaseEntity,
    ) {
    }
}
