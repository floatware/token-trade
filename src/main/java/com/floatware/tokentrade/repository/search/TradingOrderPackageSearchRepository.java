package com.floatware.tokentrade.repository.search;

import com.floatware.tokentrade.domain.TradingOrderPackage;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the TradingOrderPackage entity.
 */
public interface TradingOrderPackageSearchRepository extends ElasticsearchRepository<TradingOrderPackage, Long> {
}
