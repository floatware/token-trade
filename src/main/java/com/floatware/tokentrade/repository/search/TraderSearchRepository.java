package com.floatware.tokentrade.repository.search;

import com.floatware.tokentrade.domain.Trader;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Trader entity.
 */
public interface TraderSearchRepository extends ElasticsearchRepository<Trader, Long> {
}
