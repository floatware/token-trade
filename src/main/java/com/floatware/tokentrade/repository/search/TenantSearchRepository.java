package com.floatware.tokentrade.repository.search;

import com.floatware.tokentrade.domain.Tenant;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Tenant entity.
 */
public interface TenantSearchRepository extends ElasticsearchRepository<Tenant, Long> {
}
