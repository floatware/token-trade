package com.floatware.tokentrade.repository.search;

import com.floatware.tokentrade.domain.Market;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Market entity.
 */
public interface MarketSearchRepository extends ElasticsearchRepository<Market, Long> {
}
