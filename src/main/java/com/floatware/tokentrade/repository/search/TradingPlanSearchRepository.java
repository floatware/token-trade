package com.floatware.tokentrade.repository.search;

import com.floatware.tokentrade.domain.TradingPlan;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the TradingPlan entity.
 */
public interface TradingPlanSearchRepository extends ElasticsearchRepository<TradingPlan, Long> {
}
