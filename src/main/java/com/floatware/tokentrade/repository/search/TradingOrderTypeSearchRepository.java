package com.floatware.tokentrade.repository.search;

import com.floatware.tokentrade.domain.TradingOrderType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the TradingOrderType entity.
 */
public interface TradingOrderTypeSearchRepository extends ElasticsearchRepository<TradingOrderType, Long> {
}
