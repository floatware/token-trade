package com.floatware.tokentrade.repository.search;

import com.floatware.tokentrade.domain.ClientGroup;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ClientGroup entity.
 */
public interface ClientGroupSearchRepository extends ElasticsearchRepository<ClientGroup, Long> {
}
