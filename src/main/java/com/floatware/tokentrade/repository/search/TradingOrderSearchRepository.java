package com.floatware.tokentrade.repository.search;

import com.floatware.tokentrade.domain.TradingOrder;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the TradingOrder entity.
 */
public interface TradingOrderSearchRepository extends ElasticsearchRepository<TradingOrder, Long> {
}
