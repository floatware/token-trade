package com.floatware.tokentrade.repository.search;

import com.floatware.tokentrade.domain.MarketAccount;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the MarketAccount entity.
 */
public interface MarketAccountSearchRepository extends ElasticsearchRepository<MarketAccount, Long> {
}
