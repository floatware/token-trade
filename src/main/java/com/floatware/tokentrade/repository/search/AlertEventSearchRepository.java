package com.floatware.tokentrade.repository.search;

import com.floatware.tokentrade.domain.AlertEvent;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AlertEvent entity.
 */
public interface AlertEventSearchRepository extends ElasticsearchRepository<AlertEvent, Long> {
}
