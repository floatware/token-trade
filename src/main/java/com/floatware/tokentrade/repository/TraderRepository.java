package com.floatware.tokentrade.repository;

import com.floatware.tokentrade.domain.Trader;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the Trader entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TraderRepository extends JpaRepository<Trader, Long> {

    @Query("select trader from Trader trader where trader.user.login = ?#{principal.username}")
    List<Trader> findByUserIsCurrentUser();

}
