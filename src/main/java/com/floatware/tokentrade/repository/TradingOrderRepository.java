package com.floatware.tokentrade.repository;

import com.floatware.tokentrade.domain.TradingOrder;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TradingOrder entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TradingOrderRepository extends JpaRepository<TradingOrder, Long> {

}
