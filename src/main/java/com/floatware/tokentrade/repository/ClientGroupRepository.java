package com.floatware.tokentrade.repository;

import com.floatware.tokentrade.domain.ClientGroup;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the ClientGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientGroupRepository extends JpaRepository<ClientGroup, Long> {
    @Query("select distinct client_group from ClientGroup client_group left join fetch client_group.clients")
    List<ClientGroup> findAllWithEagerRelationships();

    @Query("select client_group from ClientGroup client_group left join fetch client_group.clients where client_group.id =:id")
    ClientGroup findOneWithEagerRelationships(@Param("id") Long id);

}
