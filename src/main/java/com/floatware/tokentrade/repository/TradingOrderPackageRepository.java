package com.floatware.tokentrade.repository;

import com.floatware.tokentrade.domain.TradingOrderPackage;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TradingOrderPackage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TradingOrderPackageRepository extends JpaRepository<TradingOrderPackage, Long> {

}
