package com.floatware.tokentrade.repository;

import com.floatware.tokentrade.domain.AlertEvent;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AlertEvent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AlertEventRepository extends JpaRepository<AlertEvent, Long> {

}
