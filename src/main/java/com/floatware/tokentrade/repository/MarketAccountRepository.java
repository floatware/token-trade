package com.floatware.tokentrade.repository;

import com.floatware.tokentrade.domain.MarketAccount;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the MarketAccount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MarketAccountRepository extends JpaRepository<MarketAccount, Long> {

}
