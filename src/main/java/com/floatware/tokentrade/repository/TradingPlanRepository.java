package com.floatware.tokentrade.repository;

import com.floatware.tokentrade.domain.TradingPlan;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TradingPlan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TradingPlanRepository extends JpaRepository<TradingPlan, Long> {

}
