package com.floatware.tokentrade.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A TradingOrderType.
 */
@Entity
@Table(name = "tt_trading_order_type")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "tradingordertype")
public class TradingOrderType extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "trading_ordertype", nullable = false)
    private String tradingOrdertype;

    @ManyToOne
    private Market supportedMarket;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTradingOrdertype() {
        return tradingOrdertype;
    }

    public TradingOrderType tradingOrdertype(String tradingOrdertype) {
        this.tradingOrdertype = tradingOrdertype;
        return this;
    }

    public void setTradingOrdertype(String tradingOrdertype) {
        this.tradingOrdertype = tradingOrdertype;
    }

    public Market getSupportedMarket() {
        return supportedMarket;
    }

    public TradingOrderType supportedMarket(Market market) {
        this.supportedMarket = market;
        return this;
    }

    public void setSupportedMarket(Market market) {
        this.supportedMarket = market;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TradingOrderType tradingOrderType = (TradingOrderType) o;
        if (tradingOrderType.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tradingOrderType.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TradingOrderType{" +
            "id=" + getId() +
            ", tradingOrdertype='" + getTradingOrdertype() + "'" +
            "}";
    }
}
