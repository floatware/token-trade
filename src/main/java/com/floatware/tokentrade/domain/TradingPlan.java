package com.floatware.tokentrade.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import com.floatware.tokentrade.domain.enumeration.TradingType;

/**
 * A TradingPlan.
 */
@Entity
@Table(name = "tt_trading_plan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "tradingplan")
public class TradingPlan extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "uuid", nullable = false)
    private String uuid;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "trading_type", nullable = false)
    private TradingType tradingType;

    @OneToOne
    @JoinColumn(unique = true)
    private Note note;

    @ManyToOne
    private Trader trader;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public TradingPlan uuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public TradingType getTradingType() {
        return tradingType;
    }

    public TradingPlan tradingType(TradingType tradingType) {
        this.tradingType = tradingType;
        return this;
    }

    public void setTradingType(TradingType tradingType) {
        this.tradingType = tradingType;
    }

    public Note getNote() {
        return note;
    }

    public TradingPlan note(Note note) {
        this.note = note;
        return this;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public Trader getTrader() {
        return trader;
    }

    public TradingPlan trader(Trader trader) {
        this.trader = trader;
        return this;
    }

    public void setTrader(Trader trader) {
        this.trader = trader;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TradingPlan tradingPlan = (TradingPlan) o;
        if (tradingPlan.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tradingPlan.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TradingPlan{" +
            "id=" + getId() +
            ", uuid='" + getUuid() + "'" +
            ", tradingType='" + getTradingType() + "'" +
            "}";
    }
}
