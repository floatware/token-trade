package com.floatware.tokentrade.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import com.floatware.tokentrade.domain.enumeration.TradingOrderStatus;

/**
 * A TradingOrder.
 */
@Entity
@Table(name = "tt_trading_order")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "tradingorder")
public class TradingOrder extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "uuid", nullable = false)
    private String uuid;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private TradingOrderStatus status;

    @NotNull
    @Column(name = "original_amount", precision=10, scale=2, nullable = false)
    private BigDecimal originalAmount;

    @Column(name = "average_price", precision=10, scale=2)
    private BigDecimal averagePrice;

    @Column(name = "cumulative_amount", precision=10, scale=2)
    private BigDecimal cumulativeAmount;

    @ManyToOne
    private MarketAccount marketAccount;

    @ManyToOne
    private TradingOrderType tradingOrderType;

    @ManyToOne
    private TradingOrderPackage tradingOrderPackage;

    @ManyToOne
    private Asset asset;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public TradingOrder uuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public TradingOrderStatus getStatus() {
        return status;
    }

    public TradingOrder status(TradingOrderStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(TradingOrderStatus status) {
        this.status = status;
    }

    public BigDecimal getOriginalAmount() {
        return originalAmount;
    }

    public TradingOrder originalAmount(BigDecimal originalAmount) {
        this.originalAmount = originalAmount;
        return this;
    }

    public void setOriginalAmount(BigDecimal originalAmount) {
        this.originalAmount = originalAmount;
    }

    public BigDecimal getAveragePrice() {
        return averagePrice;
    }

    public TradingOrder averagePrice(BigDecimal averagePrice) {
        this.averagePrice = averagePrice;
        return this;
    }

    public void setAveragePrice(BigDecimal averagePrice) {
        this.averagePrice = averagePrice;
    }

    public BigDecimal getCumulativeAmount() {
        return cumulativeAmount;
    }

    public TradingOrder cumulativeAmount(BigDecimal cumulativeAmount) {
        this.cumulativeAmount = cumulativeAmount;
        return this;
    }

    public void setCumulativeAmount(BigDecimal cumulativeAmount) {
        this.cumulativeAmount = cumulativeAmount;
    }

    public MarketAccount getMarketAccount() {
        return marketAccount;
    }

    public TradingOrder marketAccount(MarketAccount marketAccount) {
        this.marketAccount = marketAccount;
        return this;
    }

    public void setMarketAccount(MarketAccount marketAccount) {
        this.marketAccount = marketAccount;
    }

    public TradingOrderType getTradingOrderType() {
        return tradingOrderType;
    }

    public TradingOrder tradingOrderType(TradingOrderType tradingOrderType) {
        this.tradingOrderType = tradingOrderType;
        return this;
    }

    public void setTradingOrderType(TradingOrderType tradingOrderType) {
        this.tradingOrderType = tradingOrderType;
    }

    public TradingOrderPackage getTradingOrderPackage() {
        return tradingOrderPackage;
    }

    public TradingOrder tradingOrderPackage(TradingOrderPackage tradingOrderPackage) {
        this.tradingOrderPackage = tradingOrderPackage;
        return this;
    }

    public void setTradingOrderPackage(TradingOrderPackage tradingOrderPackage) {
        this.tradingOrderPackage = tradingOrderPackage;
    }

    public Asset getAsset() {
        return asset;
    }

    public TradingOrder asset(Asset asset) {
        this.asset = asset;
        return this;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TradingOrder tradingOrder = (TradingOrder) o;
        if (tradingOrder.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tradingOrder.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TradingOrder{" +
            "id=" + getId() +
            ", uuid='" + getUuid() + "'" +
            ", status='" + getStatus() + "'" +
            ", originalAmount='" + getOriginalAmount() + "'" +
            ", averagePrice='" + getAveragePrice() + "'" +
            ", cumulativeAmount='" + getCumulativeAmount() + "'" +
            "}";
    }
}
