package com.floatware.tokentrade.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import com.floatware.tokentrade.domain.enumeration.MarketAccountType;

/**
 * A MarketAccount.
 */
@Entity
@Table(name = "tt_market_account")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "marketaccount")
public class MarketAccount extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "market_account_type", nullable = false)
    private MarketAccountType marketAccountType;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "jhi_password")
    private String password;

    @NotNull
    @Column(name = "api_key", nullable = false)
    private String apiKey;

    @NotNull
    @Column(name = "secret_key", nullable = false)
    private String secretKey;

    @ManyToOne
    private Client client;

    @ManyToOne
    private Market market;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MarketAccountType getMarketAccountType() {
        return marketAccountType;
    }

    public MarketAccount marketAccountType(MarketAccountType marketAccountType) {
        this.marketAccountType = marketAccountType;
        return this;
    }

    public void setMarketAccountType(MarketAccountType marketAccountType) {
        this.marketAccountType = marketAccountType;
    }

    public String getUserName() {
        return userName;
    }

    public MarketAccount userName(String userName) {
        this.userName = userName;
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public MarketAccount password(String password) {
        this.password = password;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getApiKey() {
        return apiKey;
    }

    public MarketAccount apiKey(String apiKey) {
        this.apiKey = apiKey;
        return this;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public MarketAccount secretKey(String secretKey) {
        this.secretKey = secretKey;
        return this;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public Client getClient() {
        return client;
    }

    public MarketAccount client(Client client) {
        this.client = client;
        return this;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Market getMarket() {
        return market;
    }

    public MarketAccount market(Market market) {
        this.market = market;
        return this;
    }

    public void setMarket(Market market) {
        this.market = market;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MarketAccount marketAccount = (MarketAccount) o;
        if (marketAccount.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), marketAccount.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MarketAccount{" +
            "id=" + getId() +
            ", marketAccountType='" + getMarketAccountType() + "'" +
            ", userName='" + getUserName() + "'" +
            ", password='" + getPassword() + "'" +
            ", apiKey='" + getApiKey() + "'" +
            ", secretKey='" + getSecretKey() + "'" +
            "}";
    }
}
