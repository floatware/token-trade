package com.floatware.tokentrade.domain.enumeration;

/**
 * The ClientStatus enumeration.
 */
public enum ClientStatus {
    ACTIVE, INACTIVE, INTERN
}
