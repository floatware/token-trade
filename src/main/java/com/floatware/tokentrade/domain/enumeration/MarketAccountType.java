package com.floatware.tokentrade.domain.enumeration;

/**
 * The MarketAccountType enumeration.
 */
public enum MarketAccountType {
    PRIMARY, SECONDARY
}
