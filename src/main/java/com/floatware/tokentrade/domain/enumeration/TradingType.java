package com.floatware.tokentrade.domain.enumeration;

/**
 * The TradingType enumeration.
 */
public enum TradingType {
    BID, ASK, BID_LIMIT, ASK_LIMIT
}
