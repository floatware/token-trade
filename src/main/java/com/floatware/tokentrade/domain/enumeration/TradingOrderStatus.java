package com.floatware.tokentrade.domain.enumeration;

/**
 * The TradingOrderStatus enumeration.
 */
public enum TradingOrderStatus {
    PENDING_NEW, NEW, PARTIALLY_FILLED, FILLED, PENDING_CANCEL, CANCELED, PENDING_REPLACE, REPLACED, STOPPED, REJECTED, EXPIRED
}
