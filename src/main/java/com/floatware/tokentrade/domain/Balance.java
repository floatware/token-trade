package com.floatware.tokentrade.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Balance.
 */
@Entity
@Table(name = "tt_balance")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "balance")
public class Balance extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "balance_data")
    private String balanceData;

    @ManyToOne
    private MarketAccount marketAccount;

    @ManyToOne
    private Balance parent;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBalanceData() {
        return balanceData;
    }

    public Balance balanceData(String balanceData) {
        this.balanceData = balanceData;
        return this;
    }

    public void setBalanceData(String balanceData) {
        this.balanceData = balanceData;
    }

    public MarketAccount getMarketAccount() {
        return marketAccount;
    }

    public Balance marketAccount(MarketAccount marketAccount) {
        this.marketAccount = marketAccount;
        return this;
    }

    public void setMarketAccount(MarketAccount marketAccount) {
        this.marketAccount = marketAccount;
    }

    public Balance getParent() {
        return parent;
    }

    public Balance parent(Balance balance) {
        this.parent = balance;
        return this;
    }

    public void setParent(Balance balance) {
        this.parent = balance;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Balance balance = (Balance) o;
        if (balance.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), balance.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Balance{" +
            "id=" + getId() +
            ", balanceData='" + getBalanceData() + "'" +
            "}";
    }
}
