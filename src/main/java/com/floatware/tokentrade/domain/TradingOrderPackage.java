package com.floatware.tokentrade.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A TradingOrderPackage.
 */
@Entity
@Table(name = "tt_trading_order_package")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "tradingorderpackage")
public class TradingOrderPackage extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "uuid", nullable = false)
    private String uuid;

    @ManyToOne
    private TradingPlan tradingPlan;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public TradingOrderPackage uuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public TradingPlan getTradingPlan() {
        return tradingPlan;
    }

    public TradingOrderPackage tradingPlan(TradingPlan tradingPlan) {
        this.tradingPlan = tradingPlan;
        return this;
    }

    public void setTradingPlan(TradingPlan tradingPlan) {
        this.tradingPlan = tradingPlan;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TradingOrderPackage tradingOrderPackage = (TradingOrderPackage) o;
        if (tradingOrderPackage.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tradingOrderPackage.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TradingOrderPackage{" +
            "id=" + getId() +
            ", uuid='" + getUuid() + "'" +
            "}";
    }
}
