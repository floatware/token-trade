package com.floatware.tokentrade.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Asset.
 */
@Entity
@Table(name = "tt_asset")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "asset")
public class Asset extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "short_name", nullable = false)
    private String shortName;

    @NotNull
    @Column(name = "implementation", nullable = false)
    private String implementation;

    @NotNull
    @Column(name = "chart_representation", nullable = false)
    private String chartRepresentation;

    @ManyToOne
    private Market market;

    @ManyToOne
    private Currency marketCurrency;

    @ManyToOne
    private Currency baseCurrency;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Asset name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public Asset shortName(String shortName) {
        this.shortName = shortName;
        return this;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getImplementation() {
        return implementation;
    }

    public Asset implementation(String implementation) {
        this.implementation = implementation;
        return this;
    }

    public void setImplementation(String implementation) {
        this.implementation = implementation;
    }

    public String getChartRepresentation() {
        return chartRepresentation;
    }

    public Asset chartRepresentation(String chartRepresentation) {
        this.chartRepresentation = chartRepresentation;
        return this;
    }

    public void setChartRepresentation(String chartRepresentation) {
        this.chartRepresentation = chartRepresentation;
    }

    public Market getMarket() {
        return market;
    }

    public Asset market(Market market) {
        this.market = market;
        return this;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public Currency getMarketCurrency() {
        return marketCurrency;
    }

    public Asset marketCurrency(Currency currency) {
        this.marketCurrency = currency;
        return this;
    }

    public void setMarketCurrency(Currency currency) {
        this.marketCurrency = currency;
    }

    public Currency getBaseCurrency() {
        return baseCurrency;
    }

    public Asset baseCurrency(Currency currency) {
        this.baseCurrency = currency;
        return this;
    }

    public void setBaseCurrency(Currency currency) {
        this.baseCurrency = currency;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Asset asset = (Asset) o;
        if (asset.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), asset.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Asset{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", shortName='" + getShortName() + "'" +
            ", implementation='" + getImplementation() + "'" +
            ", chartRepresentation='" + getChartRepresentation() + "'" +
            "}";
    }
}
