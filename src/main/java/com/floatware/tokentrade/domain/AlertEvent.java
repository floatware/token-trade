package com.floatware.tokentrade.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A AlertEvent.
 */
@Entity
@Table(name = "tt_alert_event")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "alertevent")
public class AlertEvent extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "current_price", precision=10, scale=2, nullable = false)
    private BigDecimal currentPrice;

    @NotNull
    @Column(name = "alert_price", precision=10, scale=2, nullable = false)
    private BigDecimal alertPrice;

    @Column(name = "fired")
    private Boolean fired;

    @Column(name = "note")
    private String note;

    @ManyToOne
    private Asset asset;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public AlertEvent currentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
        return this;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }

    public BigDecimal getAlertPrice() {
        return alertPrice;
    }

    public AlertEvent alertPrice(BigDecimal alertPrice) {
        this.alertPrice = alertPrice;
        return this;
    }

    public void setAlertPrice(BigDecimal alertPrice) {
        this.alertPrice = alertPrice;
    }

    public Boolean isFired() {
        return fired;
    }

    public AlertEvent fired(Boolean fired) {
        this.fired = fired;
        return this;
    }

    public void setFired(Boolean fired) {
        this.fired = fired;
    }

    public String getNote() {
        return note;
    }

    public AlertEvent note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Asset getAsset() {
        return asset;
    }

    public AlertEvent asset(Asset asset) {
        this.asset = asset;
        return this;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AlertEvent alertEvent = (AlertEvent) o;
        if (alertEvent.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), alertEvent.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AlertEvent{" +
            "id=" + getId() +
            ", currentPrice='" + getCurrentPrice() + "'" +
            ", alertPrice='" + getAlertPrice() + "'" +
            ", fired='" + isFired() + "'" +
            ", note='" + getNote() + "'" +
            "}";
    }
}
