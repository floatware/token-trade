package com.floatware.tokentrade.web.rest.dto;

public class ProfileInfoDTO {

    private String[] activeProfiles;

    private String ribbonEnv;

    public ProfileInfoDTO(String[] activeProfiles, String ribbonEnv) {
        this.activeProfiles = activeProfiles;
        this.ribbonEnv = ribbonEnv;
    }

    public String[] getActiveProfiles() {
        return activeProfiles;
    }

    public String getRibbonEnv() {
        return ribbonEnv;
    }
}
