package com.floatware.tokentrade.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.floatware.tokentrade.domain.AlertEvent;
import com.floatware.tokentrade.service.AlertEventService;
import com.floatware.tokentrade.web.rest.util.HeaderUtil;
import com.floatware.tokentrade.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AlertEvent.
 */
@RestController
@RequestMapping("/api")
public class AlertEventResource {

    private final Logger log = LoggerFactory.getLogger(AlertEventResource.class);

    private static final String ENTITY_NAME = "alertEvent";

    private final AlertEventService alertEventService;

    public AlertEventResource(AlertEventService alertEventService) {
        this.alertEventService = alertEventService;
    }

    /**
     * POST  /alert-events : Create a new alertEvent.
     *
     * @param alertEvent the alertEvent to create
     * @return the ResponseEntity with status 201 (Created) and with body the new alertEvent, or with status 400 (Bad Request) if the alertEvent has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/alert-events")
    @Timed
    public ResponseEntity<AlertEvent> createAlertEvent(@Valid @RequestBody AlertEvent alertEvent) throws URISyntaxException {
        log.debug("REST request to save AlertEvent : {}", alertEvent);
        if (alertEvent.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new alertEvent cannot already have an ID")).body(null);
        }
        AlertEvent result = alertEventService.save(alertEvent);
        return ResponseEntity.created(new URI("/api/alert-events/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /alert-events : Updates an existing alertEvent.
     *
     * @param alertEvent the alertEvent to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated alertEvent,
     * or with status 400 (Bad Request) if the alertEvent is not valid,
     * or with status 500 (Internal Server Error) if the alertEvent couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/alert-events")
    @Timed
    public ResponseEntity<AlertEvent> updateAlertEvent(@Valid @RequestBody AlertEvent alertEvent) throws URISyntaxException {
        log.debug("REST request to update AlertEvent : {}", alertEvent);
        if (alertEvent.getId() == null) {
            return createAlertEvent(alertEvent);
        }
        AlertEvent result = alertEventService.save(alertEvent);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, alertEvent.getId().toString()))
            .body(result);
    }

    /**
     * GET  /alert-events : get all the alertEvents.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of alertEvents in body
     */
    @GetMapping("/alert-events")
    @Timed
    public ResponseEntity<List<AlertEvent>> getAllAlertEvents(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of AlertEvents");
        Page<AlertEvent> page = alertEventService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/alert-events");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /alert-events/:id : get the "id" alertEvent.
     *
     * @param id the id of the alertEvent to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the alertEvent, or with status 404 (Not Found)
     */
    @GetMapping("/alert-events/{id}")
    @Timed
    public ResponseEntity<AlertEvent> getAlertEvent(@PathVariable Long id) {
        log.debug("REST request to get AlertEvent : {}", id);
        AlertEvent alertEvent = alertEventService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(alertEvent));
    }

    /**
     * DELETE  /alert-events/:id : delete the "id" alertEvent.
     *
     * @param id the id of the alertEvent to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/alert-events/{id}")
    @Timed
    public ResponseEntity<Void> deleteAlertEvent(@PathVariable Long id) {
        log.debug("REST request to delete AlertEvent : {}", id);
        alertEventService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/alert-events?query=:query : search for the alertEvent corresponding
     * to the query.
     *
     * @param query the query of the alertEvent search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/alert-events")
    @Timed
    public ResponseEntity<List<AlertEvent>> searchAlertEvents(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of AlertEvents for query {}", query);
        Page<AlertEvent> page = alertEventService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/alert-events");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
