package com.floatware.tokentrade.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.floatware.tokentrade.domain.MarketAccount;
import com.floatware.tokentrade.service.MarketAccountService;
import com.floatware.tokentrade.web.rest.util.HeaderUtil;
import com.floatware.tokentrade.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing MarketAccount.
 */
@RestController
@RequestMapping("/api")
public class MarketAccountResource {

    private final Logger log = LoggerFactory.getLogger(MarketAccountResource.class);

    private static final String ENTITY_NAME = "marketAccount";

    private final MarketAccountService marketAccountService;

    public MarketAccountResource(MarketAccountService marketAccountService) {
        this.marketAccountService = marketAccountService;
    }

    /**
     * POST  /market-accounts : Create a new marketAccount.
     *
     * @param marketAccount the marketAccount to create
     * @return the ResponseEntity with status 201 (Created) and with body the new marketAccount, or with status 400 (Bad Request) if the marketAccount has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/market-accounts")
    @Timed
    public ResponseEntity<MarketAccount> createMarketAccount(@Valid @RequestBody MarketAccount marketAccount) throws URISyntaxException {
        log.debug("REST request to save MarketAccount : {}", marketAccount);
        if (marketAccount.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new marketAccount cannot already have an ID")).body(null);
        }
        MarketAccount result = marketAccountService.save(marketAccount);
        return ResponseEntity.created(new URI("/api/market-accounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /market-accounts : Updates an existing marketAccount.
     *
     * @param marketAccount the marketAccount to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated marketAccount,
     * or with status 400 (Bad Request) if the marketAccount is not valid,
     * or with status 500 (Internal Server Error) if the marketAccount couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/market-accounts")
    @Timed
    public ResponseEntity<MarketAccount> updateMarketAccount(@Valid @RequestBody MarketAccount marketAccount) throws URISyntaxException {
        log.debug("REST request to update MarketAccount : {}", marketAccount);
        if (marketAccount.getId() == null) {
            return createMarketAccount(marketAccount);
        }
        MarketAccount result = marketAccountService.save(marketAccount);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, marketAccount.getId().toString()))
            .body(result);
    }

    /**
     * GET  /market-accounts : get all the marketAccounts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of marketAccounts in body
     */
    @GetMapping("/market-accounts")
    @Timed
    public ResponseEntity<List<MarketAccount>> getAllMarketAccounts(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of MarketAccounts");
        Page<MarketAccount> page = marketAccountService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/market-accounts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /market-accounts/:id : get the "id" marketAccount.
     *
     * @param id the id of the marketAccount to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the marketAccount, or with status 404 (Not Found)
     */
    @GetMapping("/market-accounts/{id}")
    @Timed
    public ResponseEntity<MarketAccount> getMarketAccount(@PathVariable Long id) {
        log.debug("REST request to get MarketAccount : {}", id);
        MarketAccount marketAccount = marketAccountService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(marketAccount));
    }

    /**
     * DELETE  /market-accounts/:id : delete the "id" marketAccount.
     *
     * @param id the id of the marketAccount to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/market-accounts/{id}")
    @Timed
    public ResponseEntity<Void> deleteMarketAccount(@PathVariable Long id) {
        log.debug("REST request to delete MarketAccount : {}", id);
        marketAccountService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/market-accounts?query=:query : search for the marketAccount corresponding
     * to the query.
     *
     * @param query the query of the marketAccount search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/market-accounts")
    @Timed
    public ResponseEntity<List<MarketAccount>> searchMarketAccounts(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of MarketAccounts for query {}", query);
        Page<MarketAccount> page = marketAccountService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/market-accounts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
