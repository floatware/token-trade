/**
 * View Models used by Spring MVC REST controllers.
 */
package com.floatware.tokentrade.web.rest.vm;
