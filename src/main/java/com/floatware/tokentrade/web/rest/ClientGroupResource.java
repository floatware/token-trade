package com.floatware.tokentrade.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.floatware.tokentrade.domain.ClientGroup;
import com.floatware.tokentrade.service.ClientGroupService;
import com.floatware.tokentrade.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ClientGroup.
 */
@RestController
@RequestMapping("/api")
public class ClientGroupResource {

    private final Logger log = LoggerFactory.getLogger(ClientGroupResource.class);

    private static final String ENTITY_NAME = "clientGroup";

    private final ClientGroupService clientGroupService;

    public ClientGroupResource(ClientGroupService clientGroupService) {
        this.clientGroupService = clientGroupService;
    }

    /**
     * POST  /client-groups : Create a new clientGroup.
     *
     * @param clientGroup the clientGroup to create
     * @return the ResponseEntity with status 201 (Created) and with body the new clientGroup, or with status 400 (Bad Request) if the clientGroup has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/client-groups")
    @Timed
    public ResponseEntity<ClientGroup> createClientGroup(@Valid @RequestBody ClientGroup clientGroup) throws URISyntaxException {
        log.debug("REST request to save ClientGroup : {}", clientGroup);
        if (clientGroup.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new clientGroup cannot already have an ID")).body(null);
        }
        ClientGroup result = clientGroupService.save(clientGroup);
        return ResponseEntity.created(new URI("/api/client-groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /client-groups : Updates an existing clientGroup.
     *
     * @param clientGroup the clientGroup to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated clientGroup,
     * or with status 400 (Bad Request) if the clientGroup is not valid,
     * or with status 500 (Internal Server Error) if the clientGroup couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/client-groups")
    @Timed
    public ResponseEntity<ClientGroup> updateClientGroup(@Valid @RequestBody ClientGroup clientGroup) throws URISyntaxException {
        log.debug("REST request to update ClientGroup : {}", clientGroup);
        if (clientGroup.getId() == null) {
            return createClientGroup(clientGroup);
        }
        ClientGroup result = clientGroupService.save(clientGroup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, clientGroup.getId().toString()))
            .body(result);
    }

    /**
     * GET  /client-groups : get all the clientGroups.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of clientGroups in body
     */
    @GetMapping("/client-groups")
    @Timed
    public List<ClientGroup> getAllClientGroups() {
        log.debug("REST request to get all ClientGroups");
        return clientGroupService.findAll();
        }

    /**
     * GET  /client-groups/:id : get the "id" clientGroup.
     *
     * @param id the id of the clientGroup to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the clientGroup, or with status 404 (Not Found)
     */
    @GetMapping("/client-groups/{id}")
    @Timed
    public ResponseEntity<ClientGroup> getClientGroup(@PathVariable Long id) {
        log.debug("REST request to get ClientGroup : {}", id);
        ClientGroup clientGroup = clientGroupService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(clientGroup));
    }

    /**
     * DELETE  /client-groups/:id : delete the "id" clientGroup.
     *
     * @param id the id of the clientGroup to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/client-groups/{id}")
    @Timed
    public ResponseEntity<Void> deleteClientGroup(@PathVariable Long id) {
        log.debug("REST request to delete ClientGroup : {}", id);
        clientGroupService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/client-groups?query=:query : search for the clientGroup corresponding
     * to the query.
     *
     * @param query the query of the clientGroup search
     * @return the result of the search
     */
    @GetMapping("/_search/client-groups")
    @Timed
    public List<ClientGroup> searchClientGroups(@RequestParam String query) {
        log.debug("REST request to search ClientGroups for query {}", query);
        return clientGroupService.search(query);
    }

}
