package com.floatware.tokentrade.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.floatware.tokentrade.domain.TradingPlan;
import com.floatware.tokentrade.service.TradingPlanService;
import com.floatware.tokentrade.web.rest.util.HeaderUtil;
import com.floatware.tokentrade.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TradingPlan.
 */
@RestController
@RequestMapping("/api")
public class TradingPlanResource {

    private final Logger log = LoggerFactory.getLogger(TradingPlanResource.class);

    private static final String ENTITY_NAME = "tradingPlan";

    private final TradingPlanService tradingPlanService;

    public TradingPlanResource(TradingPlanService tradingPlanService) {
        this.tradingPlanService = tradingPlanService;
    }

    /**
     * POST  /trading-plans : Create a new tradingPlan.
     *
     * @param tradingPlan the tradingPlan to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tradingPlan, or with status 400 (Bad Request) if the tradingPlan has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trading-plans")
    @Timed
    public ResponseEntity<TradingPlan> createTradingPlan(@Valid @RequestBody TradingPlan tradingPlan) throws URISyntaxException {
        log.debug("REST request to save TradingPlan : {}", tradingPlan);
        if (tradingPlan.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tradingPlan cannot already have an ID")).body(null);
        }
        TradingPlan result = tradingPlanService.save(tradingPlan);
        return ResponseEntity.created(new URI("/api/trading-plans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /trading-plans : Updates an existing tradingPlan.
     *
     * @param tradingPlan the tradingPlan to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tradingPlan,
     * or with status 400 (Bad Request) if the tradingPlan is not valid,
     * or with status 500 (Internal Server Error) if the tradingPlan couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/trading-plans")
    @Timed
    public ResponseEntity<TradingPlan> updateTradingPlan(@Valid @RequestBody TradingPlan tradingPlan) throws URISyntaxException {
        log.debug("REST request to update TradingPlan : {}", tradingPlan);
        if (tradingPlan.getId() == null) {
            return createTradingPlan(tradingPlan);
        }
        TradingPlan result = tradingPlanService.save(tradingPlan);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tradingPlan.getId().toString()))
            .body(result);
    }

    /**
     * GET  /trading-plans : get all the tradingPlans.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tradingPlans in body
     */
    @GetMapping("/trading-plans")
    @Timed
    public ResponseEntity<List<TradingPlan>> getAllTradingPlans(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of TradingPlans");
        Page<TradingPlan> page = tradingPlanService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/trading-plans");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /trading-plans/:id : get the "id" tradingPlan.
     *
     * @param id the id of the tradingPlan to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tradingPlan, or with status 404 (Not Found)
     */
    @GetMapping("/trading-plans/{id}")
    @Timed
    public ResponseEntity<TradingPlan> getTradingPlan(@PathVariable Long id) {
        log.debug("REST request to get TradingPlan : {}", id);
        TradingPlan tradingPlan = tradingPlanService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tradingPlan));
    }

    /**
     * DELETE  /trading-plans/:id : delete the "id" tradingPlan.
     *
     * @param id the id of the tradingPlan to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/trading-plans/{id}")
    @Timed
    public ResponseEntity<Void> deleteTradingPlan(@PathVariable Long id) {
        log.debug("REST request to delete TradingPlan : {}", id);
        tradingPlanService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/trading-plans?query=:query : search for the tradingPlan corresponding
     * to the query.
     *
     * @param query the query of the tradingPlan search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/trading-plans")
    @Timed
    public ResponseEntity<List<TradingPlan>> searchTradingPlans(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of TradingPlans for query {}", query);
        Page<TradingPlan> page = tradingPlanService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/trading-plans");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
