package com.floatware.tokentrade.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.floatware.tokentrade.domain.TradingOrderType;
import com.floatware.tokentrade.service.TradingOrderTypeService;
import com.floatware.tokentrade.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TradingOrderType.
 */
@RestController
@RequestMapping("/api")
public class TradingOrderTypeResource {

    private final Logger log = LoggerFactory.getLogger(TradingOrderTypeResource.class);

    private static final String ENTITY_NAME = "tradingOrderType";

    private final TradingOrderTypeService tradingOrderTypeService;

    public TradingOrderTypeResource(TradingOrderTypeService tradingOrderTypeService) {
        this.tradingOrderTypeService = tradingOrderTypeService;
    }

    /**
     * POST  /trading-order-types : Create a new tradingOrderType.
     *
     * @param tradingOrderType the tradingOrderType to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tradingOrderType, or with status 400 (Bad Request) if the tradingOrderType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trading-order-types")
    @Timed
    public ResponseEntity<TradingOrderType> createTradingOrderType(@Valid @RequestBody TradingOrderType tradingOrderType) throws URISyntaxException {
        log.debug("REST request to save TradingOrderType : {}", tradingOrderType);
        if (tradingOrderType.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tradingOrderType cannot already have an ID")).body(null);
        }
        TradingOrderType result = tradingOrderTypeService.save(tradingOrderType);
        return ResponseEntity.created(new URI("/api/trading-order-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /trading-order-types : Updates an existing tradingOrderType.
     *
     * @param tradingOrderType the tradingOrderType to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tradingOrderType,
     * or with status 400 (Bad Request) if the tradingOrderType is not valid,
     * or with status 500 (Internal Server Error) if the tradingOrderType couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/trading-order-types")
    @Timed
    public ResponseEntity<TradingOrderType> updateTradingOrderType(@Valid @RequestBody TradingOrderType tradingOrderType) throws URISyntaxException {
        log.debug("REST request to update TradingOrderType : {}", tradingOrderType);
        if (tradingOrderType.getId() == null) {
            return createTradingOrderType(tradingOrderType);
        }
        TradingOrderType result = tradingOrderTypeService.save(tradingOrderType);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tradingOrderType.getId().toString()))
            .body(result);
    }

    /**
     * GET  /trading-order-types : get all the tradingOrderTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tradingOrderTypes in body
     */
    @GetMapping("/trading-order-types")
    @Timed
    public List<TradingOrderType> getAllTradingOrderTypes() {
        log.debug("REST request to get all TradingOrderTypes");
        return tradingOrderTypeService.findAll();
        }

    /**
     * GET  /trading-order-types/:id : get the "id" tradingOrderType.
     *
     * @param id the id of the tradingOrderType to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tradingOrderType, or with status 404 (Not Found)
     */
    @GetMapping("/trading-order-types/{id}")
    @Timed
    public ResponseEntity<TradingOrderType> getTradingOrderType(@PathVariable Long id) {
        log.debug("REST request to get TradingOrderType : {}", id);
        TradingOrderType tradingOrderType = tradingOrderTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tradingOrderType));
    }

    /**
     * DELETE  /trading-order-types/:id : delete the "id" tradingOrderType.
     *
     * @param id the id of the tradingOrderType to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/trading-order-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteTradingOrderType(@PathVariable Long id) {
        log.debug("REST request to delete TradingOrderType : {}", id);
        tradingOrderTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/trading-order-types?query=:query : search for the tradingOrderType corresponding
     * to the query.
     *
     * @param query the query of the tradingOrderType search
     * @return the result of the search
     */
    @GetMapping("/_search/trading-order-types")
    @Timed
    public List<TradingOrderType> searchTradingOrderTypes(@RequestParam String query) {
        log.debug("REST request to search TradingOrderTypes for query {}", query);
        return tradingOrderTypeService.search(query);
    }

}
