package com.floatware.tokentrade.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.floatware.tokentrade.domain.TradingOrderPackage;
import com.floatware.tokentrade.service.TradingOrderPackageService;
import com.floatware.tokentrade.web.rest.util.HeaderUtil;
import com.floatware.tokentrade.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TradingOrderPackage.
 */
@RestController
@RequestMapping("/api")
public class TradingOrderPackageResource {

    private final Logger log = LoggerFactory.getLogger(TradingOrderPackageResource.class);

    private static final String ENTITY_NAME = "tradingOrderPackage";

    private final TradingOrderPackageService tradingOrderPackageService;

    public TradingOrderPackageResource(TradingOrderPackageService tradingOrderPackageService) {
        this.tradingOrderPackageService = tradingOrderPackageService;
    }

    /**
     * POST  /trading-order-packages : Create a new tradingOrderPackage.
     *
     * @param tradingOrderPackage the tradingOrderPackage to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tradingOrderPackage, or with status 400 (Bad Request) if the tradingOrderPackage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trading-order-packages")
    @Timed
    public ResponseEntity<TradingOrderPackage> createTradingOrderPackage(@Valid @RequestBody TradingOrderPackage tradingOrderPackage) throws URISyntaxException {
        log.debug("REST request to save TradingOrderPackage : {}", tradingOrderPackage);
        if (tradingOrderPackage.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tradingOrderPackage cannot already have an ID")).body(null);
        }
        TradingOrderPackage result = tradingOrderPackageService.save(tradingOrderPackage);
        return ResponseEntity.created(new URI("/api/trading-order-packages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /trading-order-packages : Updates an existing tradingOrderPackage.
     *
     * @param tradingOrderPackage the tradingOrderPackage to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tradingOrderPackage,
     * or with status 400 (Bad Request) if the tradingOrderPackage is not valid,
     * or with status 500 (Internal Server Error) if the tradingOrderPackage couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/trading-order-packages")
    @Timed
    public ResponseEntity<TradingOrderPackage> updateTradingOrderPackage(@Valid @RequestBody TradingOrderPackage tradingOrderPackage) throws URISyntaxException {
        log.debug("REST request to update TradingOrderPackage : {}", tradingOrderPackage);
        if (tradingOrderPackage.getId() == null) {
            return createTradingOrderPackage(tradingOrderPackage);
        }
        TradingOrderPackage result = tradingOrderPackageService.save(tradingOrderPackage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tradingOrderPackage.getId().toString()))
            .body(result);
    }

    /**
     * GET  /trading-order-packages : get all the tradingOrderPackages.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tradingOrderPackages in body
     */
    @GetMapping("/trading-order-packages")
    @Timed
    public ResponseEntity<List<TradingOrderPackage>> getAllTradingOrderPackages(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of TradingOrderPackages");
        Page<TradingOrderPackage> page = tradingOrderPackageService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/trading-order-packages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /trading-order-packages/:id : get the "id" tradingOrderPackage.
     *
     * @param id the id of the tradingOrderPackage to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tradingOrderPackage, or with status 404 (Not Found)
     */
    @GetMapping("/trading-order-packages/{id}")
    @Timed
    public ResponseEntity<TradingOrderPackage> getTradingOrderPackage(@PathVariable Long id) {
        log.debug("REST request to get TradingOrderPackage : {}", id);
        TradingOrderPackage tradingOrderPackage = tradingOrderPackageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tradingOrderPackage));
    }

    /**
     * DELETE  /trading-order-packages/:id : delete the "id" tradingOrderPackage.
     *
     * @param id the id of the tradingOrderPackage to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/trading-order-packages/{id}")
    @Timed
    public ResponseEntity<Void> deleteTradingOrderPackage(@PathVariable Long id) {
        log.debug("REST request to delete TradingOrderPackage : {}", id);
        tradingOrderPackageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/trading-order-packages?query=:query : search for the tradingOrderPackage corresponding
     * to the query.
     *
     * @param query the query of the tradingOrderPackage search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/trading-order-packages")
    @Timed
    public ResponseEntity<List<TradingOrderPackage>> searchTradingOrderPackages(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of TradingOrderPackages for query {}", query);
        Page<TradingOrderPackage> page = tradingOrderPackageService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/trading-order-packages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
