package com.floatware.tokentrade.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.floatware.tokentrade.domain.TradingOrder;
import com.floatware.tokentrade.service.TradingOrderService;
import com.floatware.tokentrade.web.rest.util.HeaderUtil;
import com.floatware.tokentrade.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TradingOrder.
 */
@RestController
@RequestMapping("/api")
public class TradingOrderResource {

    private final Logger log = LoggerFactory.getLogger(TradingOrderResource.class);

    private static final String ENTITY_NAME = "tradingOrder";

    private final TradingOrderService tradingOrderService;

    public TradingOrderResource(TradingOrderService tradingOrderService) {
        this.tradingOrderService = tradingOrderService;
    }

    /**
     * POST  /trading-orders : Create a new tradingOrder.
     *
     * @param tradingOrder the tradingOrder to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tradingOrder, or with status 400 (Bad Request) if the tradingOrder has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trading-orders")
    @Timed
    public ResponseEntity<TradingOrder> createTradingOrder(@Valid @RequestBody TradingOrder tradingOrder) throws URISyntaxException {
        log.debug("REST request to save TradingOrder : {}", tradingOrder);
        if (tradingOrder.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tradingOrder cannot already have an ID")).body(null);
        }
        TradingOrder result = tradingOrderService.save(tradingOrder);
        return ResponseEntity.created(new URI("/api/trading-orders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /trading-orders : Updates an existing tradingOrder.
     *
     * @param tradingOrder the tradingOrder to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tradingOrder,
     * or with status 400 (Bad Request) if the tradingOrder is not valid,
     * or with status 500 (Internal Server Error) if the tradingOrder couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/trading-orders")
    @Timed
    public ResponseEntity<TradingOrder> updateTradingOrder(@Valid @RequestBody TradingOrder tradingOrder) throws URISyntaxException {
        log.debug("REST request to update TradingOrder : {}", tradingOrder);
        if (tradingOrder.getId() == null) {
            return createTradingOrder(tradingOrder);
        }
        TradingOrder result = tradingOrderService.save(tradingOrder);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tradingOrder.getId().toString()))
            .body(result);
    }

    /**
     * GET  /trading-orders : get all the tradingOrders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tradingOrders in body
     */
    @GetMapping("/trading-orders")
    @Timed
    public ResponseEntity<List<TradingOrder>> getAllTradingOrders(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of TradingOrders");
        Page<TradingOrder> page = tradingOrderService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/trading-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /trading-orders/:id : get the "id" tradingOrder.
     *
     * @param id the id of the tradingOrder to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tradingOrder, or with status 404 (Not Found)
     */
    @GetMapping("/trading-orders/{id}")
    @Timed
    public ResponseEntity<TradingOrder> getTradingOrder(@PathVariable Long id) {
        log.debug("REST request to get TradingOrder : {}", id);
        TradingOrder tradingOrder = tradingOrderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tradingOrder));
    }

    /**
     * DELETE  /trading-orders/:id : delete the "id" tradingOrder.
     *
     * @param id the id of the tradingOrder to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/trading-orders/{id}")
    @Timed
    public ResponseEntity<Void> deleteTradingOrder(@PathVariable Long id) {
        log.debug("REST request to delete TradingOrder : {}", id);
        tradingOrderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/trading-orders?query=:query : search for the tradingOrder corresponding
     * to the query.
     *
     * @param query the query of the tradingOrder search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/trading-orders")
    @Timed
    public ResponseEntity<List<TradingOrder>> searchTradingOrders(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of TradingOrders for query {}", query);
        Page<TradingOrder> page = tradingOrderService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/trading-orders");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
