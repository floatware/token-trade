/**
 * Data Access Objects used by WebSocket services.
 */
package com.floatware.tokentrade.web.websocket.dto;
