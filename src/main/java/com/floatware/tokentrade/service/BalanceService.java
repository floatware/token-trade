package com.floatware.tokentrade.service;

import com.floatware.tokentrade.domain.Balance;
import com.floatware.tokentrade.repository.BalanceRepository;
import com.floatware.tokentrade.repository.search.BalanceSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Balance.
 */
@Service
@Transactional
public class BalanceService {

    private final Logger log = LoggerFactory.getLogger(BalanceService.class);

    private final BalanceRepository balanceRepository;

    private final BalanceSearchRepository balanceSearchRepository;

    public BalanceService(BalanceRepository balanceRepository, BalanceSearchRepository balanceSearchRepository) {
        this.balanceRepository = balanceRepository;
        this.balanceSearchRepository = balanceSearchRepository;
    }

    /**
     * Save a balance.
     *
     * @param balance the entity to save
     * @return the persisted entity
     */
    public Balance save(Balance balance) {
        log.debug("Request to save Balance : {}", balance);
        Balance result = balanceRepository.save(balance);
        balanceSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the balances.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Balance> findAll(Pageable pageable) {
        log.debug("Request to get all Balances");
        return balanceRepository.findAll(pageable);
    }

    /**
     *  Get one balance by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Balance findOne(Long id) {
        log.debug("Request to get Balance : {}", id);
        return balanceRepository.findOne(id);
    }

    /**
     *  Delete the  balance by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Balance : {}", id);
        balanceRepository.delete(id);
        balanceSearchRepository.delete(id);
    }

    /**
     * Search for the balance corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Balance> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Balances for query {}", query);
        Page<Balance> result = balanceSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
