package com.floatware.tokentrade.service;

import com.floatware.tokentrade.domain.TradingOrderType;
import com.floatware.tokentrade.repository.TradingOrderTypeRepository;
import com.floatware.tokentrade.repository.search.TradingOrderTypeSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing TradingOrderType.
 */
@Service
@Transactional
public class TradingOrderTypeService {

    private final Logger log = LoggerFactory.getLogger(TradingOrderTypeService.class);

    private final TradingOrderTypeRepository tradingOrderTypeRepository;

    private final TradingOrderTypeSearchRepository tradingOrderTypeSearchRepository;

    public TradingOrderTypeService(TradingOrderTypeRepository tradingOrderTypeRepository, TradingOrderTypeSearchRepository tradingOrderTypeSearchRepository) {
        this.tradingOrderTypeRepository = tradingOrderTypeRepository;
        this.tradingOrderTypeSearchRepository = tradingOrderTypeSearchRepository;
    }

    /**
     * Save a tradingOrderType.
     *
     * @param tradingOrderType the entity to save
     * @return the persisted entity
     */
    public TradingOrderType save(TradingOrderType tradingOrderType) {
        log.debug("Request to save TradingOrderType : {}", tradingOrderType);
        TradingOrderType result = tradingOrderTypeRepository.save(tradingOrderType);
        tradingOrderTypeSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the tradingOrderTypes.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<TradingOrderType> findAll() {
        log.debug("Request to get all TradingOrderTypes");
        return tradingOrderTypeRepository.findAll();
    }

    /**
     *  Get one tradingOrderType by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public TradingOrderType findOne(Long id) {
        log.debug("Request to get TradingOrderType : {}", id);
        return tradingOrderTypeRepository.findOne(id);
    }

    /**
     *  Delete the  tradingOrderType by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TradingOrderType : {}", id);
        tradingOrderTypeRepository.delete(id);
        tradingOrderTypeSearchRepository.delete(id);
    }

    /**
     * Search for the tradingOrderType corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<TradingOrderType> search(String query) {
        log.debug("Request to search TradingOrderTypes for query {}", query);
        return StreamSupport
            .stream(tradingOrderTypeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
