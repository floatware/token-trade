package com.floatware.tokentrade.service;

import com.floatware.tokentrade.domain.TradingPlan;
import com.floatware.tokentrade.repository.TradingPlanRepository;
import com.floatware.tokentrade.repository.search.TradingPlanSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing TradingPlan.
 */
@Service
@Transactional
public class TradingPlanService {

    private final Logger log = LoggerFactory.getLogger(TradingPlanService.class);

    private final TradingPlanRepository tradingPlanRepository;

    private final TradingPlanSearchRepository tradingPlanSearchRepository;

    public TradingPlanService(TradingPlanRepository tradingPlanRepository, TradingPlanSearchRepository tradingPlanSearchRepository) {
        this.tradingPlanRepository = tradingPlanRepository;
        this.tradingPlanSearchRepository = tradingPlanSearchRepository;
    }

    /**
     * Save a tradingPlan.
     *
     * @param tradingPlan the entity to save
     * @return the persisted entity
     */
    public TradingPlan save(TradingPlan tradingPlan) {
        log.debug("Request to save TradingPlan : {}", tradingPlan);
        TradingPlan result = tradingPlanRepository.save(tradingPlan);
        tradingPlanSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the tradingPlans.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TradingPlan> findAll(Pageable pageable) {
        log.debug("Request to get all TradingPlans");
        return tradingPlanRepository.findAll(pageable);
    }

    /**
     *  Get one tradingPlan by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public TradingPlan findOne(Long id) {
        log.debug("Request to get TradingPlan : {}", id);
        return tradingPlanRepository.findOne(id);
    }

    /**
     *  Delete the  tradingPlan by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TradingPlan : {}", id);
        tradingPlanRepository.delete(id);
        tradingPlanSearchRepository.delete(id);
    }

    /**
     * Search for the tradingPlan corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TradingPlan> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of TradingPlans for query {}", query);
        Page<TradingPlan> result = tradingPlanSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
