package com.floatware.tokentrade.service;

import com.floatware.tokentrade.domain.AlertEvent;
import com.floatware.tokentrade.repository.AlertEventRepository;
import com.floatware.tokentrade.repository.search.AlertEventSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing AlertEvent.
 */
@Service
@Transactional
public class AlertEventService {

    private final Logger log = LoggerFactory.getLogger(AlertEventService.class);

    private final AlertEventRepository alertEventRepository;

    private final AlertEventSearchRepository alertEventSearchRepository;

    public AlertEventService(AlertEventRepository alertEventRepository, AlertEventSearchRepository alertEventSearchRepository) {
        this.alertEventRepository = alertEventRepository;
        this.alertEventSearchRepository = alertEventSearchRepository;
    }

    /**
     * Save a alertEvent.
     *
     * @param alertEvent the entity to save
     * @return the persisted entity
     */
    public AlertEvent save(AlertEvent alertEvent) {
        log.debug("Request to save AlertEvent : {}", alertEvent);
        AlertEvent result = alertEventRepository.save(alertEvent);
        alertEventSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the alertEvents.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AlertEvent> findAll(Pageable pageable) {
        log.debug("Request to get all AlertEvents");
        return alertEventRepository.findAll(pageable);
    }

    /**
     *  Get one alertEvent by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public AlertEvent findOne(Long id) {
        log.debug("Request to get AlertEvent : {}", id);
        return alertEventRepository.findOne(id);
    }

    /**
     *  Delete the  alertEvent by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AlertEvent : {}", id);
        alertEventRepository.delete(id);
        alertEventSearchRepository.delete(id);
    }

    /**
     * Search for the alertEvent corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AlertEvent> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of AlertEvents for query {}", query);
        Page<AlertEvent> result = alertEventSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
