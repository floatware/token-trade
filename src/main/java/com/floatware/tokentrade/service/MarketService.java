package com.floatware.tokentrade.service;

import com.floatware.tokentrade.domain.Market;
import com.floatware.tokentrade.repository.MarketRepository;
import com.floatware.tokentrade.repository.search.MarketSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Market.
 */
@Service
@Transactional
public class MarketService {

    private final Logger log = LoggerFactory.getLogger(MarketService.class);

    private final MarketRepository marketRepository;

    private final MarketSearchRepository marketSearchRepository;

    public MarketService(MarketRepository marketRepository, MarketSearchRepository marketSearchRepository) {
        this.marketRepository = marketRepository;
        this.marketSearchRepository = marketSearchRepository;
    }

    /**
     * Save a market.
     *
     * @param market the entity to save
     * @return the persisted entity
     */
    public Market save(Market market) {
        log.debug("Request to save Market : {}", market);
        Market result = marketRepository.save(market);
        marketSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the markets.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Market> findAll(Pageable pageable) {
        log.debug("Request to get all Markets");
        return marketRepository.findAll(pageable);
    }

    /**
     *  Get one market by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Market findOne(Long id) {
        log.debug("Request to get Market : {}", id);
        return marketRepository.findOne(id);
    }

    /**
     *  Delete the  market by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Market : {}", id);
        marketRepository.delete(id);
        marketSearchRepository.delete(id);
    }

    /**
     * Search for the market corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Market> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Markets for query {}", query);
        Page<Market> result = marketSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
