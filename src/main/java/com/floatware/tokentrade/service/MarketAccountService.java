package com.floatware.tokentrade.service;

import com.floatware.tokentrade.domain.MarketAccount;
import com.floatware.tokentrade.repository.MarketAccountRepository;
import com.floatware.tokentrade.repository.search.MarketAccountSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing MarketAccount.
 */
@Service
@Transactional
public class MarketAccountService {

    private final Logger log = LoggerFactory.getLogger(MarketAccountService.class);

    private final MarketAccountRepository marketAccountRepository;

    private final MarketAccountSearchRepository marketAccountSearchRepository;

    public MarketAccountService(MarketAccountRepository marketAccountRepository, MarketAccountSearchRepository marketAccountSearchRepository) {
        this.marketAccountRepository = marketAccountRepository;
        this.marketAccountSearchRepository = marketAccountSearchRepository;
    }

    /**
     * Save a marketAccount.
     *
     * @param marketAccount the entity to save
     * @return the persisted entity
     */
    public MarketAccount save(MarketAccount marketAccount) {
        log.debug("Request to save MarketAccount : {}", marketAccount);
        MarketAccount result = marketAccountRepository.save(marketAccount);
        marketAccountSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the marketAccounts.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MarketAccount> findAll(Pageable pageable) {
        log.debug("Request to get all MarketAccounts");
        return marketAccountRepository.findAll(pageable);
    }

    /**
     *  Get one marketAccount by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public MarketAccount findOne(Long id) {
        log.debug("Request to get MarketAccount : {}", id);
        return marketAccountRepository.findOne(id);
    }

    /**
     *  Delete the  marketAccount by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete MarketAccount : {}", id);
        marketAccountRepository.delete(id);
        marketAccountSearchRepository.delete(id);
    }

    /**
     * Search for the marketAccount corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MarketAccount> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of MarketAccounts for query {}", query);
        Page<MarketAccount> result = marketAccountSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
