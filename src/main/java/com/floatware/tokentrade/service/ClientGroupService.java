package com.floatware.tokentrade.service;

import com.floatware.tokentrade.domain.ClientGroup;
import com.floatware.tokentrade.repository.ClientGroupRepository;
import com.floatware.tokentrade.repository.search.ClientGroupSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ClientGroup.
 */
@Service
@Transactional
public class ClientGroupService {

    private final Logger log = LoggerFactory.getLogger(ClientGroupService.class);

    private final ClientGroupRepository clientGroupRepository;

    private final ClientGroupSearchRepository clientGroupSearchRepository;

    public ClientGroupService(ClientGroupRepository clientGroupRepository, ClientGroupSearchRepository clientGroupSearchRepository) {
        this.clientGroupRepository = clientGroupRepository;
        this.clientGroupSearchRepository = clientGroupSearchRepository;
    }

    /**
     * Save a clientGroup.
     *
     * @param clientGroup the entity to save
     * @return the persisted entity
     */
    public ClientGroup save(ClientGroup clientGroup) {
        log.debug("Request to save ClientGroup : {}", clientGroup);
        ClientGroup result = clientGroupRepository.save(clientGroup);
        clientGroupSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the clientGroups.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ClientGroup> findAll() {
        log.debug("Request to get all ClientGroups");
        return clientGroupRepository.findAllWithEagerRelationships();
    }

    /**
     *  Get one clientGroup by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ClientGroup findOne(Long id) {
        log.debug("Request to get ClientGroup : {}", id);
        return clientGroupRepository.findOneWithEagerRelationships(id);
    }

    /**
     *  Delete the  clientGroup by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ClientGroup : {}", id);
        clientGroupRepository.delete(id);
        clientGroupSearchRepository.delete(id);
    }

    /**
     * Search for the clientGroup corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<ClientGroup> search(String query) {
        log.debug("Request to search ClientGroups for query {}", query);
        return StreamSupport
            .stream(clientGroupSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
