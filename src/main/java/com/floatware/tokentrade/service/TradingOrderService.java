package com.floatware.tokentrade.service;

import com.floatware.tokentrade.domain.TradingOrder;
import com.floatware.tokentrade.repository.TradingOrderRepository;
import com.floatware.tokentrade.repository.search.TradingOrderSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing TradingOrder.
 */
@Service
@Transactional
public class TradingOrderService {

    private final Logger log = LoggerFactory.getLogger(TradingOrderService.class);

    private final TradingOrderRepository tradingOrderRepository;

    private final TradingOrderSearchRepository tradingOrderSearchRepository;

    public TradingOrderService(TradingOrderRepository tradingOrderRepository, TradingOrderSearchRepository tradingOrderSearchRepository) {
        this.tradingOrderRepository = tradingOrderRepository;
        this.tradingOrderSearchRepository = tradingOrderSearchRepository;
    }

    /**
     * Save a tradingOrder.
     *
     * @param tradingOrder the entity to save
     * @return the persisted entity
     */
    public TradingOrder save(TradingOrder tradingOrder) {
        log.debug("Request to save TradingOrder : {}", tradingOrder);
        TradingOrder result = tradingOrderRepository.save(tradingOrder);
        tradingOrderSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the tradingOrders.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TradingOrder> findAll(Pageable pageable) {
        log.debug("Request to get all TradingOrders");
        return tradingOrderRepository.findAll(pageable);
    }

    /**
     *  Get one tradingOrder by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public TradingOrder findOne(Long id) {
        log.debug("Request to get TradingOrder : {}", id);
        return tradingOrderRepository.findOne(id);
    }

    /**
     *  Delete the  tradingOrder by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TradingOrder : {}", id);
        tradingOrderRepository.delete(id);
        tradingOrderSearchRepository.delete(id);
    }

    /**
     * Search for the tradingOrder corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TradingOrder> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of TradingOrders for query {}", query);
        Page<TradingOrder> result = tradingOrderSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
