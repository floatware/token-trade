package com.floatware.tokentrade.service;

import com.floatware.tokentrade.domain.TradingOrderPackage;
import com.floatware.tokentrade.repository.TradingOrderPackageRepository;
import com.floatware.tokentrade.repository.search.TradingOrderPackageSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing TradingOrderPackage.
 */
@Service
@Transactional
public class TradingOrderPackageService {

    private final Logger log = LoggerFactory.getLogger(TradingOrderPackageService.class);

    private final TradingOrderPackageRepository tradingOrderPackageRepository;

    private final TradingOrderPackageSearchRepository tradingOrderPackageSearchRepository;

    public TradingOrderPackageService(TradingOrderPackageRepository tradingOrderPackageRepository, TradingOrderPackageSearchRepository tradingOrderPackageSearchRepository) {
        this.tradingOrderPackageRepository = tradingOrderPackageRepository;
        this.tradingOrderPackageSearchRepository = tradingOrderPackageSearchRepository;
    }

    /**
     * Save a tradingOrderPackage.
     *
     * @param tradingOrderPackage the entity to save
     * @return the persisted entity
     */
    public TradingOrderPackage save(TradingOrderPackage tradingOrderPackage) {
        log.debug("Request to save TradingOrderPackage : {}", tradingOrderPackage);
        TradingOrderPackage result = tradingOrderPackageRepository.save(tradingOrderPackage);
        tradingOrderPackageSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the tradingOrderPackages.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TradingOrderPackage> findAll(Pageable pageable) {
        log.debug("Request to get all TradingOrderPackages");
        return tradingOrderPackageRepository.findAll(pageable);
    }

    /**
     *  Get one tradingOrderPackage by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public TradingOrderPackage findOne(Long id) {
        log.debug("Request to get TradingOrderPackage : {}", id);
        return tradingOrderPackageRepository.findOne(id);
    }

    /**
     *  Delete the  tradingOrderPackage by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TradingOrderPackage : {}", id);
        tradingOrderPackageRepository.delete(id);
        tradingOrderPackageSearchRepository.delete(id);
    }

    /**
     * Search for the tradingOrderPackage corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TradingOrderPackage> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of TradingOrderPackages for query {}", query);
        Page<TradingOrderPackage> result = tradingOrderPackageSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
