package com.floatware.tokentrade.service;

import com.floatware.tokentrade.domain.Tenant;
import com.floatware.tokentrade.repository.TenantRepository;
import com.floatware.tokentrade.repository.search.TenantSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Tenant.
 */
@Service
@Transactional
public class TenantService {

    private final Logger log = LoggerFactory.getLogger(TenantService.class);

    private final TenantRepository tenantRepository;

    private final TenantSearchRepository tenantSearchRepository;

    public TenantService(TenantRepository tenantRepository, TenantSearchRepository tenantSearchRepository) {
        this.tenantRepository = tenantRepository;
        this.tenantSearchRepository = tenantSearchRepository;
    }

    /**
     * Save a tenant.
     *
     * @param tenant the entity to save
     * @return the persisted entity
     */
    public Tenant save(Tenant tenant) {
        log.debug("Request to save Tenant : {}", tenant);
        Tenant result = tenantRepository.save(tenant);
        tenantSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the tenants.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Tenant> findAll() {
        log.debug("Request to get all Tenants");
        return tenantRepository.findAll();
    }

    /**
     *  Get one tenant by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Tenant findOne(Long id) {
        log.debug("Request to get Tenant : {}", id);
        return tenantRepository.findOne(id);
    }

    /**
     *  Delete the  tenant by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Tenant : {}", id);
        tenantRepository.delete(id);
        tenantSearchRepository.delete(id);
    }

    /**
     * Search for the tenant corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Tenant> search(String query) {
        log.debug("Request to search Tenants for query {}", query);
        return StreamSupport
            .stream(tenantSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
