package com.floatware.tokentrade.service;

import com.floatware.tokentrade.domain.Currency;
import com.floatware.tokentrade.repository.CurrencyRepository;
import com.floatware.tokentrade.repository.search.CurrencySearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Currency.
 */
@Service
@Transactional
public class CurrencyService {

    private final Logger log = LoggerFactory.getLogger(CurrencyService.class);

    private final CurrencyRepository currencyRepository;

    private final CurrencySearchRepository currencySearchRepository;

    public CurrencyService(CurrencyRepository currencyRepository, CurrencySearchRepository currencySearchRepository) {
        this.currencyRepository = currencyRepository;
        this.currencySearchRepository = currencySearchRepository;
    }

    /**
     * Save a currency.
     *
     * @param currency the entity to save
     * @return the persisted entity
     */
    public Currency save(Currency currency) {
        log.debug("Request to save Currency : {}", currency);
        Currency result = currencyRepository.save(currency);
        currencySearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the currencies.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Currency> findAll() {
        log.debug("Request to get all Currencies");
        return currencyRepository.findAll();
    }

    /**
     *  Get one currency by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Currency findOne(Long id) {
        log.debug("Request to get Currency : {}", id);
        return currencyRepository.findOne(id);
    }

    /**
     *  Delete the  currency by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Currency : {}", id);
        currencyRepository.delete(id);
        currencySearchRepository.delete(id);
    }

    /**
     * Search for the currency corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Currency> search(String query) {
        log.debug("Request to search Currencies for query {}", query);
        return StreamSupport
            .stream(currencySearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
