package com.floatware.tokentrade.service;

import com.floatware.tokentrade.domain.Trader;
import com.floatware.tokentrade.repository.TraderRepository;
import com.floatware.tokentrade.repository.search.TraderSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Trader.
 */
@Service
@Transactional
public class TraderService {

    private final Logger log = LoggerFactory.getLogger(TraderService.class);

    private final TraderRepository traderRepository;

    private final TraderSearchRepository traderSearchRepository;

    public TraderService(TraderRepository traderRepository, TraderSearchRepository traderSearchRepository) {
        this.traderRepository = traderRepository;
        this.traderSearchRepository = traderSearchRepository;
    }

    /**
     * Save a trader.
     *
     * @param trader the entity to save
     * @return the persisted entity
     */
    public Trader save(Trader trader) {
        log.debug("Request to save Trader : {}", trader);
        Trader result = traderRepository.save(trader);
        traderSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the traders.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Trader> findAll(Pageable pageable) {
        log.debug("Request to get all Traders");
        return traderRepository.findAll(pageable);
    }

    /**
     *  Get one trader by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Trader findOne(Long id) {
        log.debug("Request to get Trader : {}", id);
        return traderRepository.findOne(id);
    }

    /**
     *  Delete the  trader by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Trader : {}", id);
        traderRepository.delete(id);
        traderSearchRepository.delete(id);
    }

    /**
     * Search for the trader corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Trader> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Traders for query {}", query);
        Page<Trader> result = traderSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
